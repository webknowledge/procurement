###
# Structure Analysis Project (c) by $Authors
#
# This file is part of the Structure Analysis Project, 24/02/2016
#
# Structure Analysis Project is licensed under a dual license: European Union Public License 1.1
# and Commons Attribution 4.0 International (CC BY 4.0) License.
# Academic institutions and research are required to comply with
# CC BY 4.0 Attribution clause and cite the following paper describing the work.
# Please check the license file for more details.
###


from collections import defaultdict

from cam.metrics.similarities import similarity_sets, len_similarity_sets
from cam.parse.simpleParsing import regexpTokenize
from cam.settings import ApplicationSettings as settings

__author__ = "Ovidiu Serban"
__credits__ = ["Qiang Guo", "Ovidiu Serban", "Juan Tirado", "Eiko Yoneki"]  # alphabetic order
__maintainer__ = "Ovidiu Serban"
__email__ = "ovidiu.serban@cl.cam.ac.uk"


class BaseDictionary:
    """
    Base implementation for the dictionary encodings
    """

    def __init__(self):
        pass

    def add(self, xpath, text):
        pass

    def frequencies(self):
        pass

    def size(self):
        pass

    def encoding(self):
        pass

    def save(self, serializer):
        pass

    def load(self, serializer, object_id):
        pass


class XPathTextDictionary(BaseDictionary):
    """
    A dictionary implementation, used in the FrequencyAnalysis algorithm
    """

    def __init__(self, createEncoding, textSimilarityThreshold):
        super().__init__()
        self._encoding = defaultdict(str)
        self._frequencies = defaultdict(int)
        self._textEncoding = defaultdict(set)
        self._textTranslation = defaultdict(list)
        self._textFrequencies = defaultdict(int)
        self._values = defaultdict(list)
        self._createEncoding = createEncoding
        self._textSimilarityThreshold = textSimilarityThreshold

    def add(self, xpath, text):
        textEncoding = self._determineTextEncoding(text)
        encoding = self._determineEncoding(xpath, textEncoding)
        if encoding is not None:
            values = self._values[encoding]
            values.append(("/".join(xpath), text))

            _updateFrequencies(self._frequencies, encoding)
            _updateFrequencies(self._textFrequencies, textEncoding)

    def frequencies(self):
        return self._frequencies

    def textFrequencies(self):
        return self._textFrequencies

    def textEncoding(self):
        return self._textEncoding

    def textTranslation(self):
        return self._textTranslation

    def encoding(self):
        return self._encoding

    def values(self):
        return self._values

    def hasTranslation(self, encoding):
        return encoding in self._textTranslation

    def encodingFrequency(self, encoding):
        return self._textFrequencies[encoding]

    def save(self, serializer):
        _saveField(serializer, "encoding", self._encoding)
        _saveField(serializer, "frequencies", self._frequencies)
        _saveField(serializer, "textEncoding", _convertSetDictionary(self._textEncoding))
        _saveField(serializer, "textTranslation", self._textTranslation)
        _saveField(serializer, "textFrequencies", self._textFrequencies)
        _saveField(serializer, "values", self._values)

    def load(self, serializer, object_id):
        self._encoding = _loadField(serializer, "encoding")
        self._frequencies = _loadField(serializer, "frequencies")
        self._textEncoding = _loadField(serializer, "textEncoding")
        self._textFrequencies = _loadField(serializer, "textFrequencies")
        self._textTranslation = _loadField(serializer, "textTranslation")
        if self._textTranslation is None:
            self._textTranslation = defaultdict(list)
        self._values = _loadField(serializer, "values")

    def _determineEncoding(self, xpath, textEncoding):
        xpath_tuple = "/".join(xpath)
        xpath_tuple = "/".join((xpath_tuple, textEncoding))
        encoding = self._encoding.get(xpath_tuple)
        if encoding is None and self._createEncoding:
            encoding = len(self._encoding)
            self._encoding[xpath_tuple] = encoding
        return str(encoding)

    def _determineTextEncoding(self, text):
        textList = tuple(regexpTokenize(text.lower()))
        encoding = self._textEncodingSimilarity(textList)
        if encoding is None:
            encoding = str(len(self._textEncoding))
            self._textEncoding[encoding].add(textList)
        return encoding

    def _textEncodingSimilarity(self, text):
        if len(text) > settings.PROPERTY_max_tokens:
            return None

        for (key, values) in self._textEncoding.items():
            for value in values:
                if len(value) <= settings.PROPERTY_max_tokens:
                    if text == value:
                        return key
                    elif len_similarity_sets(text, value) <= settings.PROPERTY_len_similarity_threshold:
                        if similarity_sets(text, value, self._textSimilarityThreshold) > self._textSimilarityThreshold:
                            values.add(text)
                            return key
        return None

    def addTranslation(self, encoding, translation):
        textList = regexpTokenize(translation.lower())
        if encoding in self._textTranslation:
            self._textTranslation[encoding].append(textList)
        else:
            self._textTranslation.update({encoding: [textList]})

    def getTranslationOrText(self, encoding):
        if encoding in self._textTranslation:
            return self._textTranslation[encoding]
        else:
            return self._textEncoding[encoding]


def _saveField(serializer, fieldName, fieldValue):
    serialized = defaultdict()
    serialized["field"] = fieldName
    serialized["fieldValue"] = fieldValue
    serializer.serialize(serialized)


def _convertSetDictionary(setDictionary):
    listDictionary = defaultdict(list)
    for (key, values) in setDictionary.items():
        for value in values:
            listDictionary[key].append(list(value))
    return listDictionary


def _loadField(serializer, fieldName):
    serialized = serializer.deserialize_object({"field": fieldName})
    if serialized is not None:
        return serialized["fieldValue"]
    else:
        return None


def _updateFrequencies(frequencyDictionary, encoding):
    freq = frequencyDictionary.get(encoding)
    if freq is None:
        freq = 0
    freq += 1
    frequencyDictionary[encoding] = freq
