###
# Structure Analysis Project (c) by $Authors
#
# This file is part of the Structure Analysis Project, 24/02/2016
#
# Structure Analysis Project is licensed under a dual license: European Union Public License 1.1
# and Commons Attribution 4.0 International (CC BY 4.0) License.
# Academic institutions and research are required to comply with
# CC BY 4.0 Attribution clause and cite the following paper describing the work.
# Please check the license file for more details.
###


import logging
import os
import sys
import traceback

from glob import glob
from multiprocessing.pool import Pool

from cam.converter.datarescuers import getRescueClass, DataRescue
from cam.converter.htmlconverter import HtmlConverter
from cam.converter.xmlcreator import GenericXmlCreator, getXmlCreator
from cam.dictionary.encodingdictionary import getEncodingDictionary, EncodingDictionary
from cam.parse.simpleGenericParsing import GenericParser, getParser
from cam.serialisers.fileSerializer import FileSerializer
from cam.settings import ApplicationSettings as settings

__author__ = "Ovidiu Serban"
__credits__ = ["Qiang Guo", "Ovidiu Serban", "Juan Tirado", "Eiko Yoneki"]  # alphabetic order
__maintainer__ = "Ovidiu Serban"
__email__ = "ovidiu.serban@cl.cam.ac.uk"

logger = logging.getLogger("batchXmlExporter")

NUMBER_PROCESSES = 4


# This batch conversion script, uses a multiprocess pool to convert in parallel multiple html files into xml
# The number of processes can be configured, depending of CPU core number, the default is 4


def convertHtmlToXml(_filename, _outputName, _languageCode,
                     _mappingDictionary: EncodingDictionary,
                     _dataRescuer: DataRescue,
                     _dataParser: GenericParser,
                     _xmlCreator: GenericXmlCreator):
    try:
        htmlConverter = HtmlConverter(_mappingDictionary,
                                      _mappingDictionary.getTokenThreshold(settings.DEFAULT_tokenThreshold),
                                      _mappingDictionary.getListSimilarityThreshold(
                                          settings.DEFAULT_listSimilarityThreshold),
                                      _mappingDictionary.getPathThreshold(settings.DEFAULT_pathThreshold),
                                      _dataRescuer, _dataParser, _xmlCreator, _languageCode)
        htmlConverter.convertFilename(_filename)
        _xmlCreator.saveFilename(_outputName)
        logger.info("Saved xml file = %s", _outputName)
    except Exception as e:
        print("Exception: " + str(e))
        traceback.print_exc(file=sys.stdout)


if __name__ == "__main__":
    logging.basicConfig(stream=sys.stdout, level=logging.INFO,
                        format='%(asctime)s [%(name)s] %(levelname)s %(message)s',
                        datefmt='%a, %d %b %Y %H:%M:%S')

    if len(sys.argv) < 2:
        logger.error("Language must be provided as a parameter ...")
        exit()

    languageCode = None
    if len(sys.argv) == 3:
        languageCode = sys.argv[2]
        logger.info("Using Language code = " + languageCode)

    countryCode = sys.argv[1]
    encodingFile = "res/{}/encoding.json".format(countryCode)
    outFolder = "res/{}/converted".format(countryCode)
    inFolder = "res/{}/raw/*".format(countryCode)

    if not os.path.exists(outFolder):
        logger.info("Output folder (%s) does not exist, creating folder ...", outFolder)
        os.makedirs(outFolder)
    else:
        logger.info("Output folder (%s) exists, re-using it ...", outFolder)

    logger.info("Using input folder = %s", inFolder)

    serializer = FileSerializer(encodingFile)
    mappingDictionary = getEncodingDictionary(countryCode)
    mappingDictionary.loadEncoding(serializer)

    dataRescuer = getRescueClass(countryCode)
    dataParser = getParser(countryCode)
    xmlCreator = getXmlCreator(countryCode)

    corpora_list = glob(inFolder)

    pool = Pool(processes=NUMBER_PROCESSES)
    for filename in corpora_list:
        outputName = outFolder + "/" + os.path.basename(filename) + ".xml"
        pool.apply_async(convertHtmlToXml, args=(filename, outputName, languageCode,
                                                 mappingDictionary, dataRescuer, dataParser, xmlCreator))

    pool.close()
    pool.join()
