###
# Structure Analysis Project (c) by $Authors
#
# This file is part of the Structure Analysis Project, 24/02/2016
#
# Structure Analysis Project is licensed under a dual license: European Union Public License 1.1
# and Commons Attribution 4.0 International (CC BY 4.0) License.
# Academic institutions and research are required to comply with
# CC BY 4.0 Attribution clause and cite the following paper describing the work.
# Please check the license file for more details.
###


import codecs
import os
import logging
import sys

from cam.dictionary.xpathtextdictionary import XPathTextDictionary
from cam.serialisers.fileSerializer import FileSerializer

__author__ = "Ovidiu Serban"
__credits__ = ["Qiang Guo", "Ovidiu Serban", "Juan Tirado", "Eiko Yoneki"]  # alphabetic order
__maintainer__ = "Ovidiu Serban"
__email__ = "ovidiu.serban@cl.cam.ac.uk"

logger = logging.getLogger("FrequencyCSVExport")


# This script converts the frequency dictionary (translated) into a csv file, which can be easily loaded by other
# data processing tools (e.g. Libre Office Calc)
# Can be launched via the external bash script

def loadDictionary(dictionaryName):
    _serializer = FileSerializer(dictionaryName)
    dictionary = XPathTextDictionary(True, 0.7)
    dictionary.load(_serializer, None)
    _serializer.close()
    return dictionary


def exportTextFrequencies(csvFilename, corporaDict, frequencyThreshold):
    with codecs.open(csvFilename, "w+", "utf8") as ofile:
        for (encoding, frequency) in corporaDict.textFrequencies().items():
            if frequency >= frequencyThreshold:
                line = "\";\"".join([" ".join(tokens) for tokens in corporaDict.getTranslationOrText(encoding)])
                line = str(encoding) + ";" + str(frequency) + ";\"" + line + "\""
                ofile.write(line + "\n")


if __name__ == "__main__":
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG,
                        format='%(asctime)s [%(name)s] %(levelname)s %(message)s',
                        datefmt='%a, %d %b %Y %H:%M:%S')

    if len(sys.argv) < 2:
        logger.error("Language must be provided as a parameter ...")
        exit()

    lang = sys.argv[1]
    translatedFile = "res/{}/data.translated.json".format(lang)
    dataFile = "res/{}/data.json".format(lang)
    outFile = "res/{}/data.csv".format(lang)

    logger.info("Using language = %s, translatedFilename=%s dataFilename=%s outFile=%s",
                lang, translatedFile, dataFile, outFile)

    if os.path.isfile(translatedFile):
        _corporaDict = loadDictionary(translatedFile)
        logger.info("Loaded translated file as input ...")
    else:
        _corporaDict = loadDictionary(dataFile)
        logger.info("Loaded data file as input ...")

    exportTextFrequencies(outFile, _corporaDict, 5)
