###
# Structure Analysis Project (c) by $Authors
#
# This file is part of the Structure Analysis Project, 24/02/2016
#
# Structure Analysis Project is licensed under a dual license: European Union Public License 1.1
# and Commons Attribution 4.0 International (CC BY 4.0) License.
# Academic institutions and research are required to comply with
# CC BY 4.0 Attribution clause and cite the following paper describing the work.
# Please check the license file for more details.
###


import re

from bs4 import Tag
from nltk import regexp_tokenize, WordPunctTokenizer
from cam.settings import ApplicationSettings as settings

__author__ = "Ovidiu Serban"
__credits__ = ["Qiang Guo", "Ovidiu Serban", "Juan Tirado", "Eiko Yoneki"]  # alphabetic order
__maintainer__ = "Ovidiu Serban"
__email__ = "ovidiu.serban@cl.cam.ac.uk"

###
# This file contains a series of helper methods to aid the parsing process
# It a collection of generic method definitions
###


regexp_individual_patterns = [
    u'(?:\d+[\-])+\d+',  # phone numbers
    u'(?:\d+[\,\.])+\d*',  # numbers
    u'(?:http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+)',  # urls
    u'(?:[^@|\s]+@[^@]+\.[^@|\s]+)',  # e-mail
    u'\w\'',  # french definite articles, and first person singular pronouns
    u'\w+',  # alpha numeric sequences
    u'\S+'  # non-whitespace characters
]
globalRegexpPattern = u"|".join(regexp_individual_patterns)

wordPunctTokenizerObject = WordPunctTokenizer()


def cleanupText(item):
    result = item.strip()
    result = re.sub("^[MDCLXVI]+[.]([0-9]+[.)])+", " ", result)
    result = re.sub("^([0-9]+[.])*[0-9]+[)]", " ", result)
    result = re.sub("(?i)^section [MDCLXVI]+", " ", result)
    result = re.sub("(?i)^section [0-9]+", " ", result)
    result = re.sub("(^[).;,])|([(]$)", " ", result)
    result = re.sub("\n", " ", result)
    result = re.sub("\t", " ", result)
    result = re.sub("[ ]+", " ", result)
    return result.strip()


def cleanupPath(pathList):
    return [item for item in pathList if item not in ["a"]]


def getTagName(tag: Tag) -> str:
    if settings.PROPERTY_complexPath:
        extraTag = ""
        for e in settings.PROPERTY_complexAttributes:
            if e in tag.attrs:
                extraTag = tag.attrs[e]
                break

        if isinstance(extraTag, list):
            extraTag = ".".join(extraTag)

        if len(extraTag) > 0:
            return tag.name + "." + extraTag.replace(" ", ".")
        else:
            return tag.name
    else:
        return tag.name


def parent_path(tag: Tag) -> list(str()):
    result = []
    for item in tag.parents:
        if item is not None:
            result.insert(0, getTagName(item))
    return result


def xpath(tag, tag_text):
    return parent_path(tag), tag_text


def fixEmail(rawDocument):
    email_fix = re.sub("[<]span[ ]+class[=]([']|[\"])arobas([']|[\"])[>][^<]+?[<][/]span[>]", "@", rawDocument)
    email_fix = re.sub("[<]span[ ]+class[=]([']|[\"])arobas([']|[\"])[/][>]", "@", email_fix)
    return email_fix


def fixEmailInDocument(inputDocument):
    return fixEmail(inputDocument.read())


def _valid(token):
    return token not in [":", ".", ",", "?", "!", "-", "=", "~"]


def regexpTokenize(line):
    tokens = regexp_tokenize(line, pattern=globalRegexpPattern)
    tokens = [token for token in tokens if _valid(token)]
    return tokens


def wordPunctTokenizer(line):
    tokens = wordPunctTokenizerObject.tokenize(line)
    tokens = [token for token in tokens if _valid(token)]
    return tokens


def splitKey(key: str) -> (str, str):
    if key is None:
        return None, None

    sKey = key.rsplit(".", maxsplit=1)
    if len(sKey) < 2:
        return None, key
    else:
        return sKey[0], sKey[1]


def samePath(path1: str, path2: str) -> bool:
    if path1 is None or path2 is None:
        return False
    else:
        return path1 == path2


def sameParent(path1: str, path2: str) -> bool:
    if path1 is None or path2 is None:
        return False
    else:
        if path1.startswith(path2) or path2.startswith(path1):
            return True
        else:
            ppath1, cpath1 = splitKey(path1)
            ppath2, cpath2 = splitKey(path2)
            return samePath(ppath1, ppath2)
