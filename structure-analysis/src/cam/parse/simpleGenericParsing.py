###
# Structure Analysis Project (c) by $Authors
#
# This file is part of the Structure Analysis Project, 24/02/2016
#
# Structure Analysis Project is licensed under a dual license: European Union Public License 1.1
# and Commons Attribution 4.0 International (CC BY 4.0) License.
# Academic institutions and research are required to comply with
# CC BY 4.0 Attribution clause and cite the following paper describing the work.
# Please check the license file for more details.
###


import re
from collections import defaultdict
from bs4 import Tag, NavigableString, Comment, CData, ProcessingInstruction, Declaration

from cam.parse.simpleParsing import cleanupText, cleanupPath, xpath
from cam.settings import ApplicationSettings as settings

__author__ = "Ovidiu Serban"
__credits__ = ["Qiang Guo", "Ovidiu Serban", "Juan Tirado", "Eiko Yoneki"]  # alphabetic order
__maintainer__ = "Ovidiu Serban"
__email__ = "ovidiu.serban@cl.cam.ac.uk"


class GenericParser:
    """
    A generic base parser describing the iteration strategy of the HTML document:
    - the basic strategy is a flat sequential parsing, where the xpaths are processed sequentially
    - a hierarchical parsing strategy, where the xpaths are fed sequentially, but the parsing order is hierarchical
    """

    def traverseDelegate(self, tag, delegateFunction, dataRescuer):
        pass

    def traverseToDictionary(self, tag, frequencyDictionary):
        self.traverseDelegate(tag, frequencyDictionary.add, None)

    @staticmethod
    def _addPath(delegateFunction, xtagpath, text):
        if ":" in text:
            text = re.sub("http:", "http~", text)
            text = re.sub("https:", "https~", text)
            textList = text.split(":")
        else:
            textList = [text]

        for item in textList:
            item = re.sub("http~", "http:", item)
            item = re.sub("https~", "https:", item)
            clean_item = cleanupText(item)
            if len(clean_item) > 0:
                delegateFunction(cleanupPath(xtagpath), clean_item)


class FlatSequentialParser(GenericParser):
    def traverseDelegate(self, tag, delegateFunction, dataRescuer):
        if isinstance(tag, NavigableString) and not isinstance(tag, Comment) and not isinstance(tag, CData) \
                and not isinstance(tag, ProcessingInstruction) and not isinstance(tag, Declaration):
            tag_text = cleanupText(tag.string)
            if len(tag_text) > 0:
                (xtagpath, text) = xpath(tag, tag_text)
                # sometimes the script is misinterpreted as text by the parser
                if "script" not in xtagpath:
                    self._addPath(delegateFunction, xtagpath, text)
        elif isinstance(tag, Tag):
            for item in tag.children:
                if dataRescuer is not None and dataRescuer.isTagRescuable(item):
                    rescuedItem = dataRescuer.rescueTag(item)
                    if len(rescuedItem) > 0:
                        (xtagpath, text) = xpath(item, rescuedItem)
                        self._addPath(delegateFunction, xtagpath, text)
                else:
                    self.traverseDelegate(item, delegateFunction, dataRescuer)


class UniqueSequentialParser(GenericParser):
    def __init__(self):
        self.utagEncoding = defaultdict(int)

    def traverseDelegate(self, tag, delegateFunction, dataRescuer):
        self.traverseParentDelegate([], tag, delegateFunction, dataRescuer)

    def traverseParentDelegate(self, parentPath, tag, delegateFunction, dataRescuer):
        if isinstance(tag, NavigableString) and not isinstance(tag, Comment) and not isinstance(tag, CData) \
                and not isinstance(tag, ProcessingInstruction) and not isinstance(tag, Declaration):
            tag_text = cleanupText(tag.string)
            if len(tag_text) > 0:
                # sometimes the script is misinterpreted as text by the parser
                if "script" not in parentPath:
                    self._addPath(delegateFunction, parentPath, tag_text)
        elif isinstance(tag, Tag):
            localCopy = list(parentPath)
            localCopy.append(self.translateTag(tag))
            for item in tag.children:
                if dataRescuer is not None and dataRescuer.isTagRescuable(item):
                    rescuedItem = dataRescuer.rescueTag(item)
                    if len(rescuedItem) > 0:
                        self._addPath(delegateFunction, parentPath, rescuedItem)
                else:
                    self.traverseParentDelegate(localCopy, item, delegateFunction, dataRescuer)

    def translateTag(self, tag):
        tagName = tag.name
        nextId = self.utagEncoding[tagName]
        self.utagEncoding[tagName] = nextId + 1
        tagName = tagName + "-" + str(nextId)

        if settings.PROPERTY_complexPath:
            extraTag = ""
            for e in settings.PROPERTY_complexAttributes:
                if e in tag.attrs:
                    extraTag = tag.attrs[e]
                    break

            if isinstance(extraTag, list):
                extraTag = ".".join(extraTag)

            if len(extraTag) > 0:
                return tagName + "." + extraTag.replace(" ", ".")
            else:
                return tagName
        else:
            return tagName


# parser builder and custom parsing
customParsers = {
    "cz": UniqueSequentialParser
}


def getParser(countryCode):
    parser = customParsers.get(countryCode, FlatSequentialParser)
    return parser()
