###
# Structure Analysis Project (c) by $Authors
#
# This file is part of the Structure Analysis Project, 24/02/2016
#
# Structure Analysis Project is licensed under a dual license: European Union Public License 1.1
# and Commons Attribution 4.0 International (CC BY 4.0) License.
# Academic institutions and research are required to comply with
# CC BY 4.0 Attribution clause and cite the following paper describing the work.
# Please check the license file for more details.
###


from nltk.metrics.distance import edit_distance

__author__ = "Ovidiu Serban"
__credits__ = ["Qiang Guo", "Ovidiu Serban", "Juan Tirado", "Eiko Yoneki"]  # alphabetic order
__maintainer__ = "Ovidiu Serban"
__email__ = "ovidiu.serban@cl.cam.ac.uk"


def similarity_token(token1: str, token2: str) -> float:
    """
    Edit distance-based similarities for tokens

    :param token1: first token, string
    :param token2: second token, string
    :return: the similarity of the two strings, based on the edit distance measure
    """

    return 1 - (float(edit_distance(token1.lower(), token2.lower())) / max(len(token1), len(token2), 1))


def similarity_sets(set1: tuple, set2: tuple, token_similarity_threshold: float) -> float:
    """
    Edit distance-based similarities for sets, using a token similarity threshold, for the individual tokens

    :param set1: first sets, strings
    :param set2: second sets, strings
    :param token_similarity_threshold
    :return: the similarity of the two sets, based on the edit distance measure
    """

    return 1 - (float(_edit_distance_array(set1, set2, token_similarity_threshold)) / max(len(set1), len(set2), 1))


def len_similarity_sets(set1: tuple, set2: tuple) -> float:
    """
    A length-based similarity for sets

    :param set1: first set
    :param set2: second set
    :return: the similarity of the two sets, based on their length
    """

    min_len = min(len(set1), len(set2))
    max_len = max(len(set1), len(set2))
    return max_len - min_len


def similarity_path(path1: list, path2: list) -> float:
    """
    A path similarity, based on the length of the longest sub-path sequence

    :param path1: first path
    :param path2: second path
    :return: the path similarity
    """

    n = min(len(path1), len(path2))
    count = 0
    for i in range(n):
        if path1[i] == path2[i]:
            count += 1
        else:
            break
    return max(len(path1), len(path2)) - count


def similarityParentPath(path1, path2):
    """
    A path similarity, based on the length of the longest sub-path sequence

    :param path1: first path
    :param path2: second path
    :return: the path similarity
    """
    n = min(len(path1), len(path2))
    count = 0
    for i in range(n):
        if path1[i] == path2[i]:
            count += 1
        else:
            break
    return n - count, count


# copied from nltk.metrics.distance and modified to work with lists/arrays/sets
def _edit_dist_init(len1: int, len2: int) -> list:
    lev = []
    for i in range(len1):
        lev.append([0] * len2)  # initialize 2D array to zero
    for i in range(len1):
        lev[i][0] = i  # column 0: 0,1,2,3,4,...
    for j in range(len2):
        lev[0][j] = j  # row 0: 0,1,2,3,4,...
    return lev


def _edit_dist_step_array(lev, i, j, s1, s2, token_similarity_threshold):
    c1 = s1[i - 1]
    c2 = s2[j - 1]

    # skipping a character in s1
    a = lev[i - 1][j] + 1
    # skipping a character in s2
    b = lev[i][j - 1] + 1
    # substitution
    c = lev[i - 1][j - 1] + (similarity_token(c1, c2) < token_similarity_threshold)

    # transposition
    d = c + 1  # never picked by default

    # pick the cheapest
    lev[i][j] = min(a, b, c, d)


def _edit_distance_array(set1, set2, token_similarity_threshold):
    # set up a 2-D array
    len1 = len(set1)
    len2 = len(set2)
    lev = _edit_dist_init(len1 + 1, len2 + 1)

    # iterate over the array
    for i in range(len1):
        for j in range(len2):
            _edit_dist_step_array(lev, i + 1, j + 1, set1, set2, token_similarity_threshold)
    return lev[len1][len2]


if __name__ == "__main__":
    print(similarity_sets(("a", "aa"), ("bb", "aab"), 0.9))
    print(similarity_token("abcd", "bb"))
    print(edit_distance("abcd", "bb", False))
    print(similarity_path(["a", "h", "h1", "etc"], ["a", "h", "h1", "etc"]))
    print(similarity_path(["a", "h", "h1", "etc"], ["a", "h", "h1"]))
    print(similarity_path(["a", "h", "h1", "etc"], ["a", "h", "h1", "ec"]))
    print(similarity_path(["a", "h", "h1", "etc"], ["a", "h"]))
    print(similarity_path(["a", "h", "h1", "etc"], ["a", "h1", "kk"]))
