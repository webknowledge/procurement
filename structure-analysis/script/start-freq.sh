#!/bin/bash

###
# Structure Analysis Project (c) by $Authors
#
# This file is part of the Structure Analysis Project, 24/02/2016
#
# Structure Analysis Project is licensed under a dual license: European Union Public License 1.1
# and Commons Attribution 4.0 International (CC BY 4.0) License.
# Academic institutions and research are required to comply with
# CC BY 4.0 Attribution clause and cite the following paper describing the work.
# Please check the license file for more details.
###

# author: Ovidiu Serban, ovidiu.serban@cl.cam.ac.uk
# credits: Qiang Guo, Ovidiu Serban, Juan Tirado, Eiko Yoneki  # alphabetic order
# maintainer: Ovidiu Serban


# The start frequency analysis script takes one parameter, the country code and performs the frequency analysis
# This is an iterative process, and depending on the sample size and quality of the data, can take a long time

pythonPath="src/"
pythonExecutionPoint="cam/process/batchFrequencyAnalysis.py"
executionParameters=$1

PYTHONPATH=${pythonPath} python3 -u "${pythonPath}/${pythonExecutionPoint}" ${executionParameters}