# Introduction

This is a particular use case of [MAKI](https://gitlab.com/webknowledge/maki) designed to extract data from 
diverse European public procurement data providers. 

# Get the code
```
git clone https://gitlab.com/webknowledge/procurement.git
```

# Explore
The project has four subprojects each one has its own instructions and readme
file. Explore the subprojects in the following order to have a clear idea
of the iterative process.

1. crawling
2. struture-analysis
3. mitford
4. parsing

# More information

Please refer to the [project web page](www.duckplus.com) for more information.
