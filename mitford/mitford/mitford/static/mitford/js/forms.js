/* Display jqueryui elements*/
		
$(function() {
	$( "#tabs" ).tabs( {collapsible: false});
	$( "#tabs-date" ).tabs({collapsible: false});
	$("#current-xpaths-table").selectable({
		filter:'tr',
  		selected: function(event, ui){
  			var aux = $(ui.selected).find("tr");
  			aux.bgColor = "blue";
    	}		
	});
	
	
	 
	$( "#tabs-date-add" ).button().click(
		function(){
			addDateTabEntry(1);
		});
	
	
	//close the tab
	$('#tabs-date').tabs().delegate( "span.ui-icon-close", "click", function() {
      var panelId = $( this ).closest( "li" ).remove().attr( "aria-controls" );
      $( "#" + panelId ).remove();
      $('#tabs-date').tabs( "refresh" );
    });
	
	$('#tabs-date').tabs().bind( "keyup", function( event ) {
      if ( event.altKey && event.keyCode === $.ui.keyCode.BACKSPACE ) {
        var panelId = $('#tabs-date-add').find( ".ui-tabs-active" ).remove().attr( "aria-controls" );
        $( "#" + panelId ).remove();
        $('#tabs-date').tabs( "refresh" );
      }
    });
	
});

	function closeAllTabs(containerName){
		
		var elements = $("#tabs-date > ul > li");
		for(var i=0;i<elements.length;i++){
			var panel = elements[i];
			panel.remove();
			$('#tabs-date').tabs( "refresh" );
			$("#tabs-date-"+(i+1)).remove();
		}
	}


	function addDateTabEntry(numTabs){
			var tableTemplate="<table class=\"ui-widget ui-widget-content\">"+
		    			"<thead>"+
		    				"<tr class=\"ui-widget-header\">"+ 
		    				"<th>"+
		    					"Option"+
		    				"</th>"+
		    				"<th>"+
		    					"XPath"+
		    				"</th>"+
		    				"</tr>"+
		    			"</thead>"+
		    			"<tbody>"+
		    				"<tr>"+
		    					"<td>String format</td>"+
		    					"<td><label id=\"string_xpath_#{id}\"></label></td>"+
		    				"</tr>"+
		    				"<tr>"+
		    					"<td>Day value</td>"+
		    					"<td><label id=\"day_xpath_#{id}\"></label></td>"+
		    				"</tr>"+
		    				"<tr>"+
		    					"<td>Month value</td>"+
		    					"<td><label id=\"month_xpath_#{id}\"></label></td>"+
		    				"</tr>"+
		    				"<tr>"+
		    					"<td>Year</td>"+
		    					"<td><label id=\"year_xpath_#{id}\"></label></td>"+
		    				"</tr>"+
		    				"<tr>"+
		    					"<td>Time</td>"+
		    					"<td><label id=\"time_xpath_#{id}\"></label></td>"+
		    				"</tr>"+
		    			"</tbody>"+
		    		"</table>";
			var tabTemplate = "<li><a href='#{href}'>#{label}</a> <span class='ui-icon ui-icon-close' role='presentation'>Remove Tab</span></li>";
			
			var currentNumTabs=$( "#tabs-date >ul >li" ).length;
			
			for(var tabCounter=1;tabCounter<=numTabs;tabCounter++){
				var tabId = currentNumTabs+tabCounter;
				var tabTitle = "tabs-date-" + tabId;
				var li = $( tabTemplate.replace( /#\{href\}/g, "#" + tabTitle ).replace( /#\{label\}/g, "Date "+tabId ) );
				$( "#tabs-date" ).find(".ui-tabs-nav").append(li);
				var contentHtml = "<div id='" + tabTitle + "' class='tabber'>"+tableTemplate+"</div>";
				$( "#tabs-date" ).append(contentHtml.replace(/#\{id\}/g,tabId));
			
			}
			$( "#tabs-date" ).tabs("refresh");
		}


/*
Page loader
*/

pageLoader=$("#loader").dialog({
	modal: true,
	autoOpen:false,
	resizable:false,
	closeOnEscape: false
	});

$("#progressbar").progressbar({
		value:false
});


/*
Customizable error message
*/
errorMessage=$( "#error-message" ).dialog({
      		modal: true,
      		autoOpen:false,
      			buttons: {
        			Ok: function() {
          				$( this ).dialog( "close" );
        			}
      			}
    		});

/*
Dialog asking to add new xpaths to a node with some of them.
*/
var dialog_add = $("#dialog-form-add").dialog({
	autoOpen: false,
	resizable: false,
	height: 600,
	width:	600,
	modal: true,
	open: function(){
			var incomingData = $(this).data('toAdd'); 
			var schemaName = incomingData[0];
			var nodeSchema = incomingData[1];
		    var nodeXML = incomingData[2];
		    
		    var theTable = document.getElementById("current-xpaths-table");
		    while(theTable.rows.length>1){
		    	theTable.deleteRow(1);
		    }
		    
		    
		    if(nodeSchema["__xpath__"].length==0){
		    	return;		    	
		    }
		    
		    for(var i=0;i<nodeSchema["__xpath__"].length;i++){
		    	var theCell = theTable.insertRow((i+1)).insertCell(0);
		    	theCell.innerHTML = nodeSchema["__xpath__"][i];
		    }
		    
		},
	buttons: {
		"Add":function(){
			$( this ).dialog( "close" );
			var incomingData = $(this).data('toAdd'); 
			var schemaName = incomingData[0];
			var nodeSchema = incomingData[1];
		    var nodeXML = incomingData[2];
		    
		    
		    if(nodeSchema["type"]=="NullBooleanField"){
				dialog_boolean.data("toConvert",[schemaName,nodeSchema,nodeXML]);
				dialog_boolean.dialog("open");
			}else if(nodeSchema["type"]=="EnumerationField"){
				dialog_enum.data("toConvert",[schemaName,nodeSchema,nodeXML]);
				dialog_enum.dialog("open");
			}else if(nodeSchema["type"]=="DateTimeField"){
				dialog_date.data("toConvert",[schemaName,nodeSchema,nodeXML]);
				dialog_date.dialog("open");
			}else{
				//nodeSchema["__xpath__"].push(nodeXML["xml"]);
				//nodeSchema.tags.push(nodeXML["info"]);
				$(schemaName).treeview('checkNode',nodeSchema);
				$('#tree').treeview('unselectNode',nodeXML);	
			}
		},
		"Remove entry":function(){
			//find selected element and remove
			var incomingData = $(this).data('toAdd');
			var nodeSchema = incomingData[1];
			$( ".ui-selected",this).each(function(){
				var index = $("#current-xpaths-table tr").index(this);
				document.getElementById("current-xpaths-table").deleteRow(index);
				nodeSchema["__xpath__"].splice((index-1),1);
				nodeSchema.tags.splice((index-1),1);
				
			});
			 
		},
		"Uncheck":function(){
			$( this ).dialog( "close" );
			var incomingData = $(this).data('toAdd');
			var schemaName = incomingData[0]; 
			var nodeSchema = incomingData[1];
		    var nodeXML = incomingData[2];
		    nodeSchema.tags=[];
		    delete nodeSchema["__xpath__"];
			if (typeof(nodeSchema["__conversion__"])!="undefined"){
				delete nodeSchema["__conversion__"];
			}
		    $('#tree').treeview('unselectNode',nodeXML);
		},
		"Cancel":function(){
			var incomingData = $(this).data('toAdd');
			var schemaName = incomingData[0]; 
			var nodeSchema = incomingData[1];
		    var nodeXML = incomingData[2];
			$( this ).dialog( "close" );
			$('#tree').treeview('unselectNode',nodeXML);
			$(schemaName).treeview('checkNode',nodeSchema);
		}
	}
});

/*
Message to finish the mapping per field or continuing.
*/
var dialog_confirmation = $("#dialog-form-confirm").dialog({
	autoOpen: false,
	resizable: false,
	height: 200,
	width:	400,
	modal: true,
	buttons: {
		"Continue configuring":function(){
			$( this ).dialog( "close" );
			var schemaName = $(this).data('toConvert')[0];
			var nodeSchema = $(this).data('toConvert')[1];
			
			$(schemaName).treeview('checkNode',nodeSchema);
		},
		"Uncheck":function(){
			$( this ).dialog( "close" );
			var nodeSchema = $(this).data('toConvert');
			nodeSchema.tags=[];
			
			delete nodeSchema["__xpath__"];
			if (typeof(nodeSchema["__conversion__"])!="undefined"){
				delete nodeSchema["__conversion__"];
			}		
		}
	}
});


/*
Dialog for boolean values
*/
var dialog_boolean = $( "#dialog-form-boolean" ).dialog({
	autoOpen: false,
	resizable: false,
	height: 200,
	width: 400,
	modal: true,
	
	buttons: {
		//modify the variable userschoice accordingly to the selected value
		"True": function() {
			$(this).dialog("close");
		    var toConvert=$(this).data('toConvert');
		    var schemaName = toConvert[0];
		    var nodeSchema = toConvert[1];
		    var nodeXML = toConvert[2];        		    
		    
		    //Add the conversion type
		    if(typeof(nodeSchema["__conversion__"])=="undefined"){
				nodeSchema["__conversion__"]={};
			}
			if(typeof(nodeSchema["__xpath__"])=="undefined"){
				nodeSchema["__xpath__"]=[];
			}
			
			nodeSchema["__conversion__"][nodeXML["info"]]=true;
			nodeSchema["__xpath__"].push(nodeXML["xml"]);
			
			if(typeof(nodeSchema.tags)=="undefined"){
				nodeSchema.tags=[];
			}
			nodeSchema.tags.push(nodeXML["xml"]);
		
			
			targetNodes=[];
			$(schemaName).treeview('checkNode',nodeSchema);
			$('#tree').treeview('unselectNode',nodeXML);
   		},
		"False": function() {
		    $(this).dialog("close");
		    var toConvert=$(this).data('toConvert');
		    var schemaName = toConvert[0];
		    var nodeSchema = toConvert[1];
		    var nodeXML = toConvert[2];        		    
		    
		    //Add the conversion type
		    if(typeof(nodeSchema["__conversion__"])=="undefined"){
				nodeSchema["__conversion__"]={};
			}
			if(typeof(nodeSchema["__xpath__"])=="undefined"){
				nodeSchema["__xpath__"]=[];
			}
			if(typeof(nodeSchema.tags)=="undefined"){
				nodeSchema.tags=[];
			}
			
			if(typeof(nodeSchema["__xpath__"])=="undefined"){
				nodeSchema["__xpath__"]=[nodeXML["xml"]];
				nodeSchema.tags.push(infoTags);
			}else{
				nodeSchema["__xpath__"].push(nodeXML["xml"]);
				nodeSchema.tags.push(infoTags);
			}				
			
			nodeSchema["__conversion__"][nodeXML["info"]]=false;
			nodeSchema["__xpath__"].push(nodeXML["xml"]);
			nodeSchema.tags.push(nodeXML["xml"]);
			targetNodes=[];
			$(schemaName).treeview('checkNode',nodeSchema);
			$('#tree').treeview('unselectNode',nodeXML);
	 	}
	}
});

    	
 /*
 Dialog for enumerated values
 */   	
var dialog_enum = $("#dialog-form-enum").dialog({
	autoOpen: false,
	resizable: false,
	height: 400,
	width: 500,
	modal: true,
	buttons:{ 
		"OK": function() {
			var toConvert=$(this).data('toConvert');
		    var nodeSchema = toConvert[0];
		    var nodeXML = toConvert[1];			 
		    addDateTabEntry(1)
			
			var theTable = document.getElementById("enumeration-table");
			var samples={};      				
			//for(var i=0;i<nodeSchema["enum"].length;i++){
			for(var i=0;i<theTable.rows.length;i++){
				var theRow = theTable.rows[i];
				//cell for enumerated
				var e = theRow.cells[0].innerHTML;
				//cell for the value
				var value = theRow.cells[1].childNodes[0].value;
				samples[value]=e;	
			}
			nodeSchema["__conversion__"]=samples;
			nodeSchema["__xpath__"]=nodeXML["xml"];
			
	      					      				
			$(this).dialog("close");
		}
	},
	open: function() {
		var toConvert=$(this).data('toConvert');
		var nodeSchema = toConvert[0];
		//update the options for the enumeration
		updateEnumTable(nodeSchema);
	},
	close: function(){
		//unselect xml
		var toConvert=$(this).data('toConvert');
		var nodeSchema = toConvert[0];
		var nodeXML = toConvert[1];
		$('#tree').treeview('unselectNode',nodeXML);
	
		//restart the menu
		$(this).find("option").remove().end();
	}
});

/*
Dialog for dates
*/
var dialog_date = $("#dialog-form-date").dialog({
	autoOpen: false,
	resizable: false,
	height: 700,
	width: 700,
	modal: true,
	buttons:{ 
		"OK": function() {
			var toConvert=$(this).data('toConvert');
			var shemaName = toConvert[0];
			var nodeSchema = toConvert[1];
			var nodeXML = toConvert[2];		
			
			var conversions = null;
			var ids=["string_check","year_check","month_check","day_check","time_check"];
			var xpaths = ["string_xpath","year_xpath","month_xpath","day_xpath","time_xpath"];
			if (typeof(nodeSchema["__conversion__"])=="undefined"){
				nodeSchema["__conversion__"]=[{}];
			}
			var activeTab = $("#tabs-date").tabs("option","active");
			var selected = $( "#select_date option:selected" ).val();
			if (activeTab==nodeSchema["__conversion__"].length){
				nodeSchema["__conversion__"].push({});
			}
			nodeSchema["__conversion__"][activeTab][selected]=nodeXML["xml"];
			$('#tree').treeview('unselectNode',nodeXML);
			$(this).dialog("close");
		}
	},
	open: function(){
		$("#select_date").selectmenu();
		closeAllTabs();
		var toConvert=$(this).data('toConvert');
		var schemaName = toConvert[0];
		var nodeSchema = toConvert[1];
		var nodeXML = toConvert[2];			 
		document.getElementById("date_label").innerHTML=nodeXML["info"];
		
		var ids=["string","year","month","day","time"];
		var xpaths = ["string_xpath_","year_xpath_","month_xpath_","day_xpath_","time_xpath_"];
		if (typeof(nodeSchema["__conversion__"])!="undefined"){
			var conversions = nodeSchema["__conversion__"];
			addDateTabEntry(conversions.length);
			for (var entry_k=0;entry_k<conversions.length;entry_k++){
				for(var i=0;i<ids.length;i++){
					var theId = xpaths[i]+""+(entry_k+1);
					if(typeof(nodeSchema["__conversion__"][entry_k][ids[i]])!="undefined"){
						document.getElementById(theId).innerHTML=nodeSchema["__conversion__"][entry_k][ids[i]];
					}else{
						document.getElementById(theId).innerHTML="";
					}
				}	
			}
		}else{
			addDateTabEntry(1);	
		}
		$('#tabs-date-1').fadeIn();
		$('#tabs-date').tabs("option","active",0);
		
	},
	close: function(){
		//unselect xml
		var toConvert=$(this).data('toConvert');
		var nodeSchema = toConvert[0];
		var nodeXML = toConvert[1];
		$('#tree').treeview('unselectNode',nodeXML);
	}
}); 


$('#select_contract_type').selectmenu({
	select:function(event,data){
		if(targetNodes.length==0){
			//No selected entries
			return;
		}
			
		var e = document.getElementById("select_contract_type");
		var selected = e.options[e.selectedIndex].value;
			
		if(selected==""){
			return;
		}
		var xmlNode = $('#tree').treeview('getNode',targetNodes[0]);
		var rowIndex = null;
		if(selected=="cft"){
			rowIndex=1;
		}else if(selected=="ca"){
			rowIndex=2;
		}else if(selected=="corriagenda"){
			rowIndex=3;
		}else{
			return;
		}
		var theTable=document.getElementById("contractTypeTable");
		var theRow=theTable.rows[rowIndex];
		var fieldCell = theRow.cells[1];
		fieldCell.innerHTML=xmlNode["xml"];
		var valueCell = theRow.cells[2];
		valueCell.innerHTML=xmlNode["info"];
		$('#tree').treeview('unselectNode',xmlNode);
				
	}
});