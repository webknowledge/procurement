# Introduction
This is a configured version of MITFORD in the context of the Digiwhist project. The current version contains a database schema describing the target database and distinguishes between two different kind of objects: CFT (call for tenders) and CA (contract announcement). MITFORD can be easily deployed in any web server. In order to make easier the deployment, here we present a self-contained version using Django.

# Get the code
```
git clone https://gitlab.com/digiwhist/mitford.git
```
# Setup
Setup an initial environment using python 2.7 or 3.4.
```
virtualenv env
source enb/bin/activate
pip install -r dependencies.txt
```
# Run in Django default server
```python
python manage.py runserver
```

The web page should be accessible in localhost:8000/mitford