from __future__ import unicode_literals

from django.db import models



#Schema version used by MITFORD
VERSION="2015.11.17.01"

'''
Decorator class to use in conjunction with Mitford for the population of the mapping schema.
isMapped indicates if the model has to be included into the showed mapping schema
pointingTable: indicates the name of the variable that has to be ignored in the schema
               because it is propagated from the parent entry.
'''
class MitfordEntry(object):
    def __init__(self,**kwargs):
        self.conf = kwargs
    def __call__(self,cls):
        cls._isMapped=self.conf["isMapped"]
        cls._pointingTable=self.conf["pointingTable"]
        return cls


@MitfordEntry(isMapped=True,pointingTable=None)
class Publication(models.Model):
    '''
    Publication
    '''
    contract = models.ForeignKey('Contract', null=True,verbose_name="Contract id")
    sourceId = models.TextField(null=True,verbose_name="sourceId")
    sourceName = models.TextField(null=True,verbose_name="Source name")
    machineURL = models.TextField(null=True,verbose_name="Machine URL")  
    humanURL = models.TextField(null=True,verbose_name="Human URL")  
    publicationDate = models.TextField(null=True,verbose_name="Publication date")
    dispatchDate = models.TextField(null=True,verbose_name="Dispatch date")
    language = models.TextField(null=True,verbose_name="Language")
    version = models.TextField(null=True,verbose_name="Version")
    valid = models.NullBooleanField(verbose_name="It is valid")
    included = models.NullBooleanField(verbose_name="It is included")
    releaseTag = models.TextField(null=True,verbose_name="Release tag")
    
    class Meta:
        db_table="Publication"


@MitfordEntry(isMapped=True,pointingTable=None)
class Contract(models.Model):
    id = models.IntegerField(primary_key=True)
    assignedContractId = models.TextField(null=True,verbose_name="Assigned contract ID")
    announcementType = models.TextField(null=True,verbose_name="Announcement type")
    country = models.TextField(null=True,verbose_name="Country")
    title = models.TextField(null=True,verbose_name="Title")
    titleEnglish = models.TextField(null=True,verbose_name="English title")
    procedureType = models.TextField(null=True,verbose_name="Procedure type")
    nationalProcedureType = models.TextField(null=True,verbose_name="National procedure type")
    description = models.TextField(blank=True, null=True,verbose_name="Description")
    descriptionEnglish = models.TextField(null=True,verbose_name="English description")
    bidderLimit = models.TextField(null=True,verbose_name="Bidder limitation")
    estimatedValue = models.ForeignKey('Price',null=True,related_name="estimated_value",verbose_name="Estimated value")
    finalValue = models.ForeignKey('Price', null=True,related_name='final_value',verbose_name="Final value")
    type = models.TextField(null=True,verbose_name="Contract type")
    size = models.TextField(null=True,verbose_name="Contract size")
    callForTendersPublicationDate = models.TextField(null=True,verbose_name="Call for tenders date")
    bidDeadlineDate = models.TextField(null=True,verbose_name="Bid deadline")
    docDeadlineDate = models.TextField(null=True,verbose_name="Document deadline")
    documentsPayable = models.NullBooleanField(default=True,verbose_name="Document provision priced")
    documentsPrice = models.ForeignKey('Price',null=True,related_name='doc_price',verbose_name="Price tender procurement")
    documentsLocation = models.ForeignKey("Address",null=True,verbose_name="Document location")
    contractAwardNoticePublicationDate = models.TextField(blank=True, null=True,verbose_name="Contract award notice publication date")
    lastUpdate = models.TextField(blank=True, null=True,verbose_name="Date of last update")
    awardByGroupBuyers = models.NullBooleanField(null=True,verbose_name="Award by group buyers")
    isCentralPurchase = models.NullBooleanField(null=True,verbose_name="Is central purchase")
    buyer = models.ForeignKey('Buyer', null=True,related_name="buyer",verbose_name="Buyer")
    onBehalfOf = models.ForeignKey('Buyer', null=True,related_name="on_behave_of",verbose_name="On behalf of")
    
    parentContractAwardId = models.TextField(null=True,verbose_name="Parent contract id")
    parentContractAwardDate = models.TextField(blank=True, null=True,verbose_name="Parent contract award date")
    parentContractPublicationUrl = models.TextField(null=True,verbose_name="Parent contract publication URL")
    administrator = models.ForeignKey('Body', null=True,related_name="administrator",verbose_name="Administrator")
    supervisor = models.ForeignKey('Body', null=True,related_name="supervisor",verbose_name="Supervisor")
    specificationsCreator = models.ForeignKey('Body', null=True,related_name="specifications",verbose_name="Specifications creator")
    cftVersion = models.TextField(null=True,verbose_name="Call for tenders version")
    caVersion = models.TextField(null=True,verbose_name="Contract award version")
    npwpReason = models.TextField(null=True,verbose_name="Reasons negotiated procedure")
    variantsAccepted = models.NullBooleanField(null=True,verbose_name="Variants accepted")
    deposits = models.TextField(null=True,verbose_name="Deposits")
    personalRequirements = models.TextField(null=True,verbose_name="Personal requirements")
    economicRequirements = models.TextField(null=True,verbose_name="Economic requirements")
    technicalRequirements = models.TextField(null=True,verbose_name="Technical requirement")
    priorNotification = models.NullBooleanField(null=True,verbose_name="Prior notification issued")
    correction = models.NullBooleanField(null=True,verbose_name="Correction issued")
    callForTenders = models.NullBooleanField(null=True,verbose_name="Call for tenders published")
    contractAward = models.NullBooleanField(null=True,verbose_name="Contract award published")
    appealBodyName = models.TextField(null=True,verbose_name="Appeal body name")
    mediationBodyName = models.TextField(null=True,verbose_name="Mediation body name")
    other = models.TextField(null=True,verbose_name="Other")
    coveredByGPA = models.NullBooleanField(verbose_name="Covered by GPA")    
    frameworkByAgreement = models.NullBooleanField(verbose_name="Framework agreement")
    addressOfPerformance = models.ForeignKey('Address', related_name='contractAddressOfPerformance',verbose_name="Address of performance",null=True)
    isDPS = models.NullBooleanField(verbose_name="Is dynamic purchasing system")
    estimatedStartDate = models.TextField(null=True,verbose_name="Estimated start date")
    estimatedCompletionDate = models.TextField(null=True,verbose_name="Estimated completion date")
    awardDecisionDate = models.TextField(null=True,verbose_name="Award decision date")
    contractSignatureDate = models.TextField(null=True,verbose_name="Contract signature date")
    electronicAuctionUsed = models.NullBooleanField(verbose_name="Electronic auction used")
    mainObject = models.TextField(null=True,verbose_name="Main object")
    
    class Meta:
        db_table="Contract"


@MitfordEntry(isMapped=True,pointingTable="contract")
class CourtProceedingsContract(models.Model):
    '''
    Links to information in case, there has been any form of court proceeding related to contract (commenced rather than consluded necessarily)
    '''
    id = models.IntegerField(primary_key=True)
    contract=models.ForeignKey("Contract",null=True,verbose_name="Contract id")
    url=models.TextField(null=True,verbose_name="URL")
    
    class Meta:
        db_table="CourtProceddingsContract"

@MitfordEntry(isMapped=True,pointingTable="contract")
class CourtInterventionsContract(models.Model):
    '''
    links to information in case, there has been a court intervention into the tender/contract, that is violation of has been ruled
    '''
    id = models.IntegerField(primary_key=True)
    contract=models.ForeignKey("Contract",null=True,verbose_name="Contract id")
    url=models.TextField(null=True,verbose_name="URL")
    
    class Meta:
        db_table="CourtInterventionsContract"


@MitfordEntry(isMapped=True,pointingTable="contract")
class AdditionalObjectsContract(models.Model):
    '''
    Additional cpv codes of the subject
    '''
    id = models.IntegerField(primary_key=True)
    contract = models.ForeignKey("Contract",null=True,verbose_name="Contract id")
    cpv = models.TextField(null=True,verbose_name="CPV")
    
    class Meta:
        db_table="AdditionalObjectsContract"

@MitfordEntry(isMapped=True,pointingTable="contract")
class FundingContract(models.Model):
    '''
    Contract funding model
    '''
    id = models.IntegerField(primary_key=True)
    contract = models.ForeignKey("Contract",null=True,verbose_name="Contract id")
    source = models.TextField(null=True,verbose_name="Funding resource type")
    euFund = models.NullBooleanField(verbose_name="EU funded")
    programme = models.TextField(null=True,verbose_name="Programme")
    amount = models.ForeignKey("Price",null=True,verbose_name="Amount")
    proportion = models.TextField(null=True,verbose_name="Proportion")
    
    class Meta:
        db_table="FundingContract"
 

@MitfordEntry(isMapped=True,pointingTable="contract")
class ContractAwardCriteria(models.Model):
    '''
    Contract award criteria
    '''
    id = models.IntegerField(primary_key=True)
    contract = models.ForeignKey("Contract",null=True,verbose_name="Contract id")
    name = models.TextField(null=True,verbose_name="Name")
    weight = models.TextField(null=True,verbose_name="Weight")
    description = models.TextField(null=True,verbose_name="Description")
    priceRelated = models.NullBooleanField(verbose_name="Is price related")
    
    class Meta:
        db_table="ContractAwardCriteria"
 

@MitfordEntry(isMapped=True,pointingTable="contract")
class OtherPublicationsContract(models.Model):
    '''
    Other publications included into a contract
    '''
    id = models.IntegerField(primary_key=True)
    contract = models.ForeignKey("Contract",null=True,verbose_name="Contract")
    publication = models.ForeignKey("Publication",null=True,verbose_name="Publication id")
    
    class Meta:
        db_table="OtherPublicationsContract" 

@MitfordEntry(isMapped=True,pointingTable=None)
class Address(models.Model):
    '''
    Address
    '''
    id = models.IntegerField(primary_key=True)
    rawAddress = models.TextField(null=True,verbose_name="Raw address")
    street = models.TextField(null=True,verbose_name="Street")
    city = models.TextField(null=True,verbose_name="City")
    country = models.TextField(null=True,verbose_name="Country")
    postcode = models.TextField(null=True,verbose_name="Postcode")
    nuts = models.TextField(null=True,verbose_name="Nuts")
    
    class Meta:
        db_table="Address"

@MitfordEntry(isMapped=True,pointingTable=None)
class Price(models.Model):
    '''
    Price
    '''
    id = models.IntegerField(primary_key=True)
    netAmount = models.TextField(null=True,verbose_name="Net amount")
    vat = models.TextField(blank=True, null=True,verbose_name="VAT")
    currency = models.TextField(blank=True, null=True,verbose_name="Currency")
    netAmountEur = models.TextField(null=True,verbose_name="Net amount EU")
    unitPrice = models.TextField(null=True,verbose_name="Unit price")
    unitNumber = models.TextField(null=True,verbose_name="Unit number")
    unitType = models.TextField(null=True,verbose_name="Unit type")
    minPrice = models.TextField(null=True,verbose_name="Min price")
    maxPrice = models.TextField(null=True,verbose_name="Max price")
 
    class Meta:
        db_table="Price"


@MitfordEntry(isMapped=True,pointingTable="lot")
class LotAwardCriteria(models.Model):
    '''
    Lot award criteria
    '''
    id = models.IntegerField(primary_key=True)
    lot = models.ForeignKey("Lot",null=True,verbose_name="Lot id")
    name = models.TextField(null=True,verbose_name="Name")
    weight = models.TextField(null=True,verbose_name="Weight")
    description = models.TextField(null=True,verbose_name="Description")
    priceRelated = models.NullBooleanField(verbose_name="Is price related")
    
    class Meta:
        db_table="LotAwardCriteria"



@MitfordEntry(isMapped=True,pointingTable=None)
class Body(models.Model):
    '''
    Body
    '''
    id = models.IntegerField(primary_key=True)
    bodyId = models.TextField(null=True,verbose_name="Body id")
    idType = models.TextField(null=True,verbose_name="Body type")
    idScope = models.TextField(null=True,verbose_name="Id scope")
    bodyName = models.TextField(null=True,verbose_name="Body name")
    bodyAddress = models.ForeignKey('Address',null=True,verbose_name="Address")
    email = models.TextField(null=True,verbose_name="Email")
    url = models.TextField(null=True,verbose_name="URL")
    contactPoint = models.TextField(null=True,verbose_name="Contact point")
    contactName = models.TextField(null=True,verbose_name="Contact name")
    contactPhone = models.TextField(null=True,verbose_name="Contact phone")
    
 
    class Meta:
        db_table="Body"


@MitfordEntry(isMapped=True,pointingTable="contractId")
class Lot(models.Model):
    '''
    Lot
    '''
    id = models.IntegerField(primary_key=True)
    contractId = models.ForeignKey("Contract",null=True,verbose_name="Contract id")
    assignedContractId = models.TextField(null=True,verbose_name="Assigned contract ID")
    lotNumber = models.TextField(null=True,verbose_name="Lot number")
    lotTitle = models.TextField(null=True,verbose_name="Lot title")
    lotTitleEnglish = models.TextField(null=True,verbose_name="English lot title")
    description = models.TextField(null=True,verbose_name="Description")
    descriptionEnglish = models.TextField(null=True,verbose_name="English description")
    lotStatus = models.TextField(null=True,verbose_name="Lot status type")
    mainObject = models.TextField(null=True, db_column='mainObject',verbose_name="Main object key")
    estimatedValue = models.ForeignKey('Price', null=True,related_name="lot_estimated_value",verbose_name="Estimated value")
    finalValue = models.ForeignKey('Price', null=True,related_name="lot_final_value",verbose_name="Final value")
    addressOfPerformance = models.ForeignKey('Address', related_name='lotAddressOfPerformance',verbose_name="Address of performance",null=True)
    estimatedStartDate = models.TextField(null=True,verbose_name="Estimated start date")
    estimatedCompletionDate = models.TextField(null=True,verbose_name="Estimated completion date")
    awardDecisionDate = models.TextField(null=True,verbose_name="Award decision date")
    contractSignatureDate = models.TextField(null=True,verbose_name="Contract signature date")
    completionDate = models.TextField(null=True,verbose_name="Completion date")
    cancellationDate = models.TextField(blank=True, null=True,verbose_name="Cancellation date")
    cancellationReason = models.TextField(null=True,verbose_name="Cancellation reason")
    electronicAuctionUsed = models.NullBooleanField(verbose_name="Electronic auction used")
    frameworkAgreement = models.NullBooleanField(verbose_name="Framework agreement")
    estimatedWinnersInFA = models.TextField(null=True,verbose_name="Estimated number of winners in framework agreement")
    isDPS = models.NullBooleanField(verbose_name="Is dynamic purchasing system")
    coveredByGPA = models.NullBooleanField(verbose_name="Covered by GPA")
    eligibilityCritera = models.TextField(null=True,verbose_name="Eligibility criteria")
    winningBid = models.ForeignKey('WinningBid',null=True,verbose_name="Winning bid",related_name="winningBid")
    bidsCount = models.TextField(null=True,verbose_name="Bid count")
    validBidsCount = models.TextField(null=True,verbose_name="Valid bids count")
    electronicBidsCount = models.TextField(null=True,verbose_name="Electronic bids count")

    class Meta:
        db_table="Lot"

@MitfordEntry(isMapped=True,pointingTable="lot")
class FundingLot(models.Model):
    '''
    Funding lots
    '''
    id = models.IntegerField(primary_key=True)
    lot=models.ForeignKey("Lot",null=True,verbose_name="Lot")
    source = models.TextField(null=True,verbose_name="Funding resource type")
    euFund = models.NullBooleanField(verbose_name="EU funded")
    programme = models.TextField(null=True,verbose_name="Programme")
    amount = models.ForeignKey('Price',null=True,verbose_name="Amount")
    proportion = models.TextField(null=True,verbose_name="Proportion")
 
    class Meta:
        db_table="FundingLot"
 

@MitfordEntry(isMapped=True,pointingTable="lot")
class LotAdditionalObjects(models.Model):
    '''
    Lot additional objects
    '''
    id = models.IntegerField(primary_key=True)
    lot=models.ForeignKey("Lot",null=True,verbose_name="Lot")
    cpv = models.TextField(null=True,verbose_name="CPV")
 
    class Meta:
        db_table="LotAdditionalObjects"

 
@MitfordEntry(isMapped=True,pointingTable=None)
class WinningBid(models.Model):
    '''
    Winning bids
    '''
    id = models.IntegerField(primary_key=True)
    wasInRequestedQuality = models.NullBooleanField(verbose_name="It was in requested quality")
    wasFinishedOnTime = models.NullBooleanField(verbose_name="It was finished on time")
    wasForEstimatedValue = models.NullBooleanField(verbose_name="It was for estimated value")    
    isSubcontracted = models.NullBooleanField(verbose_name="Is sub contracted")
    subcontractedProportion = models.TextField(null=True,verbose_name="Subcontracted proportion")
    finalValue = models.ForeignKey('Price',null=True,verbose_name="Final value")

    bidPrice = models.ForeignKey('Price',null=True,verbose_name="Bid",related_name="bidPrice")
    wasDisqualified = models.NullBooleanField(verbose_name="It was disqualified")
    disqualificationReason = models.TextField(null=True,verbose_name="Disqualified reason")
    bidder = models.ForeignKey('Bidder',null=True,verbose_name="Bidder")
    
    
    class Meta:
        db_table="WinningBid"
        

@MitfordEntry(isMapped=True,pointingTable="winningBid")
class AppendicesAgreement(models.Model):
    '''
    List of appendix documents per winning bid
    '''
    id = models.IntegerField(primary_key=True)
    winningBid = models.ForeignKey('WinningBid',null=True,verbose_name="Winning bid")
    document = models.ForeignKey('Document',null=True,verbose_name="Document")
      
    class Meta:
        db_table="AppendicesAgreement"
          

@MitfordEntry(isMapped=True,pointingTable="winningBid") 
class AmendmentsAgreement(models.Model):
    '''
    List of amendment documents per winning bid
    '''
    id = models.IntegerField(primary_key=True)
    winningBid = models.ForeignKey('WinningBid',null=True,verbose_name="Winning bid")
    document = models.ForeignKey('Document',null=True,verbose_name="Document")
          
    class Meta:
        db_table="AmendmentsAgreement"

 
@MitfordEntry(isMapped=True,pointingTable="contract")
class AssignedAnnouncementIds(models.Model):
    '''
    List of assigned ids per announcement
    '''
    id = models.IntegerField(primary_key=True)
    contract = models.ForeignKey('Contract', null=True)
    assignedAnnouncementId = models.TextField(null=True,verbose_name="Assigned ID")
 
    class Meta:
        db_table="AssignedAnnouncementIds"
 
@MitfordEntry(isMapped=True,pointingTable="contract") 
class AssignedContractIds(models.Model):
    '''
    List of assigned contract ids
    '''
    id = models.IntegerField(primary_key=True)
    contract = models.ForeignKey('Contract',null=True,verbose_name="Contract ID")
    assignedContractId = models.TextField(null=True,verbose_name="Assigned ID")
  
    class Meta:
        db_table="AssignedContractIds"
 
@MitfordEntry(isMapped=True,pointingTable=None)
class Bidder(models.Model):
    '''
    Bidder
    '''
    id = models.IntegerField(primary_key=True)
    isConsortium = models.NullBooleanField(verbose_name="It is consortium")
    body = models.ForeignKey("Body",null=True,verbose_name="Body")
    
    class Meta:
        db_table="Bidder"


@MitfordEntry(isMapped=True,pointingTable="bidder")
class SubcontractorsBidder(models.Model):
    '''
    Subcontractors per bidder
    '''
    id = models.IntegerField(primary_key=True)
    bidder = models.ForeignKey('Bidder',null=False,verbose_name="Bidder")
    body = models.ForeignKey('Body',null=False,verbose_name="Body")
      
    class Meta:
        db_table="SubcontractorsBidder"


@MitfordEntry(isMapped=True,pointingTable="bidder")          
class DocumentsBidder(models.Model):
    '''
    Document per bidder
    '''
    id = models.IntegerField(primary_key=True)
    bidder = models.ForeignKey('Bidder',null=False,verbose_name="Bidder")
    document = models.ForeignKey('Document',null=False,verbose_name="Document")
      
    class Meta:
        db_table="DocumentsBidder"
 

@MitfordEntry(isMapped=True,pointingTable="lot")
class Bid(models.Model):
    '''
    Bid
    '''
    id = models.IntegerField(primary_key=True)
    lot=models.ForeignKey('Lot',null=True,verbose_name="Lot")
    bidPrice = models.ForeignKey('Price',null=True,verbose_name="Bid")
    wasDisqualified = models.NullBooleanField(verbose_name="It was disqualified")
    disqualificationReason = models.TextField(null=True,verbose_name="Disqualified reason")
    bidder = models.ForeignKey('Bidder',null=True,verbose_name="Bidder")
 
    class Meta:
        db_table="Bid"

@MitfordEntry(isMapped=True,pointingTable="bid")
class BidDocuments(models.Model):
    '''
    One bid can have several documents
    '''
    id = models.IntegerField(primary_key=True)
    bid = models.ForeignKey('Bid',null=True,verbose_name="Bid")
    document = models.ForeignKey('Document',null=True,verbose_name="Document")
 
    class Meta:
        db_table="BidDocuments"
 

@MitfordEntry(isMapped=True,pointingTable=None)
class Buyer(models.Model):
    '''
    Buyer
    '''
    id = models.IntegerField(primary_key=True)
    mainActivity = models.TextField(null=True,verbose_name="Buyer activity type")
    buyerType = models.TextField(null=True,verbose_name="Buyer type")
    isPublic = models.NullBooleanField(verbose_name="It is public")
    isSubsidized = models.NullBooleanField(verbose_name="Is subsidized")
    isSectoral = models.NullBooleanField(verbose_name="Is sectoral")
    body = models.ForeignKey("Body",null=True,verbose_name="Body")

    class Meta:
        db_table="Buyer"


@MitfordEntry(isMapped=True,pointingTable="contract")
class Document(models.Model):
    '''
    Document
    '''
    id = models.IntegerField(primary_key=True)
    contract = models.ForeignKey('Contract',null=True,related_name="contractId",verbose_name="Document")
    title = models.TextField(null=True,verbose_name="Title")
    type = models.TextField(null=True,verbose_name="Document type")
    url = models.TextField(null=True,verbose_name="URL")
    publicationDatetime = models.TextField(null=True,verbose_name="Publication date")
    signatureDate = models.TextField(null=True,verbose_name="Signature date")
    
    class Meta:
        db_table="Document"
 

@MitfordEntry(isMapped=True,pointingTable="document")
class OtherVersionsDocument(models.Model):
    '''
    Other versions of document (the actual document is recognized by highest publicationDateTime
    '''
    id = models.IntegerField(primary_key=True)
    document = models.ForeignKey('Document',null=False,related_name="documentId",verbose_name="Document")
    otherVersion = models.ForeignKey('Document',null=False,related_name="otherVersion",verbose_name="Other version id")
    
    class Meta:
        db_table="OtherVersionsDocument"

 

@MitfordEntry(isMapped=True,pointingTable="winningBid")
class WinningBidPayments(models.Model):
    '''
    Payments
    '''
    id = models.IntegerField(primary_key=True)
    winningBid = models.ForeignKey('WinningBid',null=True,verbose_name="Bid")
    paymentD = models.TextField(null=True,verbose_name="Payment date")
    amount = models.ForeignKey('Price',null=True,verbose_name="Amount")
    
    class Meta:
        db_table="WinningBidPayment"
 

@MitfordEntry(isMapped=True,pointingTable="contractId")
class RelatedAnnouncementIds(models.Model):
    '''
    List of announcements per contract
    '''
    id = models.IntegerField(primary_key=True)
    contractId = models.ForeignKey('Contract', null=True,verbose_name="Contract ID")
    relatedAnnouncement = models.TextField(null=True,verbose_name="Related announcement ID")
 
    class Meta:
        db_table="RelatedAnnouncementIds"

 

@MitfordEntry(isMapped=True,pointingTable="bidderId")
class SubContractorsBodies(models.Model):
    '''
    Body for subcontractors
    '''
    id = models.IntegerField(primary_key=True)
    bidderId = models.ForeignKey('Bidder',null=False,verbose_name="Bidder")
    bodyId = models.ForeignKey('Body',null=False,verbose_name="Body")
 
    class Meta:
        db_table="SubContractorsBodies"
         
@MitfordEntry(isMapped=True,pointingTable="bidderId")         
class SubContractorsDocuments(models.Model):
    '''
    Document of subcontractors
    '''
    id = models.IntegerField(primary_key=True)
    bidderId = models.ForeignKey('Bidder',null=False,verbose_name="Bidder")
    documentId = models.ForeignKey('Document', null=False,verbose_name="Body")
    
    class Meta:
        db_table="SubContractorsDocuments"

@MitfordEntry(isMapped=False,pointingTable=None)
class Corriagenda(models.Model):
    '''
    Publication of corriagenda. Normally data is not very well structured, so we 
    basically store the publication date, the contract reference and the corrections.
    '''
    documentRef=models.TextField(null=True,verbose_name="Document referred")
    publicationDate=models.TextField(null=True,verbose_name="Publication date")
    correction=models.TextField(null=True,verbose_name="Correction")
    
    class Meta:
        db_table="Corriagenda"
