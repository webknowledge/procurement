'''
The code contained in this module uses the mappings to automatically parse the crawled data into our
database schema. This code can be used as part of the pipeline to be done after data crawling.
'''

from django.conf import settings
import json
import os
from django.apps import apps
from lxml import etree

from io import BytesIO
import logging
import glob
import traceback
from django.db.models.fields import DateField,DateTimeField,TextField,CharField,\
    NullBooleanField, EmailField, FloatField, URLField,IntegerField,DecimalField
from django.db.models import ForeignKey, OneToOneField,Model
from django.db.models import Max
from django.db import connections
from parsing.models import FailedProcessingFiles,IgnoredProcessingFiles
from prototype.models import VERSION

import codecs

from decimal import Decimal
import datetime
import re
import time # for profiling

import contextlib 

import string

import multiprocessing

import re

punct_remover = re.compile('[%s]' % re.escape(string.punctuation))


import string

# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")


global dbLock 
dbLock = multiprocessing.Lock()

global idLock
idLock = multiprocessing.Lock()

#This is used for the decimal formatting
FOURPLACES = Decimal(10) ** -4
MAX_DECIMAL = (Decimal(10) ** 20)+1

NAMESPACE_REMOVAL='''<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="no"/>

<xsl:template match="/|comment()|processing-instruction()">
    <xsl:copy>
      <xsl:apply-templates/>
    </xsl:copy>
</xsl:template>

<xsl:template match="*">
    <xsl:element name="{local-name()}">
      <xsl:apply-templates select="@*|node()"/>
    </xsl:element>
</xsl:template>

<xsl:template match="@*">
    <xsl:attribute name="{local-name()}">
      <xsl:value-of select="."/>
    </xsl:attribute>
</xsl:template>
</xsl:stylesheet>
'''

def unwrap_self_process_item(arg, **kwarg):
    '''
    This is a wrapping function to run the MappingProcessor
    as a multiprocessing function
    '''
    logger.debug("Unwrapping was called with %d elements"%len(arg))
    return MappingProcessor.taskProcessing(*arg,**kwarg)



'''
This decorator makes possible to set values without being saved into
the database.
'''
@contextlib.contextmanager
def allow_unsaved(model, field):
    model_field = model._meta.get_field(field)
    saved = model_field.allow_unsaved_instance_assignment
    model_field.allow_unsaved_instance_assignment = True
    yield
    model_field.allow_unsaved_instance_assignment = saved





def initializeIds(ids,source):
    '''
    We initialize the ids structure
    '''
    
    myapp = apps.get_app_config('prototype')
    for m in myapp.get_models():
        found=False
        aux=None
        for e in m._meta.get_fields():
            tokens=str(e).split('.')
            if tokens[-1]=="id":
                #logger.debug("---------------->Found id "+str(e))
                found=True
                break
        if found:
            aux = m.objects.using(source).all().aggregate(Max('id'))['id__max']
            
        if aux!=None:
            ids[m]=aux+1
        else:
            ids[m]=0
        logger.debug("Set "+str(m)+" id to "+str(ids[m]))
    


def constructInsertionList():
    '''
       We construct a insertion list to avoid collisions.
       This should be generated automatically in order to be
       extensively applied to other solutions
    '''
    myapp=apps.get_app_config("prototype")
    insertionOrder=[]
    insertionOrder.append(myapp.get_model("Corriagenda"))
    insertionOrder.append(myapp.get_model("Price"))
    insertionOrder.append(myapp.get_model("Address"))
    insertionOrder.append(myapp.get_model("Body"))
    insertionOrder.append(myapp.get_model("Buyer"))
    insertionOrder.append(myapp.get_model("Contract"))
    insertionOrder.append(myapp.get_model("Publication"))
    insertionOrder.append(myapp.get_model("RelatedAnnouncementIds"))
    insertionOrder.append(myapp.get_model("ContractAwardCriteria"))
    insertionOrder.append(myapp.get_model("AdditionalObjectsContract"))
    insertionOrder.append(myapp.get_model("AssignedContractIds"))
    insertionOrder.append(myapp.get_model("AssignedAnnouncementIds"))
    insertionOrder.append(myapp.get_model("FundingContract"))
    insertionOrder.append(myapp.get_model("CourtProceedingsContract"))
    insertionOrder.append(myapp.get_model("CourtInterventionsContract"))
    insertionOrder.append(myapp.get_model("OtherPublicationsContract"))
    insertionOrder.append(myapp.get_model("OtherVersionsDocument"))

    insertionOrder.append(myapp.get_model("Bidder"))
    insertionOrder.append(myapp.get_model("Bid"))

    insertionOrder.append(myapp.get_model("WinningBid"))
    
    insertionOrder.append(myapp.get_model("WinningBidPayments"))
    insertionOrder.append(myapp.get_model("AppendicesAgreement"))              

    insertionOrder.append(myapp.get_model("AmendmentsAgreement"))


    insertionOrder.append(myapp.get_model("Lot"))
    insertionOrder.append(myapp.get_model("LotAwardCriteria"))
    insertionOrder.append(myapp.get_model("FundingLot"))
    insertionOrder.append(myapp.get_model("LotAdditionalObjects"))
    
    insertionOrder.append(myapp.get_model("SubcontractorsBidder"))
    insertionOrder.append(myapp.get_model("SubContractorsBodies"))
    insertionOrder.append(myapp.get_model("SubContractorsDocuments"))
    insertionOrder.append(myapp.get_model("Document"))
    insertionOrder.append(myapp.get_model("OtherVersionsDocument"))
    insertionOrder.append(myapp.get_model("BidDocuments"))
    
    return insertionOrder

insertionOrder = constructInsertionList()

manager = multiprocessing.Manager()


class DBPool(dict):
    '''
    Shared db pool among processes. This class is basically a wrapper
    '''

    objectPool=None

    def __init__(self,local=False):
        self.local=local

        if local:
            logger.debug("Created a local DB pool")
            self.objectPool = {}
        else:
            logger.info("Created a shared DB pool")
            self.objectPool=manager.dict()
        
        self.publications=[]
        self.savepointObjects={}
        self.initializePool()
    
    def extend(self,k,v):
        '''
        Extend the list using locks to assure thread safe operations
        '''
        old = self.objectPool[k]
        old.extend(v)
        self.objectPool[k]=old        
        
    
    def __setitem__(self,key,value):
        if self.objectPool !=None:
            #traceback.print_stack()
            #  print("---------------------------------------->")
            self.objectPool[key]=value

        
        
    def __getitem__(self, key):
        return self.objectPool[key]
        
    def __iter__(self):
        return iter(self.objectPool)
    
    def __delitem__(self, key):
        del self.objectPool[key]
         
    def clear(self):
        self.objectPool.clear()
        
    def has_key(self,key):
        return self.objectPool.has_key(key)
    
    def pop(self,k,d=None):
        self.objectPool.pop(k,d)
    
    def update(self,*args,**kwargs):
        return self.objectPool.update(*args,**kwargs)
    
    def keys(self):
        return self.objectPool.keys()
    
    def values(self):
        return self.objectPool.values()
    
    def items(self):
        return self.objectPool.items()
    
    def __cmp__(self, d):
        return cmp(self.objectPool, d)

    def __contains__(self, item):
        return item in self.objectPool

    def __unicode__(self):
        return unicode(repr(self.objectPool))
        
    
    def initializePool(self):
        '''
        Initialize the object pool with one entry per available model:
        modelA: [objectA1,objectA2...]
        modelB: [objectB1,objectB2...]
        '''
        logger.debug("Initialize pool local "+str(self.local))
        
        myapp = apps.get_app_config('prototype')
        for m in myapp.get_models():
            #logger.debug("-Add entry "+str(m))
            self.objectPool[m]=[]
            self.savepointObjects[m]=0

        logger.debug("Initialized objects pool with "+str(len(self.objectPool.keys()))+" models")
        
    
    
    def clearPools(self):
        for m in self.objectPool.keys():
            self.objectPool[m]=[]
            self.savepointObjects[m]=0
        self.poolLength=0
        
            
        
    def updateSavePoints(self):
        '''
        After every successful parsing we update the saving points
        '''
        for k,v in self.objectPool.items():
            self.savepointObjects[k]=len(v)
            
    def rollBack(self):
        '''
        Parsing one entry was a failure, we have to rollback
        '''
        logger.warning("Rollback called")
        for m,index in self.savepointObjects.items():
            if len(self.objectPool[m]) != index:
                logger.warning(str(m)+" shrink from "+str(len(self.objectPool[m]))+" to "+str(index))
            del self.objectPool[m][index:]
            self.savepointObjects[m]= len(self.objectPool[m])

class MappingProcessor(object):
    '''
    Wrapper for multiprocessing mapping based processing
    '''
    def __init__(self):

        xslt_doc = etree.XML(NAMESPACE_REMOVAL)
        self.namespace_cleaner = etree.XSLT(xslt_doc)
        self.path=settings.MAPPING_FOLDER
        self.cache=None
        #structures to control the objects and references to create

    def getGlobalId(self,cls,size):
        '''
        Get a list of ids for the given class cls
        '''

        with idLock:
            start = self.ids[cls]
            end = self.ids[cls]+size
            #return a reversed list
            v = list(reversed(range(start,end)))
            self.ids[cls]=end
        
        return v
    
    def getSingleGlobalId(self,cls):
        with idLock:
            v = self.ids[cls]
            self.ids[cls]=v+1
        return v
    
    def createLocalIdsPool(self):
        '''
        Create a local pool that is synchronized with the shared pool of ids.
        '''     
        self.localIds = {}
        for m in insertionOrder:
            self.localIds[m] = self.getGlobalId(m,2000)
    
    def getLocalId(self,cls):
        if len(self.localIds[cls])==0:
            self.localIds[cls]=self.getGlobalId(cls, 2000)
        return self.localIds[cls].pop()
            
            

    def loadMapping(self,spiderName):
        '''
        Load a mapping according to the MAPPING_FOLDER and the
        spider name.
        '''
        toReturn = None
        filePath = os.path.join(self.path,spiderName+"_mapping.json")
        logger.info("Read mapping from "+filePath)
        with codecs.open(filePath,"r",'utf-8-sig') as reader:
            toReturn = json.load(reader)
        return toReturn                

    def process_item(self,item,source,theCrawlingItem):
        '''
        This function is invoked after uncompressing a ted tgz archive into
        the temporary folder. This function runs the parser for each of the
        available xml files in the resulting folder.
        '''
        mapping=self.loadMapping(source)
        logger.info("Use a mapping for DB version: "+mapping["version"])
        
        if not "version" in mapping.keys():
            logger.error("Impossible to determine the mapping version")
            return item            
        
        if mapping["version"]!=VERSION:
            logger.error("The mapping version"+mapping["version"]+" does not match the current DB version ("+VERSION)
            return item

        #Shared dictionary of ids
        ids=manager.dict()
        initializeIds(ids,source)
        self.ids = ids
        self.globalPool=DBPool(local=False)
        self.publications=[]
        
        initialTime = time.time()
                     
        #Construct the list of files to process
        sharedQueue = manager.Queue()

        for pathEntry,archiveId,url in item:

            if os.path.isdir(pathEntry):
                for level1 in glob.glob(pathEntry+"/*"):
                    sharedQueue.put((level1,archiveId))
            else:
                sharedQueue.put((pathEntry,archiveId,url))
                
        logger.info("There is a total of: "+str(sharedQueue.qsize())+" elements in the queue")
        arguments = (self,mapping,source,ids,sharedQueue,)
        
        num_procs = multiprocessing.cpu_count()-2
        #num_procs = 1

        
        if num_procs < 0:
            num_procs=1
        
        logger.info("Instantiate pool with "+str(num_procs)+" processes")
        pool=multiprocessing.Pool(num_procs)
        
        input_arguments = [arguments]*num_procs
        
        connections.close_all()

        logger.debug("Run multiprocess task...")
        pool.map(unwrap_self_process_item, input_arguments)


        pool.close()
        pool.join()

        finalTime = time.time()
        logger.info("Elapsed time: "+str(finalTime-initialTime))
        #theCrawlingItem = item["crawlingItem"]
        theCrawlingItem.completed = True
        theCrawlingItem.processed_date = datetime.datetime.now()
        theCrawlingItem.save(using=source)

        return item
    
    
    def taskProcessing(self,mapping,source,ids,queue):
        '''
        Wrapper for multiprocessing task.
        '''
        logger.debug("Time to process task!")
        connections.close_all()
        self.ids = ids
        self.objectPool = DBPool(local=True)
        self.createLocalIdsPool()
        try:
            path,archivalId,url = queue.get_nowait()
            #Update the item info
            self.itemInfo = [path,archivalId,url]
        except Exception:
            logger.debug("Worker "+str(os.getpid())+" found and empty queue    ")
            return
        counter=0
                    
        while path!=None:
            logger.debug("Worker "+str(os.getpid())+" -> "+path)
            self.fileProcessing(path, mapping, source, archivalId,url)
            try: 
                path,archivalId,url = queue.get_nowait()
                self.itemInfo = [path,archivalId,url]
            except Exception:
                #Empty queue
                #logger.info("Worker "+str(os.getpid())+" finally did "+str(counter)+" entries")
                break

            counter+=1


        return
            
   
    def fileProcessing(self,path,mapping,source,archivalId,url):
        #Parse the XML
        counter=0
        ignored=0
        xmlObject =None
        correctlyParsed=False
        try:
            with codecs.open(path,'r','utf-8') as reader:
                s=reader.read()
                xmlObject = etree.fromstring(s.encode('utf-8'))
        except Exception as e:
            correctlyParsed=False
            parsingInfo=str(e)
            logger.error("Impossible to parse XML file %s"%path)
            logger.error("Reason: [%s]"%str(e))

        xslt_doc = etree.XML(NAMESPACE_REMOVAL)
        self.namespace_cleaner = etree.XSLT(xslt_doc)

        if xmlObject != None:

            xmlObject = self.namespace_cleaner(xmlObject)
                    
            parsingStartTime=time.time()
            
            #instantiate a cache
            if self.cache!=None:
                del self.cache
            self.cache={}
            try:       
                correctlyParsed,parsingInfo = self.parseXML(source,path,mapping,xmlObject)
            except Exception as e:
                correctlyParsed = False
                parsingInfo = str(e)
                traceback.print_exc()
            parsingEndTime=time.time()
            elapsedTime = parsingEndTime-parsingStartTime 
            
            if elapsedTime>25:
                logger.info("Parsing "+path+" took "+str(elapsedTime)+" longer than 25 seconds.")
        else:
            correctlyParsed = False
            parsingInfo = "Impossible to read xml: "+path
            

        if not correctlyParsed:
            if not "Ignored" in parsingInfo and archivalId!=None:
                logger.error("Failed parsing for "+path)
                logger.error(parsingInfo)
                failedParsing = FailedProcessingFiles(archival_id=archivalId,
                                                      source=source,
                                                      path=path,
                                                      processed_date=datetime.datetime.now(),
                                                      reason=parsingInfo)      
                failedParsing.save(using=source)
                self.objectPool.clearPools()

               
            elif archivalId!=None:
                logger.debug("Ignored processing for "+path)
                ignored+=1
                ignoredParsing = IgnoredProcessingFiles(archival_id=archivalId,
                                                      source=source,
                                                      path=path,
                                                      processed_date=datetime.datetime.now(),
                                                      reason=parsingInfo)      
                ignoredParsing.save(using=source)
                

                logger.debug("Error parsing "+path+": "+str(parsingInfo))
           
            counter+=1
        else:
            logger.debug("Dump results into database")
            try:
                self.localContractDumper(source)
            except Exception as e:
                logger.error("There was one problem while writing "+path+" to database:")
                logger.error(str(e))
                
                
        return counter,ignored
            
            
    def contractDumper(self,source):
        '''
        We store all the objects in a pool and this is dumped into a single
        transaction in order to assure higher speed.
        
        The objectPool contains the objects in order of insertion so no dependencies
        will stop the insertion process.
        
        The contract dumper is currently assuming that no errors will occur during the bulk_create
        '''
                
        
        itemsCounter=0
        for m in insertionOrder:
            pool = self.globalPool[m]
            if len(pool)>0:
                #logger.info("Dump queue for model "+str(m)+" with "+str(len(pool)))
                m.objects.using(source).bulk_create(pool)
                itemsCounter+=len(pool)
        self.globalPool.clearPools()
                
        
       
    def localContractDumper(self,source):
        itemsCounter=0
        for m in insertionOrder:
            pool = self.objectPool[m]
            if len(pool)>0:
                logger.debug("Dump from pool "+str(m))
                m.objects.using(source).bulk_create(pool)
                itemsCounter+=len(pool)
        self.objectPool.clearPools()

    
    def determineContractType(self,xmlParser,conversionData):
        '''
        It determines what type of contract we have to parse according to the expert parameters.
        '''
        toReturn = None
        for key,value in conversionData.items():
            
            #found = self.findXpath(xmlParser,value["__xpath__"])
            found=self.getContentXPathList(xmlParser,value["__xpath__"],0,None,1,None)
            logger.debug("For contract distinctive entry %s we find %s"%(value["__xpath__"],found))

            if found!=None:
                if type(found)==list:
                    found = found[0].lower()
                else:
                    found = found.lower()
                logger.debug("Found contract type: %s"%found)
                
                isThere=False
                for i in value["__values__"]:
                    if i.lower()==found:
                        isThere=True
                        logger.debug("Found "+i)
                        return key

        return toReturn

        
    
    def parseXML(self,source,path,mapping,obj):
        '''
        For a given data object we run the parser using the existing
        mapping schema.
        It returns a boolean indicating the parsing result and a string
        with the corresponding error in case of any parsing problem
        '''
        logger.debug("Process XML file "+path)
        
        #determine if this is a CFT or a CA
        contractType=self.determineContractType(obj,mapping["contractTypeSelection"])
        #if contractType!="ca" and contractType!="cft" and contractType!="corriagenda":
        
        
        #use the CFT or CA mapping accordingly
        k=None
        if contractType=="ca":
            k="prototype.Publication"
        elif contractType=="cft":
            k="prototype.Publication"    
        elif contractType=="corriagenda":
            k="prototype.Corriagenda"
             
        if k==None:
            if contractType != None:
                return False,"Ignored "+contractType
            else:
                
                return False,"Ignored unknown contract type"
        else:
            logger.debug("Contrat type found: "+contractType)
#         
#             
#         logger.debug("Process a document of type ["+contractType+"]")
#         
#         v=mapping[contractType][k]
        
        #instantiate class
        v=mapping[contractType][k]
        if not "__xpath__" in v:
            logger.info("There is not xpath defined. Skip it")
            pass
        
        logger.debug("First level entry: "+k)
        class_ = apps.get_model(k)
        instance = class_()
                
        parsingResult=True
        parsingInfo=None
        try:
            #self.processMappingEntry(instance,v,obj)
            numInstances,indexXpath = self.getNumEntriesXpath(obj, v["__xpath__"])

            parentReferences=obj.xpath(v["__xpath__"][indexXpath])[0]
            #parentReferences=obj.xpath("/")[0]
            instance=self.processMappingModel(k, v,obj,index=0,parentInstance=instance,parentReferences=parentReferences)
            parsingInfo="OK"
            
            logger.debug("Finished processing: "+path)
        except Exception as e:
            parsingResult=False
            parsingInfo = str(e)
            #exc_type, exc_obj, exc_tb = sys.exc_info()
            #fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            #parsingInfo=str(exc_type)+","+str(fname)+","+str(exc_tb.tb_lineno)
            logger.info("Problem parsing: "+path+" added to unprocessed.")
            logger.info(parsingInfo)
            traceback.print_exc()
            
            
        
        return parsingResult,parsingInfo
    
   
    def findXpath(self,xmlParser,xpathList):
        '''
        For a given list of xpath entries, it returns the first one found. None if no entry was found.
        '''
        result=xpathList[0]
        for path in xpathList:
            aux=xmlParser.xpath(path)
            if len(aux)!=0:
                return aux
            
        return result
    
    def getNumEntriesXpath(self,xmlParser,listXpaths):
        '''
        It returns the number of occurrences for the first found xpath and its
        index in the list
        '''
#         for xpath in listXpaths:
        index=0
        for xpath in listXpaths:
            if xpath in self.cache:
                return len(self.cache[xpath]),index
            else:
                numElements = int(xmlParser.xpath("count("+xpath+")"))
            if numElements>0:
                return numElements,index
            index=index+1
        
        return 0,0
        
            
    
    def processMappingModel(self,mappingClass,mappingNode,xmlParser,index=0,parentInstance=None,parentReferences=None):
        '''
        Dictionary containing they object the object id and the field name
        pointing at the current object.
        [objectID: attributeOfThatObject]
        '''
        logger.debug("Get the model from the class "+mappingClass)
        attr = apps.get_model(mappingClass)
        filled=0
        numInstances=0
        attr_instance=None
        indexXpath=0
        xpathModel=None
                
        if "__xpath__" in mappingNode:
            numInstances,indexXpath = self.getNumEntriesXpath(xmlParser, mappingNode["__xpath__"])
            xpathModel = mappingNode["__xpath__"][indexXpath]
            logger.debug("Populate model: "+str(mappingClass)+" and "+str(numInstances)+" instances choosing "+str(indexXpath)+
                         " parent reference "+str(parentReferences))
        else:
            logger.debug("__xpath__ not found assume one entry...")
            numInstances=1
        
               
        for i in range(numInstances):
            logger.debug("Process "+str(i)+" instance for "+str(mappingClass)+" of "+str(numInstances))
            attr_instance = attr()
            attr_instance.id=self.getLocalId(type(attr_instance))
            if xpathModel != None:
                parentReferences=self.getXpathFromCache(xmlParser, xpathModel)[i]
                #parentReferences = xmlParser.xpath(xpathModel)[i]
            logger.debug("Call processMappingEntry with parents "+str(parentReferences))          
            filled+=self.processMappingEntry(attr_instance,mappingClass,mappingNode, xmlParser,i,parentInstance,numInstances,parentReferences)
                                   
            logger.debug("Mapping model "+str(mappingClass)+" filled with "+str(filled)+" elements")
            if filled > 0:
                                
                logger.debug("Created "+str(type(attr_instance))+" with pk "+str(attr_instance.id))
                logger.debug("The mapping model for ["+str(mappingClass)+"] was filled, save it")

                self.objectPool[type(attr_instance)].append(attr_instance)
                if attr_instance._pointingTable!=None:    
                    with allow_unsaved(attr,attr._pointingTable):
                        setattr(attr_instance, attr_instance._pointingTable, parentInstance)
                logger.debug("Update tables with incoming references")
            else:
            #    attr_instance.delete()
                attr_instance=None
    
        return attr_instance
    
  
    def processMappingEntry(self,currentInstance,mappingClass,mappingNode,xmlParser,index=0,
                            parentInstance=None,numInstances=1,parentReferences=None):
        '''
        It processes any mapping entry and sets the value
        '''
        filled=0
        class_=apps.get_model(mappingClass)
        '''
        Dictionary with the variable name and the child object for posterior connection
        '''
        logger.debug("------------>Current parentReference:"+str(parentReferences))
        for k,v in mappingNode.items():
            logger.debug("Process key: "+k+" from instance: "+str(currentInstance))
            
            if k=="__xpath__":
                #ignore the xpath of repetitive structures
                logger.debug("Ignore the __xpath__ item")
                continue
                 
            #if not k in instance._meta.get_all_field_names():
            if k[0:10]=="prototype.":
                instance=self.processMappingModel(k, v, xmlParser, index,currentInstance,parentReferences)
                if instance!=None:
                    filled+=1
                #this is a class not a simple attribute
            else:
                #explore an attribute
                attr = class_._meta.get_field(k)
                attr_type = type(attr).__name__
                logger.debug("Process attribute "+k+" of type "+attr_type)
                if attr_type=="ForeignKey":
                    logger.debug("Process foreign key")
                    attr_instance=self.processMappingModel("prototype."+attr.rel.to.__name__,v, xmlParser,index,currentInstance,parentReferences)
                    
                    if attr_instance!=None:

                        with allow_unsaved(class_,k):
                            setattr(currentInstance, k, attr_instance)

                        filled+=1
                else:                    
                    logger.debug("Regular entry ["+str(index)+"]")
                    if "__special__" in v:
                        #special value
                        content = self.getSpecialValue(v,xmlParser)
                    elif "__value__" in v:
                        #fixed value
                        content=v["__value__"]  
                    elif not "__xpath__" in v and "__conversion__" in v:
                        logger.debug("Multifield item detected")
                        content = self.getMultiField(xmlParser,v["__conversion__"],index,parentInstance,numInstances,parentReferences)
                    elif "__enumeration__" in v:
                        logger.debug("Enumeration found")
                        content=self.getEnumerationField(xmlParser,v["__enumeration__"],index,parentInstance,numInstances,parentReferences)
                        logger.debug("The enumerated returns: "+str(content))
                    else:
                        content=self.getContentXPathList(xmlParser,v["__xpath__"],index,parentInstance,numInstances,parentReferences)
                        
                    if content!=None:
                        self.setValue(currentInstance,k,content,v)
                        filled+=1
      
        return filled
    
    def getSpecialValue(self,v,xmlObject):
        '''
        We assume a set of predefined special values which content cannot
        be directly extracted from the XML value
        '''
        content = None
        if v["__special__"]=="expression":
            #get the expression to fill
            expression=v["__expression__"]
            values=v["__values__"]
            resolved_values=[]
            logger.debug("Get special value using "+str(values))
            for aux in values:
                resolved_values.append(xmlObject.xpath(aux)[0].text)
            content = expression%tuple(resolved_values)
            logger.debug("Resulting expression: "+str(content))
        elif v["__special__"]=="value":
            #fixed value
            valueToGet = v["__value__"]
            if valueToGet=="url":
                #get the currently processed url
                content = self.itemInfo[2]
            #content = self.itemInfo[valueToGet]
        elif v["__special__"]=="xml":
            content = self.getXMLString(xmlObject,v["__xpath__"]) 
            
        return content
    
    
    def getXMLString(self,xmlObject,xpathList):
        '''
        Returns the XML without conversion in a string
        '''
        for xpath in xpathList:
            xpathEntry=self.getXpathFromCache(xmlObject, xpath)
            if xpathEntry != None:
                return etree.tostring(xpathEntry[0])
        return None
        
            

    
    
    
    def getEnumerationField(self,xmlObject,enumerationDict,index=0,parent=None,numInstances=1,parentReferences=None):
        '''
        For an enumerated dictionary with the form
        {xpath_1:value, xpath2:value...xpath_n:value}
        it returns the first found xpath and the conversion value.
        '''
        for xpath in enumerationDict.keys():
            element = self.getContentXPath(xmlObject, xpath,index=index,parent=parent,numInstances=numInstances,parentReferences=parentReferences)
            if element!=None:
                #it was found
                return enumerationDict[xpath]
        return None
           
        
    
    def getMultiField(self,xmlObject,additionalData,index=0,parent=None,numInstances=1,parentReferences=None):
        '''
        Object which value is the result of connecting several values with its corresponding
        xpaths.
        ''' 
        resultingDate={"year":0,
                       "month":0,
                       "day":0,
                       "hours":0,
                       "minutes":0}
        toReturn =None
        
        for conversionData in additionalData:

            found=True
            if not "string" in conversionData:
                #composed entry
                for entry,xpath in conversionData.items():
                
                    aux=self.getContentXPath(xmlObject, xpath, index,parent,numInstances,parentReferences)
        
                    if aux==None:
                        #No entry for these value
                        found=False
                        break
                    #logger.debug("Date element content for "+xpath+" is: "+str(aux))
                    if entry=="time":
                        tokens = aux.split(":")
                        tokens[0]=punct_remover.sub('',tokens[0])
                        tokens[1]=punct_remover.sub('',tokens[1])
                        resultingDate["hours"] = int(tokens[0])
                        resultingDate["minutes"] = int(tokens[1])
                        #portion of cleaning just to save some time
                        if resultingDate["hours"]==24:
                            logger.warning("Hour converted from 24 to 00")
                            resultingDate["hours"]=0
                    else:
                        aux = punct_remover.sub('',aux)
                        resultingDate[entry]=int(aux)
                
                if found:
                    logger.debug("Date fields found: "+str(resultingDate))
                    toReturn = datetime.datetime(resultingDate["year"],
                                             resultingDate["month"],
                                             resultingDate["day"],
                                             resultingDate["hours"],
                                             resultingDate["minutes"]
                                             )
            else:
                #the date is a single field
                toReturn = self.getContentXPath(xmlObject, conversionData["string"], index,parent,numInstances)
                #toReturn = dateParser.parse(timeString)
      
            if toReturn!=None:
                break
        
        return toReturn
            
    
    def getContentXPathList(self,xmlObject,xpathList,index=0,parent=None,numInstances=1,parentReferences=None):
        '''
        Process a list of XPaths and return the content of the first
        xpath found
        '''
        for xpath in xpathList:
            content = self.getContentXPath(xmlObject, xpath, index,parent,numInstances,parentReferences)
            if content!=None:
                return content
        return None
        
    def getXpathFromCache(self,xmlObject,xpath):
        toReturn = None
        if xpath in self.cache:
            toReturn = self.cache[xpath]
        else:
            toReturn = xmlObject.xpath(xpath)
            self.cache[xpath]=toReturn
        return toReturn

    def getContentXPath(self,xmlObject,xpath,index=0,parent=None,numInstances=1,parentReferences=None):
        '''
        Get the content from the XML using the XPATH.
        Index is used for situations when the xpath return multiple results
        '''
        content=None
        logger.debug("Check xpath "+str(xpath)+" for index "+str(index)+' and '+str(numInstances)+" instances")
        xpathEntry=self.getXpathFromCache(xmlObject, xpath)

        logger.debug("The entry extracted from the cache is: %s of type %s"%(xpathEntry,type(xpathEntry)))

        if len(xpathEntry)==0:
            return content
        
        if type(xpathEntry)==etree._ElementStringResult or type(xpathEntry)==etree._ElementUnicodeResult or type(xpathEntry)==str:
            logger.debug("Return string")
            return xpathEntry.strip()
        
        toProcess = None
        if len(xpathEntry)==numInstances:
            #same number of parents and children
            toProcess = xpathEntry[index]
        else:
            #we have to find the corresponding parent
            #Find whether the parent reference is our parent
            # print('--------------------')
            # print(type(xpathEntry[0]))
            # print('--------------------')
            logger.debug("The value of the xpath entry is %s of type %s"%(xpathEntry,type(xpathEntry)))
            currentType = type(xpathEntry[0])
            if currentType==etree._ElementStringResult or currentType==etree._ElementUnicodeResult:

                #For the string results we need to take the original value
                auxPath = xpath[0:xpath.rfind('/')]
                attrib_name = xpath[(xpath.rfind('@')+1):]
                auxEntry = self.getXpathFromCache(xmlObject, auxPath)
                index = 0
                for i in auxEntry:
                    #find the item that is the current parent
                    if self.isParent(parentReferences,i):                            
                        logger.debug("The current instance "+str(i)+" is children of "+str(parentReferences))
                        logger.debug("Get item at index: "+str(index)+" from "+str(len(xpathEntry)))
                        #check wether the attribute is contained there
                        if attrib_name in i.attrib:
                            #toProcess=xpathEntry[index]
                            toProcess = i.attrib[attrib_name]
                        break
                    index=index+1
            elif parentReferences!=None:
                index=0
                for i in xpathEntry:
                    if self.isParent(parentReferences,i):
                        toProcess=i
                        logger.debug("The current instance is the father of "+str(i))
                        break
                    index=index+1
            
            if toProcess == None:
                logger.debug("The current instance is not the father")
                return None
            

        if type(toProcess)==etree._ElementStringResult or type(toProcess)==etree._ElementUnicodeResult or type(toProcess)==str:
            logger.debug('Found element string')
            content = toProcess.strip()
        elif toProcess.text!=None:
            #there is some text inside
            logger.debug("There was some text inside")
            #content = xpathEntry[index].text.strip()
            content = toProcess.text.strip()
        elif len(toProcess)!=0:
            logger.debug("Fond element node, stringify it!")
            #stringify the object
            content=self.stringify_node(toProcess)
        elif toProcess.tag!=None:
            logger.debug("Return the tag")
            content = xpathEntry[index].tag.strip()
        
        
        #logger.debug("Found content for index "+str(index)+"["+content+"]")

        return content
    
    def isParent(self,parentCandidate,entry):
        '''
        Determines whether parentCandidate is the parent of entry
        '''
        result = False

        for i in parentCandidate:
            #logger.debug("Check if the one below is children -->"+str(i==entry))
            #logger.debug(etree.tostring(i))
            if i==entry:
                result = True
            else:
                result  = self.isParent(i, entry)
            if result:
                break
                
        return result
    
    def setValueWithSamples(self,targetInstance,attrName,value,theType,theSamples):
        '''
        This function is employed when the user defines the transformation between one of the
        entries and the final type. It receives theType for the conversion and the samples.
        
        For example:
        content: TRUE_FIELD_VALUE
        theType: boolean
        theSamples: [FALSE_FIELD_VALUE:false]
        
        By exclusion the final result should be true
        '''
        convertedValue=None
        if not value in theSamples:
            if theType=="boolean":
                #this is boolean if we do not know the value, we assume
                #we found the complementary class                
                convertedValue=not theSamples.values()[0]
            else:
                logger.error("The value into XML "+value+" is unknown for sampling conversion")
                return
        else:
            convertedValue=theSamples[value]
        
        logger.debug("Convert "+value+" into "+str(convertedValue)+" using samples")
        
        targetInstance.__setattr__(attrName,convertedValue)
    
    
    def setValue(self,targetInstance,attrName,value,additionalData=None):
        '''
        Set the the attrName attribute the given value. In the case we are trying
        to set a value from a different class, we have to check and convert when
        possible
        '''
        #classTarget = type(targetInstance._meta.get_field(attrName)).__name__
        classTarget = type(targetInstance._meta.get_field(attrName))
        logger.debug("Our class target is type "+classTarget.__name__)
        
        if classTarget == CharField:
            setattr(targetInstance,attrName,value)
            #targetInstance.__setattr__(attrName,value)
            return
        
        if classTarget == TextField:
            setattr(targetInstance,attrName,value)
            #targetInstance.__setattr__(attrName,value)
            return
        
        if classTarget == OneToOneField:
            #targetInstance.__setattr__(attrName,value)
            setattr(targetInstance,attrName,value)
            logger.debug("Set with "+attrName+" with: "+str(value))
            return
        
        if classTarget == ForeignKey:
            #targetInstance.__setattr__(attrName,value)
            setattr(targetInstance, attrName, value)
            logger.debug("Set with "+attrName+" with: "+str(value))
            return
        
        #it is not a foreign key, but we have to convert to something that is not text
        convertedValue = self.convertToType(value,classTarget,additionalData)
        if(convertedValue!=None):
            targetInstance.__setattr__(attrName,convertedValue)        
        
        
        
    def convertToType(self,value,classTarget,additionalData=None):
        '''
        This function collects a set of conversion tools employed to 
        transform textfield values into more sophisticated class targets.
        This is needed in order to accomodate the classes to the data types
        defined in the database.
        '''
        
        toReturn = None

        if classTarget==FloatField:
            toReturn = None
            try:
                toReturn = float(value)
            except ValueError:
                
                logger.debug("Non floating convertible value")
                pass
            logger.debug("Convert to FloatField from "+value+" to "+str(toReturn))
        elif classTarget==DateTimeField:
            #toReturn = dateParserrser.parse(value)
#             toReturn = self.convertDate(value,additionalData["__conversion__"])
            toReturn = value
            logger.debug("Convert to DateTimeField from "+str(value)+" to "+str(toReturn))
        #elif classTarget==CountryField:
        #    toReturn = value
        #    logger.debug("Convert to CountryField from "+value+" to "+str(toReturn))
        elif classTarget==EmailField:
            if len(value)>254:
                raise Exception("EMail field length larger than 254")
            toReturn = unidecode(value)        
            #toReturn = value
            logger.debug("Convert to EmailField from "+value+" to "+str(toReturn))
        elif classTarget==URLField:
            if len(value)>500:
                raise Exception("URLField  length larger than 500")
            
#             if not bool(urlparse.urlparse(value).scheme):
#                 raise Exception("URLField "+value+" was not a valid URL")
                            
            toReturn = value
            logger.debug("Convert to URLField from "+value+" to "+str(toReturn))
        elif classTarget==IntegerField:
            toReturn = int(value)
            logger.debug("Convert to IntegerField from "+value+" to "+str(toReturn))
        elif classTarget==DecimalField:
            aux = Decimal(value.replace('%','')).quantize(FOURPLACES)
            if aux < MAX_DECIMAL:
                toReturn = aux
            elif aux.as_tuple().exponent < -4:
                raise Exception("More decimals than expected "+str(aux.as_tuple().exponent))
            else:
                raise Exception("Invalid decimal number: "+value)
            logger.debug("Convert to DecimalField from "+value+" to "+str(toReturn))
        elif classTarget==NullBooleanField:
            if value in additionalData["__conversion__"]:
                toReturn = bool(additionalData["__conversion__"][value])
            else:
                toReturn = not bool(list(additionalData["__conversion__"].values())[0])
            logger.debug("Convert to NullBooleanField from "+value+" to "+str(toReturn)) 
#         elif issubclass(classTarget,EnumField):
# #             if value in additionalData["__conversion__"]:
# #                 instance = classTarget()
#                 #toReturn = instance.convert(additionalData["__conversion__"][value])
#             #else:
#             #    logger.warn("Enumerated value "+value+" is unknown for "+str(classTarget))
#             toReturn = value
#
#             #logger.debug("This is an enumeration field from "+value+" to "+str(toReturn))
        else:
            logger.error(classTarget.__name__+" conversion was not defined")
        return toReturn
            
   
                        
    def stringify_node(self,node):
        '''
        Get all the content from a node in a txt version.
        '''
        #logger.debug("Stringify a complete structure")
#         parts = ([node.text.strip()] +
#                  list(chain(*([c.text.strip(), tostring(c), c.tail] for c in node.getchildren()))) +
#              [node.tail])
#         #parts = (list(chain(*([c.text.strip(),"\n"] for c in node.getchildren()))))
#         return ''.join(filter(None,parts)).strip()
        #return etree.tostring(node)
        toReturn = ""
        
        if len(node.getchildren())==0:
            if node.text != None:
                return node.text
            else:
                return ""
        
        for i in node:
            if len(i.getchildren())!=0:
                for j in i.getchildren():
                    toReturn = toReturn + '<'+j.tag+'>'
                    toReturn = toReturn + self.stringify_node(j)
                    toReturn = toReturn + "<\\"+j.tag+'>'
            if i.text != None:
                toReturn = toReturn + i.text.strip()+"\n"
        return toReturn.strip()
    
    
        
