'''
Created on 5 Jun 2015

@author: jmt78
'''
from django.test import TestCase
from parser import MappingBasedParser
from django.conf import settings
import json
import datetime
import codecs


from DigiwhistWeb.prototype.models import Contract,Lot,ContractTypesEnum,Price

import os


import importlib

import logging
# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")


class TestTED(TestCase):

    def test_parse(self):
        parser = MappingBasedParser()
        #Load the parsing mapping
        mappingFile = os.path.join(settings.BASE_DIR,"testData/redu_example_ted.json")
        testFile = os.path.join(settings.BASE_DIR,"testData/redu_example_ted.xml")
         
        logger.info("Get mapping from "+mappingFile)
        with codecs.open(mappingFile,"r",'utf-8-sig') as reader:
            mapping = json.load(reader)
        
        logger.info("Process XML file "+testFile)
        parser.parseXML(testFile,mapping)
        
        #Retrieve the example contract from the database and check that every field is correct

        
        c = Contract.objects.get(contractId="001335-2011")
        
        self.assertNotEqual(c,None,"Not found item")
        self.assertEqual(c.contractId,"001335-2011","Identifier differs")
        self.assertEqual(c.cftVersion,"R2.0.7.S03.E01","CFT version differs")
        self.assertEqual(c.country.name,"Italy","Country code differs "+str(c.country.name))
        
        c.delete()
        
    def test_parse_iterative(self):
        parser = MappingBasedParser()
        #Load the parsing mapping
        mappingFile = os.path.join(settings.BASE_DIR,"testData/manyitem_example_ted.json")
        testFile = os.path.join(settings.BASE_DIR,"testData/redu_example_ted.xml")
        
        logger.info("Get mapping from "+mappingFile)
        logger.info("Process XML file "+testFile)
        
        with codecs.open(mappingFile,"r",'utf-8-sig') as reader:
            mapping = json.load(reader)
        
        parser.parseXML(testFile,mapping)
        
        #Retrieve the example contract from the database and check that every field is correct

        
        contracts = Contract.objects.filter(contractId="001335-2011").count()
        self.assertEqual(contracts, 1,"We found "+str(contracts)+" contracts instead of 1")

        c = Contract.objects.get(contractId="001335-2011")
        
        l=Lot.objects.all()
        counter = 0
                
        expected = ['Kit indicatori diagnostici e valutazione della risposta alla terapia per neoplasie del sistema emopoietico/standard per analisi quantitativa in real time (tecnologia taqman).',
                    'Kit per analisi riarrangiamenti T-Cell receptor, IGH e riarrangiamenti genici tipici dei linfomi + plasticheria.',
                    'Diagnostica specialistica su striscia.',
                    'Kit per la determinazione molecolare degli antigeni eritrocitari e piastrinici.',
                     u'Programmi di controllo di qualit\xe0 esterno.'
                    ]
        
        for e in l:
            counter+=1
            print "Found: [",e.id,e.contractId.id,e.contractId.contractId,e.contractId.country,"]"
            self.assertEqual(e.description in expected,True,"Unexpected description ["+e.description+"]")
        self.assertEqual(counter, 5, "We do not have 5 entries as expected")
            

    def test_parse_iterative_multilevel(self):
        '''
        Test a repetitive structure with multiple levels
        '''
        
        parser = MappingBasedParser()
        #Load the parsing mapping
        mappingFile = os.path.join(settings.BASE_DIR,"testData/multilevel_example_ted.json")
        testFile = os.path.join(settings.BASE_DIR,"testData/redu_example_ted.xml")
        
        logger.info("Get mapping from "+mappingFile)
        logger.info("Process XML file "+testFile)
        
        with codecs.open(mappingFile,"r",'utf-8-sig') as reader:
            mapping = json.load(reader)
        
        parser.parseXML(testFile,mapping)
        
        c = Contract.objects.get(contractId="001335-2011")
        print "The contract has id: ",c.id
        
        l=Lot.objects.filter(contractId=c)
        counter = 0
        for e in l:
            counter+=1
            self.assertEqual(e.addressOfPerformance.city, "Pescara", "Unexpected city address "+str(e.addressOfPerformance.city))
            self.assertEqual(e.addressOfPerformance.postcode,"65124","Unexpected postal code "+str(e.addressOfPerformance.postcode))
            self.assertEqual(e.addressOfPerformance.street,"via R. Paolini, 45","Unexpected street "+str(e.addressOfPerformance.rawAddress))
            self.assertEqual(e.addressOfPerformance.rawAddress,"Azienda USL di Pescara","Unexpected raw address "+str(e.addressOfPerformance.rawAddress))
            self.assertEqual(e.addressOfPerformance.country.name,"Italy","Unexpected country "+str(e.addressOfPerformance.country.name))
            
            
        self.assertEqual(counter, 5, "We do not have 5 entries as expected. We have "+str(counter))
        
    def test_parse_boolean(self):
        '''
        Test the boolean conversion for examples given by users using MITFORD
        '''
        parser = MappingBasedParser()
        #Load the parsing mapping
        mappingFile = os.path.join(settings.BASE_DIR,"testData/boolean_example_ted.json")
        testFile = os.path.join(settings.BASE_DIR,"testData/redu_example_ted.xml")

        with codecs.open(mappingFile,"r",'utf-8-sig') as reader:
            mapping = json.load(reader)
        
        parser.parseXML(testFile,mapping)
        
        c = Contract.objects.get(contractId="001335-2011")
        
        self.assertEqual(c.contractId,"001335-2011","Identifier differs")
        self.assertEqual(c.variantsAccepted,False,"Variants accepted differs. Found "+str(c.variantsAccepted))
        #This one does not make sense but is used to check whether we can take tags like
        #</NO_ACCEPTABLE> tags as values 
        self.assertEqual(c.isCentralPurchase,False,"Is central purchased differs. Found "+str(c.isCentralPurchase))
        c.delete()
        
    def test_parse_enum(self):
        '''
        Test the conversion of enumerated types
        '''
        parser = MappingBasedParser()
        #Load the parsing mapping
        mappingFile = os.path.join(settings.BASE_DIR,"testData/enum_example_ted.json")
        testFile = os.path.join(settings.BASE_DIR,"testData/redu_example_ted.xml")

        with codecs.open(mappingFile,"r",'utf-8-sig') as reader:
            mapping = json.load(reader)
        
        parser.parseXML(testFile,mapping)
        
        c = Contract.objects.get(contractId="001335-2011")
        self.assertEqual(c.contractId,"001335-2011","Identifier differs")
        aux = ContractTypesEnum()
        c = Contract.objects.get(contractId="001335-2011")
        self.assertEqual(c.contractId,"001335-2011","Identifier differs")
        
        self.assertEqual(c.type,aux.convert('SUPPLIES'),'Enumerated differs')
        
    def test_parse_date(self):
        '''
        Test the date format
        '''
        parser = MappingBasedParser()
        #Load the parsing mapping
        mappingFile = os.path.join(settings.BASE_DIR,"testData/date_example_ted.json")
        testFile = os.path.join(settings.BASE_DIR,"testData/redu_example_ted.xml")

        with codecs.open(mappingFile,"r",'utf-8-sig') as reader:
            mapping = json.load(reader)
        
        parser.parseXML(testFile,mapping)        
        c = Contract.objects.get(contractId="001335-2011")
        self.assertEqual(c.contractId,"001335-2011","Identifier differs")
        self.assertEqual(c.call_for_tenders_date.year,2011,"Cftpd differs")
        self.assertEqual(c.call_for_tenders_date.month,3,"Cftpd differs")
        self.assertEqual(c.call_for_tenders_date.day,21,"Cftpd differs")
        self.assertEqual(c.call_for_tenders_date.hour,12,"Cftpd differs")
        self.assertEqual(c.call_for_tenders_date.minute,0,"Cftpd differs")
        
        self.assertEqual(c.docDeadlineDate.year,2011,"Doc deadline differs")
        self.assertEqual(c.docDeadlineDate.month,3,"Doc deadline differs")
        self.assertEqual(c.docDeadlineDate.day,21,"Doc deadline differs")
    
    
    def test_parse_timing(self):
        parser = MappingBasedParser()
        #Load the parsing mapping
        mappingFile = os.path.join(settings.BASE_DIR,"mappings/ted_mapping.json")
        #testFile = os.path.join(settings.BASE_DIR,"testData/redu_example_ted.xml")
        testFile = os.path.join(settings.BASE_DIR,"/tmp/intermediate/2011-01/20110115_010/015245_2011.xml")
                
        
        with codecs.open(mappingFile,"r",'utf-8-sig') as reader:
            mapping = json.load(reader)
                    
        #construct one item
        from DigiwhistWeb.crawling.pipelines.TedPipeline import TedPreItem
        item=TedPreItem()
        #item['url']="/tmp/intermediate/2011-01/*.xml"
        item['url']='/tmp/intermediate/2011-01/20110115_010/014754_2011.xml'
        item['source']="ted"
        item['folder']="/tmp/intermediate/2011-01/"
        item['archivalId']="111111"
        class Aux:
            name="ted"
            
        parser.process_item(item, Aux())
        
        numLots=Lot.objects.all().count()
        self.assertEqual(numLots, 8, "There are "+str(numLots)+" instead of 8")
        
    def test_parse_xslt(self):
        from lxml import etree
        parser = MappingBasedParser()
        
        #months
        testFile = os.path.join(settings.BASE_DIR,"/tmp/intermediate/2011-01/20110104_001/000182_2011.xml")
        #years
        testFile = os.path.join(settings.BASE_DIR,"/tmp/intermediate/2011-01/20110104_001/001133_2011.xml")
        
        from DigiwhistWeb.crawling.pipelines.TedPipeline import TedPreItem
        item=TedPreItem()
        #item['url']="/tmp/intermediate/2011-01/*.xml"
        item['url']=testFile
        item['source']="ted"
        item['folder']="/tmp/intermediate/2011-01/"
        item['archivalId']="111111"
        class Aux:
            name="ted"
            
        parser.process_item(item, Aux())
        
        print ""
        for x in Price.objects.all():
            print x.netAmount
            print x.currency
            print x.unitNumber
            print x.unitPrice
            
            
