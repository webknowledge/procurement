
from django.core.management.base import BaseCommand,CommandError
from parsing.pipeline.oldTedConverter import DataProcessor
from lxml import etree
import logging
import glob

logger = logging.getLogger("DigiwhistWebLogger")

KEYWORDS=set([
          "Estimated cost excluding VAT",
          "Estimated cost excluding VAT",
          "Address of the buyer profile",
          "The contracting authority is purchasing on behalf of other contracting authorities",
          "Total final value of the contract",
          "Value",
          "Other",
          "Payable documents",
          "Duration in month(s)",
          "Date",
          "Place",
          "Persons authorised to be present at the opening of tenders",
          "Division into lots",
          "Email"
          ])

class Command(BaseCommand):
    
    help = 'Preprocess the ted data before 2011 into a reasonable XML format'
    
    def add_arguments(self,parser):
        parser.add_argument('input',type=str,help='Input folder where the files are stored')
        parser.add_argument('output',type=str,help="Output folder to dump the results")
              
    
    def handle(self, *args, **options):
        self.oldTedConverter(options['input'],options['output'])
        
    def oldTedConverter(self,inputFolder,outputFolder):
        processor = DataProcessor()
        logger.info("Check input folder: "+inputFolder)
        for inputFile in glob.glob(inputFolder+"/*META*"):
            logger.info("Transform "+inputFile)
            fileName = inputFile.split("/")[-1]
            processor.process_file(inputFile)
            