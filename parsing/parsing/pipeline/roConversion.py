
import os
import glob


from parsing.models import ArchiveItem
from django.conf import settings

from parsing.pipeline.conversion.ext2xml import Ext2Xml

import os

import urllib
import logging
# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")


class DataProcessor(object):
    
    def checkOutputFolder(self,output_folder):
        logger.info("Check output folder "+output_folder)
        if not os.path.isdir(output_folder):
            logger.info("Non existing. Create folder. "+output_folder)
            os.makedirs(output_folder)
        else:
            logger.info("It already exists.")
    
    def process_item(self,item):
        
        #get all the files form the archival and store them into the intermediate folder
        
        # if not item['source']=="ro":
        #     logger.info("The ro pipeline was called from a spider of type ["+item['source']+"]")
        #     return
        
        destinationFolder = settings.INTERMEDIATE_FOLDER+"/ro"
        # self.checkOutputFolder(destinationFolder)
        #destinationFolder = destinationFolder+"/original"
        #self.checkOutputFolder(destinationFolder)

        file_path,archiveId,url=item['folder'][0]

        #convert them
        processedFolder = settings.INTERMEDIATE_FOLDER+'/ro/processed'
        self.checkOutputFolder(processedFolder)
        prc = Ext2Xml(processedFolder)
        listToProcess=[]
        listToProcess.extend(glob.glob(destinationFolder+'/*.csv'))
        listToProcess.extend(glob.glob(destinationFolder+'/*.xls'))
                
        toBeProcessed = prc.processData(listToProcess)
        folders=[]
        for f in toBeProcessed:
            folders.append((f,item["archivalId"],item['url']))

        for f in listToProcess:
            os.remove(f)

        #process the objects extracted in the given folder
        item['folder']=folders
        
        return item