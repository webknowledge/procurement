
import tarfile
import os
import glob

from parsing.models import ArchiveItem
from django.conf import settings

import logging
# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")


class DataProcessor(object):
    
    def checkOutputFolder(self):
        logger.info("Check output folder "+self.output_folder)
        if not os.path.isdir(self.output_folder):
            logger.info("Non existing. Create folder. "+self.output_folder)
            os.makedirs(self.output_folder)
        else:
            logger.info("It already exists.")
    
    def process_item(self,item):
        
        # if not item['source']=="ted":
        #     logger.info("The TED pipeline was called from a spider of type ["+item['source']+"]")
        #     return
        #
        # extension=item['url'].split("/")[-1]
        # file_name=extension.split(".")[0]
        #
        #self.output_folder=settings.INTERMEDIATE_FOLDER+"/"+file_name

        #
        # file_path=self.output_folder+"/"+extension
        # #get the content for the archive
        # x = ArchiveItem.objects.get(id=item['archivalId'])
        #
        # with open(file_path,"w") as writer:
        #     writer.write(x.file.read())
        
        #free some memory
        #logger.info("Extract: "+file_path)
        file_path,archiveItem,url=item['folder'][0]
        extension=url.split("/")[-1]
        file_name=extension.split(".")[0]
        self.output_folder=settings.INTERMEDIATE_FOLDER+"/ted/"+file_name
        self.checkOutputFolder()
        logger.info('Read the file from %s'%file_path)
        with tarfile.open(file_path,'r') as extractor:
            extractor.extractall(path=settings.INTERMEDIATE_FOLDER+'/ted/'+file_name)
        #The resulting output is in a file like yyyymmdd-XXX
        logger.info("Remove original file"+file_path)
        os.remove(file_path)
        
        folders=[]
        #logger.info("Search and extract elements in: "+file_path.split(".")[0]+"-*.tar.gz")
        #for f in glob.glob(file_path.split(".")[0]+"*.tar.gz"):
        compressed_folder=self.output_folder+"/*.tar.gz"
        logger.info("Search and extract elements in: "+compressed_folder)
        for f in glob.glob(compressed_folder):
            with tarfile.open(f,"r") as extractor:
                logger.info("Extract secondary folder: "+f)
                extractor.extractall(path=self.output_folder)
                logger.info("Delete: "+f)
                os.remove(f)
        for f in glob.glob(self.output_folder+"/*"):
                for xmlFile in glob.glob(f+'/*.xml'):
                    folders.append((xmlFile,item["archivalId"],item['url']))
         
        #process the objects extracted in the given folder
        item['folder']=folders
        return item