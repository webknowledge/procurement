
import os

from parsing.parser import NAMESPACE_REMOVAL
import copy
from django.conf import settings
import logging
from lxml import etree


# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")



xslt_doc = etree.XML(NAMESPACE_REMOVAL)
namespace_cleaner = etree.XSLT(xslt_doc)



class DataProcessor(object):
    
    '''
    It gets the data from the archive and stores it in the 
    intermediate folder
    '''
    
    
    def process_item(self,item):
        
        #get the input file in this case, only one
        inputFile,archivalId,url = item['folder'][0]
        
        with open(inputFile,'r') as reader:
            logger.info("Load original file from %s"%inputFile)
            s =reader.read()
            xmlObject = etree.fromstring(s,parser=etree.XMLParser(encoding="utf-8"))
            xmlObject = namespace_cleaner(xmlObject)
        
        logger.debug("Remove original file %s"%inputFile)
        os.remove(inputFile)
        
        item['folder']=[]
        #get a common header for all the files
        header = copy.deepcopy(xmlObject)
        #remove the notices entry
        header.getroot().remove(header.xpath("/FEED/NOTICES")[0])
        
        #now iterate all the elements of the file and create a separated file
        counter=0
        outputPath = settings.INTERMEDIATE_FOLDER+"/uk_%d.xml"
        for element in xmlObject.xpath('/FEED/NOTICES')[0]:
            toWrite = copy.deepcopy(header)
            aux = etree.Element('NOTICES')
            aux.append(element)
            toWrite.getroot().append(aux)
            filePath = outputPath%counter 
            with open(filePath,'wb') as writer:
                writer.write(etree.tostring(toWrite))
                item['folder'].append((filePath,archivalId,item['url']))
            counter = counter+1
            del toWrite
        
        logger.debug("Original file was split into %d files"%counter)
        #remove the original file
        
           
        return item