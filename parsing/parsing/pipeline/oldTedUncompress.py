
import zipfile
import os
import glob

from parsing.models import ArchiveItem
from django.conf import settings

import logging
# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")


class DataProcessor(object):
    
    def checkOutputFolder(self):
        logger.info("Check output folder "+self.output_folder)
        if not os.path.isdir(self.output_folder):
            logger.info("Non existing. Create folder. "+self.output_folder)
            os.makedirs(self.output_folder)
        else:
            logger.info("It already exists.")
    
    def process_item(self,item):
        
        if not item['source']=="oldTed":
            logger.info("The oldTed pipeline was called from a spider of type ["+item['source']+"]")
            return

        file_path,archiveItem,url=item['folder'][0]


        self.output_folder = os.path.dirname(file_path)


        self.checkOutputFolder()

        #free some memory
        logger.info("Extract: "+file_path)
        with zipfile.ZipFile(file_path,'r') as extractor:
            extractor.extractall(path=self.output_folder)
        #The resulting output is in a file like yyyymmdd-XXX
        logger.debug("Remove original file"+file_path)
        os.remove(file_path)
        entries=[]
        for f in glob.glob(self.output_folder+"/*"):
            entries.append((f,item['archivalId'],item['url']))
        
        item['folder']=entries
        return item