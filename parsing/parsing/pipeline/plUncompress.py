
import os
import glob

from django.conf import settings

import logging
# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")


class DataProcessor(object):
    
    def checkOutputFolder(self):
        logger.info("Check output folder "+self.output_folder)
        if not os.path.isdir(self.output_folder):
            logger.info("Non existing. Create folder. "+self.output_folder)
            os.makedirs(self.output_folder)
        else:
            logger.info("It already exists.")
    
    def process_item(self,item):
        
        if not item['source']=="pl":
            logger.info("The Poland pipeline was called from a different source")
            return

        previousItemFolder = item['folder']
        file_path,archiveId,url = item['folder'][0]


        extension=url.split("/")[-1]
        file_name=extension.split(".")[0]
        
        # self.output_folder=settings.INTERMEDIATE_FOLDER+"/"+file_name
        # self.checkOutputFolder()
        #
        # file_path=self.output_folder+"/"+extension
        # #get the content for the archive
        # x = ArchiveItem.objects.get(id=item['archivalId'])
        #
        # with open(file_path,"w") as writer:
        #     writer.write(x.file.read())
        
        #get the current working directory
        cwd=os.getcwd()
        #change the working path so after extraction we have all the files there
        outputFolder =settings.INTERMEDIATE_FOLDER+"/pl"
        os.chdir(outputFolder)
        #free some memory
        logger.info("Extract: "+file_path)
        #run the extract command with wine
        os.system("wine "+file_path)
        
        os.remove(file_path)
        #Add the extracted elements into the file path
        files=[]
        for l in glob.glob(outputFolder+"/*.xml"):
            files.append((l,item['archivalId'],item['url']))
        item['folder']=files
        #change the working directory to the original one
        os.chdir(cwd)        
        return item