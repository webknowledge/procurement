
import os


import copy
from django.conf import settings
import logging
from lxml import etree

from parsing.parser import NAMESPACE_REMOVAL


# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")


class DataProcessor(object):
    
    '''
    It gets the data from the archive and stores it in the 
    intermediate folder
    '''
    
    
    def process_item(self,item):
        
        #get the input file in this case, only one
        inputFile,archivalId,url = item['folder'][0]

        xslt_doc = etree.XML(NAMESPACE_REMOVAL)
        namespace_cleaner = etree.XSLT(xslt_doc)

        with open(inputFile,'r') as reader:
            logger.info("Load original file from %s"%inputFile)
            s =reader.read()
            xmlObject = etree.fromstring(s.encode('utf-8'))
            xmlObject = namespace_cleaner(xmlObject)

        logger.info("Remove original file %s"%inputFile)

        
        item['folder']=[]
        #get a common header for all the files
        header = copy.deepcopy(xmlObject)
        #remove the notices entry

        header.getroot().remove(header.xpath("/pubblicazione/data")[0])

        #now iterate all the elements of the file and create a separated file
        counter=0
        outputPath = settings.INTERMEDIATE_FOLDER+"/it/it_%d.xml"
        for element in xmlObject.xpath('/pubblicazione/data/lotto'):

            toWrite = copy.deepcopy(header)
            aux = etree.Element('data')
            aux.append(element)
            toWrite.getroot().append(aux)
            filePath = outputPath%counter 
            with open(filePath,'w') as writer:
                writer.write(etree.tostring(toWrite).decode('utf-8'))
                item['folder'].append((filePath,archivalId,item['url']))
            counter = counter+1
            del toWrite
        
        logger.info("Original file was split into %d files"%counter)
        #remove the original file

        os.remove(inputFile)
           
        return item