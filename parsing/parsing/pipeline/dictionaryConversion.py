

from django.conf import settings

from parsing.pipeline.html.converter.datarescue import getRescueClass
from parsing.pipeline.html.converter.htmlconverter import HtmlConverter
from parsing.pipeline.html.dictionary.encodingdictionary import EncodingDictionary
from parsing.pipeline.html.serialisers.fileSerializer import FileSerializer

import os

import logging

# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

pathThreshold = 1
tokenThreshold = 0.7
listSimilarityThreshold = 0.7


class DataProcessor(object):
    '''
    For a given dictionary create transform the existing files into XML from
    HTML.
    '''

    @staticmethod
    def checkOutputFolder(output_folder):
        logger.debug("Check output folder " + output_folder)
        if not os.path.isdir(output_folder):
            logger.debug("Non existing. Create folder. " + output_folder)
            try:
                os.makedirs(output_folder)
            except:
                logger.warn("The folder [%s] was already created or we had no permission" % output_folder)
        else:
            logger.debug("It already exists.")

    def process_item(self, item):
        logger.debug("Languages: " + str(item.get("file-language", [])))
        languages = item.get("file-language", [])
        source = item['source']
        output_folder = settings.INTERMEDIATE_FOLDER + "/" + source
        self.checkOutputFolder(output_folder)
        self.checkOutputFolder(output_folder + "/original/")

        previousFolderData = item['folder']
        newFolderData = []
        index = 0
        for inputFile, archival, url in previousFolderData:
            language = None
            if len(languages) > 0:
                language = languages[index]
            outputFile = self.convert(output_folder, inputFile, source, language)
            if outputFile is not None:
                newFolderData.append((outputFile, item['archivalId'], item['url']))

            index += 1

        # replace original item['folder'] with the new ones
        item['folder'] = newFolderData
        return item

    @staticmethod
    def convert(outputFolder, inputFile, source, language):
        outputSource = source
        if language is not None:
            outputSource = source + "-" + language

        logger.debug("Using " + outputFolder + " folder")
        encodingFile = settings.DICTIONARY_FOLDER + "/" + outputSource + "/encoding.json"
        # Some conversion helpers
        serializer = FileSerializer(encodingFile)
        mappingDictionary = EncodingDictionary()
        mappingDictionary.loadEncoding(serializer)

        outputFile = outputFolder + "/" + os.path.basename(inputFile) + ".xml"
        counter = 0
        while os.path.exists(outputFile):
            counter += 1
            outputFile = outputFolder + "/" + os.path.basename(inputFile) + ".xml_%d" % counter

        dataRescuer = getRescueClass(outputSource)
        htmlConverter = HtmlConverter(mappingDictionary,
                                      mappingDictionary.getTokenThreshold(tokenThreshold),
                                      mappingDictionary.getListSimilarityThreshold(listSimilarityThreshold),
                                      mappingDictionary.getPathThreshold(pathThreshold),
                                      dataRescuer, language)
        htmlConverter.convertFilename(inputFile)
        htmlConverter.saveFilename(outputFile)
        logger.debug("Saved xml file = %s", outputFile)

        os.remove(inputFile)
        # Add the extracted elements into the file path

        return outputFile
