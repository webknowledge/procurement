

from parsing.models import ArchiveItem
from django.conf import settings

import os
import logging
import codecs
import urllib
# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")


class DataProcessor(object):
    
    '''
    It gets the data from the archive and stores it in the 
    intermediate folder
    '''
    
    def checkOutputFolder(self,folder):
        logger.debug("Check output folder "+folder)
        if not os.path.isdir(folder):
            logger.debug("Non existing. Create folder. "+folder)
            os.makedirs(folder)
        else:
            logger.debug("The output folder "+folder+" already exists")
    
    
    def process_item(self,item):
        
        object = ArchiveItem.objects.get(id=item['archivalId'])


        
        file_name=urllib.parse.quote(item['url'],safe='')
        path_file=settings.INTERMEDIATE_FOLDER+"/"+item['source']+'/'+file_name
                
        self.checkOutputFolder(settings.INTERMEDIATE_FOLDER)
        self.checkOutputFolder(settings.INTERMEDIATE_FOLDER+"/"+item['source'])

        sourceEncoding = "utf-8"
        provided=True
        contentType = object.contentType
        if contentType is not None:
            sourceCharset = contentType.split("charset=")
            if sourceCharset is not None and len(sourceCharset) > 1:
                logger.debug("Content Type: " + sourceCharset[1])
                sourceEncoding = sourceCharset[1]
            else:
                logger.debug("No content type provided for the object!")
                provided=False
        else:
            provided=False

        if sourceEncoding==None:
            provided=False


        logger.debug("->Dump into: "+path_file)
        if provided:
            logger.debug('Encoding provided')
            with codecs.open(path_file,'w','utf-8') as writer:
                writer.write(object.file.read().decode(sourceEncoding))
        else:
            logger.debug('Encoding not provided')
            with open(path_file,'wb') as writer:
                writer.write(object.file.read())
        logger.debug('Dumped')

        item['folder'].append((path_file,item['archivalId'],item['url']))

        return item