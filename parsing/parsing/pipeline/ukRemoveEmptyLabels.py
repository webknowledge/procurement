

from parsing.parser import NAMESPACE_REMOVAL
import logging
from lxml import etree


# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")


xslt_doc = etree.XML(NAMESPACE_REMOVAL)
namespace_cleaner = etree.XSLT(xslt_doc)



class DataProcessor(object):
    
    '''
    UK input files contain entries such as
    <DAY/>
    <MONTH/>
    <YEAR/>
    <TIME/>
    Remove any empty entry, except those we know that have to be used to fill the data.
    '''

    STOP_TAGS=["PT_OPEN","PT_CLOSE","PT_CANCELLED"]    
    
    def process_item(self,item):
        
        logger.debug("Removing empty dates from the input file")
        #get the input file in this case, only one
        inputFile,archivalId,url = item['folder'][0]
        
        with open(inputFile,'r') as reader:
            logger.debug("Load original file from %s"%inputFile)
            s =reader.read()
            xmlObject = etree.fromstring(s,parser=etree.XMLParser(encoding="utf-8"))
            xmlObject = namespace_cleaner(xmlObject)
        
        toRemove = self.removeDates(xmlObject.getroot())
        
        for i in toRemove:
            i.getparent().remove(i)
            
        
        with open(inputFile,'wb') as writer:
            writer.write(etree.tostring(xmlObject))
        
        logger.debug("File with removed entries dumped")
 
        return item
    
    def removeDates(self,xmlObject):

        toRemove = []
        if len(xmlObject)!=0:
            for child in xmlObject:
                if child.text==None or child.text=='':
                    if not child.tag in self.STOP_TAGS:  
                        toRemove.append(child)
                else:
                    result=self.removeDates(child)
                    if len(result)>0:
                        toRemove.extend(result)
        
        
        return toRemove
