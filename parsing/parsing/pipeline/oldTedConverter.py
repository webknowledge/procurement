
from lxml import etree
import logging
import re
import string
import os

regex = re.compile('[%s]|[\s]' % (re.escape(string.punctuation)))

logger = logging.getLogger("DigiwhistWebLogger")

KEYWORDS=set([
          "Estimated cost excluding VAT",
          "Estimated cost excluding VAT",
          "Address of the buyer profile",
          "The contracting authority is purchasing on behalf of other contracting authorities",
          "Total final value of the contract",
          "Value",
          "Other",
          "Payable documents",
          "Duration in month(s)",
          "Date",
          "Place",
          "Persons authorised to be present at the opening of tenders",
          "Division into lots",
          "Email"
          ])

class DataProcessor(object):
    
        
    def process_item(self,item):
        inputFolders = item['folder']
        #for inputFile in glob.glob(inputFolder+"/*META*"):
        toProcess = []
        for inputFile,archiveItem,url in inputFolders:
            logger.debug("Transform "+inputFile)
            listItems = self.process_file(inputFile)
            for x in listItems:
                toProcess.append((x,archiveItem,url))
            logger.debug("Remove inputFile: "+inputFile)
            os.remove(inputFile)
        #item['folder']=[toProcess,item['archivalId'],item['url']]
        item['folder']=toProcess
        return item
    
    def process_file(self,inputFile):
        with open(inputFile) as reader:
            parser=etree.fromstring(reader.read())
        
        counters={}
        currentCounter=0
        createdFiles=[]
        for doc in parser.xpath("/part/doc"):

            type = doc[0].xpath("refdtd")[0].text



            if type in counters:
                currentCounter=counters[type]
                counters[type]=counters[type]+1
            else:
                counters[type]=1
                currentCounter=1

            
            if type=="CONTRACT" or type=="CONTRACT_AWARD_SUM":           
                #process a doc element
                processed = etree.Element(type)
                
                #add the attributes
                for k,v in doc.items():
                    processed.set(k,v)
    
                for x in doc[0]:
                    conversion = self.findPattern(x)
                    if conversion!=None:
                        processed.append(conversion)
                    else:
                        processed.append(x)
                    
               
                #Write processed
                outputFile = inputFile+"_"+type+"_"+str(currentCounter)+"_converted" 
                with open(outputFile,"wb") as writer:
                    logger.debug("Create "+outputFile)
                    toWrite = etree.tostring(processed,pretty_print=True)
                    writer.write(toWrite)
                    createdFiles.append(outputFile)

                
        return createdFiles
        
    
    def findPattern(self,element):
        entry = self.findTimark(element)
        if entry==None:
            #Search for sections
            #Is this a section?
            #section
            grseq = element.xpath("grseq")

            if len(grseq)>0:
                entry = etree.Element("content")
                currentSectionElement = None
                for s in grseq:
                    #print etree.tostring(s)
                    sectionTitle = s.xpath("tigrseq")
                    if len(sectionTitle)>0:
                        title = sectionTitle[0].text
                        if title!=None:
                            title = self.tagClean(title)                        
                            currentSectionElement = etree.Element(title)
                            section_content = self.findMarklist(s)
                            if section_content!=None:
                                currentSectionElement.append(section_content)
                            entry.append(currentSectionElement)
                    else:
                        section_content = self.findMarklist(s)
                        if section_content!=None:
                            if currentSectionElement!=None:
                                currentSectionElement.append(section_content)
                            else:
                                entry.append(section_content)
                    
                
        return entry
    
    def findMarklist(self,element):
        entry=None
        
        #check if the marklist has any text
        p_text = element.xpath("marklist/p")
        lot_element = None
        if len(p_text)>0:
            if p_text[0].text!=None and "LOT NO" in p_text[0].text:
                #print p_text[0].text,p_text[1].text
                lot_element = etree.Element("lotDescription")
                lot_number = p_text[0].text.strip()[-1]
                lot_number_element = etree.Element("lotNumber")
                lot_number_element.text = self.textClean(lot_number)
                lot_element.append(lot_number_element)
                if len(p_text)>1:
                    lot_title = etree.Element("lotTitle")
                    lot_title.text = self.textClean(p_text[1].text.replace("TITLE: ",""))
                    lot_element.append(lot_title)
                entry = lot_element
        
        mark = element.xpath("marklist/mlioccur")
#         if len(mark)==0:
#             #second option
#             mark = element.xpath("grseq/marklist/mlioccur")
        aux=None                        
        if len(mark)>0:
            
            for mlioccurr in mark:
                
                #print etree.tostring(mlioccurr,pretty_print=True)
                aux = self.findTimark(mlioccurr)
                
                if aux!=None:
                    #add the attributes
                    for k,v in mlioccurr.items():
                        aux.set(k,v)
                    if entry!=None:
                        entry.append(aux)
                    else:
                        entry=aux                            

        return entry
        
        
        
    def findTimark(self,element):
        #nomark = element.xpath("normark")
        timark_entry = None
        #identifier
        timark = element.xpath("timark")
        #text
        txtmark = element.xpath("txtmark")
        
        
        
        if len(timark)>0:
            #for x in timark:
            #    print "-->",x.tag,": ",x.text
            newTag = self.tagClean(timark[0].text)
            #clean the tag
            
            timark_entry=etree.Element(newTag)
            
            
            if len(txtmark)>0:
                #check <p> entries
                for p in txtmark[0]:
                    
                    #print etree.tostring(p)
                    if p.tag == "p" and p.text!=None:
                        if p.text.find(':')!=-1:
                            #This is some child data
                            tokens = p.text.split(":")
                            if tokens[0] in KEYWORDS:
                                tag = self.tagClean(tokens[0])
                                text = self.textClean(tokens[1])
                                newChild = etree.SubElement(timark_entry,tag)
                                newChild.text=self.textClean(text)
                            else:
                                #there is a : in the text but is not a keyword
                                timark_entry.text = self.textClean(p.text)  
                                
                        else:
                            #this is just raw data
                            if timark_entry.text==None:
                                timark_entry.text = self.textClean(p.text)
                            else:
                                timark_entry.text+= "/n"+self.textClean(p.text)
                    else:
                        #other kind of entries
                        
                        #process each entry inside the txtmark
                        #normally they start with <p>
                        if len(p)>0:
                            #this is a complex object
                            #assume address element
                            aux=etree.Element(p[0].tag)
                            for p1 in p:
                                aux.append(p1)
                            timark_entry.append(aux)
                        else:
                            if timark_entry.text==None:
                                timark_entry.text=self.textClean(etree.tostring(p).decode('utf-8'))
                            else:
                                timark_entry.text=self.textClean(timark_entry.text+"\n"+etree.tostring(p).decode('utf-8'))
        
            
        return timark_entry
                
        
        
    def tagClean(self,tag):
        #newTag = re.sub(r'[^\w\s]', '_', tag)
        if tag==None:
            return tag

        newTag = regex.sub('_',tag)
        if len(re.findall(r'^\d+',newTag))>0:
            newTag = "N"+newTag
        
        #newTag = tag.rstrip('()?:!.,;').replace(" ","_")
        #newTag = newTag.replace('(','')
        #newTag = newTag.replace(')','')
        #newTag = newTag.replace(':','')
        #newTag = newTag.replace(',','_')
        #newTag = newTag.replace('/','_')
        #newTag = newTag.replace('.','_')
        return newTag
    
    def textClean(self,text):
        newText = text.rstrip('.')
        return newText
