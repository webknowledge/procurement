
import os


import logging
from lxml import etree
import goslate
from parsing.parser import namespace_cleaner
import codecs
# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")


class DataProcessor(object):
    '''
    Create a mew XML file translating the tags into English.
    '''

    def process_item(self,item):

        self.translator = goslate.Goslate()

        #get the input file in this case, only one
        inputFile,archivalId = item['folder'][0]

        with codecs.open(inputFile,'r',encoding="utf-8") as reader:
            logger.debug("Load original file from %s"%inputFile)
            s =reader.read()
            xmlObject = etree.fromstring(s,parser=etree.XMLParser(encoding="utf-8"))
            #xmlObject = etree.fromstring(s,parser=etree.XMLParser())
            xmlObject = namespace_cleaner(xmlObject)

        xmlObject = xmlObject.getroot()

        outputFile = inputFile.split('.')[0]+"_translated.xml"
        item['folder']=[(outputFile,archivalId)]
        #get a common header for all the files
        #remove the notices entry

        listOfTags = self.convertChild(xmlObject)

        for i in self.translator.translate(listOfTags,'en'):
            print i




        return item

        convertedObject = self.convertChild(xmlObject)

        #print etree.tostring(convertedObject,pretty_print=True)

        self.writeOutputFile(convertedObject,outputFile)
        #now iterate all the elements of the file and create a separated file

        logger.debug("Remove original file %s"%inputFile)
        os.remove(inputFile)
        return item



    def convertChild(self,original):

        tags = []
        tags.append(original.tag)

        for child in original:
            childrenTags = self.convertChild(child)
            tags.extend(childrenTags)

        return tags



    def convertChild2(self,original):

        originalTag = original.tag
        originalTag.replace('_',' ')
        print "Translate: ",originalTag
        print type(originalTag)
        translator = goslate.Goslate()
        translatedTag = translator.translate(originalTag,'en')
        print "-->",translatedTag
        translatedTag.replace(' ','_')


        if len(original.attrib)==0 and original.text==None and len(original.getchildren())==0:
            #empty node
            return None

        #simply copy the element
        newChild=etree.Element(translatedTag)

        for k,v in original.attrib.iteritems():
            newChild.attrib[k]=v

        newChild.text=original.text

        for child in original:
            newChildren=self.convertChild(child)
            if newChildren!=None:
                newChild.append(newChildren)

        return newChild

    def cleanString(self,oldString):
        '''
        Transforms a string into an XML compatible string.
        :param oldString:
        :return:
        '''
        newString = oldString.replace(' ','_')
        newString = newString.replace('.','_')
        newString = newString.replace('-','_')
        newString = newString.replace('(','_')
        newString = newString.replace(')','_')
        newString = newString.replace(',','')
        newString = newString.replace(':','')
        newString = newString.replace(u'\xa7','')
        newString = newString.replace('/','')
        newString = newString.replace('?','')
        newString = newString.replace('%','')

        return newString.encode("ascii","ignore")


    def writeOutputFile(self,convertedObject,outputFile):
        '''
        Write the new converted xml file to a new location
        :param convertedObject:
        :param outputFile:
        :return:
        '''

        with codecs.open(outputFile,'w',encoding="ascii") as writer:
            writer.write(etree.tostring(convertedObject))