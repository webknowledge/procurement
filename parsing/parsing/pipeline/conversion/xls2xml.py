import codecs
import logging
import xlrd as xlrd
from yattag import Doc, indent
from parsing.pipeline.conversion.base2xml import Base2Xml

logger = logging.getLogger("Csv2Xml")


class Xls2Xml(Base2Xml):
    def processSingleData(self, filename):
        processingResult = []

        csvData = xlrd.open_workbook(filename).sheet_by_index(0)
        csvData.get_rows()

        header = [item.replace(' ', '_') for item in csvData.row_values(0)]
        for rowNum in range(1, csvData.nrows, 1):
            outFilename = self.getOutputFilename(filename, rowNum)
            extraHeader, extraValue = self.getExtraFields(filename)
            if self._processSingleRow(outFilename, extraHeader, extraValue, header, csvData.row_values(rowNum)):
                processingResult.append(outFilename)
                logger.info("Finished writing: " + outFilename)
        return processingResult

    @staticmethod
    def _processSingleRow(outFilename, extraHeader, extraValue, header, row):
        doc, tag, text = Doc().tagtext()
        with tag("document"):
            for i in range(len(extraHeader)):
                with tag(extraHeader[i]):
                    text(extraValue[i])

            for i in range(len(header)):
                with tag(header[i]):
                    text(row[i])

        with codecs.open(outFilename, "w+", "utf-8") as ifile:
            ifile.write(indent(doc.getvalue(), indentation=' ' * 4, newline='\n'))
        return True
