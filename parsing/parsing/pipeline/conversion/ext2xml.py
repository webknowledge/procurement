import os

from parsing.pipeline.conversion.base2xml import Base2Xml
from parsing.pipeline.conversion.csv2xml import Csv2Xml
from parsing.pipeline.conversion.xls2xml import Xls2Xml


class Ext2Xml(Base2Xml):
    extensionProcessor = {".csv": Csv2Xml, ".xls": Xls2Xml}

    def processSingleData(self, filename):
        fn, fileExtension = os.path.splitext(filename)
        processor = self.extensionProcessor[fileExtension]
        if processor is None:
            return []
        else:
            processor = processor(self._processorPath)
            return processor.processSingleData(filename)
