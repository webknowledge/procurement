import os
from os.path import basename, splitext

documentType = {
    "anunturi-participare": "call-for-tenders",
    "invitatii-de-participare": "call-for-offers",
    "contracte": "contracts",
    "cumparari-directe": "direct-contracts",
}


class Base2Xml:
    def __init__(self, processorPath):
        self._processorPath = processorPath

    def processData(self, fileList):
        processingResult = []
        for item in fileList:
            processingResult.extend(self.processSingleData(item))
        return processingResult

    def processSingleData(self, filename):
        return []

    def getOutputFilename(self, inputFilename, rowNumber):
        fn, extension = splitext(basename(inputFilename))
        parentPath = self._processorPath + "/" + fn + "/"
        if not os.path.exists(parentPath):
            os.makedirs(parentPath)
        return parentPath + fn + "." + str(rowNumber) + ".xml"

    @staticmethod
    def getExtraFields(inputFilename):
        fileDocType = "unknown"
        fn, extension = splitext(basename(inputFilename))
        for (prefix, docType) in documentType.items():
            #if fn.startswith(prefix):
            if prefix in fn:
                fileDocType = docType
                break
        return ["docType"], [fileDocType]
