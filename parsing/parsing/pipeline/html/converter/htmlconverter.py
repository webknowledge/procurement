import codecs
import logging

from bs4 import BeautifulSoup
from yattag import Doc, indent

from parsing.pipeline.html.metrics.similarities import similarity_path
from parsing.pipeline.html.parse.simpleParsing import fixEmail, traverseDelegate


class HtmlConverter:
    def __init__(self, mappingDictionary, tokenThreshold, listSimilarityThreshold, pathThreshold, dataRescuer,
                 language):
        self._mappingDictionary = mappingDictionary
        self._tokenThreshold = tokenThreshold
        self._listSimilarityThreshold = listSimilarityThreshold
        self._pathThreshold = pathThreshold
        self._dataRescuer = dataRescuer
        self._language = language

        logging.debug("HTML Converter using the parameters, tokenThreshold = %s, listSimilarityThreshold = %s, "
                     "pathThreshold = %s", tokenThreshold, listSimilarityThreshold, pathThreshold)

        self._previousKey = "Root"
        self._previousPath = []

        self._textBuffer = ""

        self._doc, self._tag, self._text = Doc().tagtext()

    def convertFilename(self, filename):
        with codecs.open(filename, "r+", "utf-8") as ifile:
            fileContent = ifile.read()
            self.convertFileContent(fileContent, filename)

    def convertFileContent(self, fileContent, filename):
        soup = BeautifulSoup(fixEmail(fileContent), 'html5lib')
        self.convertSoup(soup, filename)

    def convertSoup(self, soup, filename):
        with self._tag("document"):
            rootDocument = soup

            if self._dataRescuer.canProcessNonStandard():
                nonStandardResult = self._dataRescuer.parseNonStandard(soup)
                if nonStandardResult is not None and len(nonStandardResult) > 0:
                    self._flushParsedDictionary(nonStandardResult)

            if self._dataRescuer.isRootChangeable(rootDocument):
                rootDocument = self._dataRescuer.changeRoot(rootDocument)
                logging.debug("Changed processing root to: " + rootDocument.name)

            traverseDelegate(rootDocument, self._addPair, self._dataRescuer)
        logging.debug("Finished processing filename: %s !", filename)

    def saveFilename(self, outputFilename):
        with codecs.open(outputFilename, "w+", "utf-8") as ifile:
            ifile.write(indent(self._doc.getvalue(), indentation=' ' * 4, newline='\n'))

    def _addPair(self, path, text):
        joinedPath = "/".join(path) + "/" + text

        (currentKey, similarityMeasure) = self._mappingDictionary.getTextEncodingSimilarity(text, self._tokenThreshold)
        if (currentKey is None) or (similarityMeasure < self._listSimilarityThreshold):
            if similarity_path(self._previousPath, path) <= self._pathThreshold:
                # seems to be a value
                logging.debug(" MATCHED VAL: %s = %s (%s)", self._previousKey, text, joinedPath)
                self._textBuffer = self._textBuffer + " " + text
            else:
                self._flushTextBuffer()
                if self._dataRescuer.isTextRescuable(path, text):
                    (currentKey, rescuedText) = self._dataRescuer.rescueText(path, text)
                    if currentKey is not None and rescuedText is not None and len(rescuedText) > 0:
                        self._previousKey = currentKey
                        self._previousPath = path
                        self._textBuffer = self._textBuffer + " " + rescuedText
                else:
                    logging.debug("IGNORED TEXT: %s (%s)", text, joinedPath)
        else:
            if self._mappingDictionary.isIgnore(currentKey):
                self._flushTextBuffer()
                logging.debug(" IGNORED KEY: %s (%s)", text, joinedPath)
            else:
                self._flushTextBuffer()
                logging.debug(" MATCHED KEY: %s = %s (%s)", currentKey, text, joinedPath)
                self._previousKey = currentKey
                self._previousPath = path

    def _flushTextBuffer(self):
        if len(self._textBuffer) > 0:
            with self._tag(self._generateKeyName()):
                self._text(self._textBuffer.strip())
            self._textBuffer = ""

    def _generateKeyName(self):
        if self._language is None:
            return "k" + self._previousKey
        else:
            return "k_" + self._language + "_" + self._previousKey

    def _flushParsedDictionary(self, dictionaryResult):
        for key, value in dictionaryResult.items():
            with self._tag(key):
                self._text(value.strip())
