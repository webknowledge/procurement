from parsing.pipeline.html.converter.datarescuers import DataRescue, CzRescuer, PtRescuer, LtRescuer, ChRescuer, EeRescuer, BgRescuer, \
    HrRescuer

rescuers = {
    "bg": BgRescuer,
    "ch-fr": ChRescuer,
    "ch-it": ChRescuer,
    "ch-en": ChRescuer,
    "ch-de": ChRescuer,
    "cz": CzRescuer,
    "ee": EeRescuer,
    "hr": HrRescuer,
    "pt": PtRescuer,
    "lt": LtRescuer
}


def getRescueClass(countryCode):
    rescuer = rescuers.get(countryCode, DataRescue)
    return rescuer()


if __name__ == "__main__":
    print(type(getRescueClass("cz")).__name__)
    print(type(getRescueClass("pt")).__name__)
    print(type(getRescueClass("fr")).__name__)
