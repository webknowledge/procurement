# -*- coding: utf-8 -*-
import os
from glob import glob
from multiprocessing.pool import Pool
import logging

import sys

from parsing.pipeline.html.converter.datarescue import getRescueClass
from parsing.pipeline.html.converter.htmlconverter import HtmlConverter
from parsing.pipeline.html.dictionary.encodingdictionary import EncodingDictionary
from parsing.pipeline.html.serialisers.fileSerializer import FileSerializer

_pathThreshold = 1
_tokenThreshold = 0.7
_listSimilarityThreshold = 0.7

logger = logging.getLogger("batchXmlExporter")


def convertHtmlToXml(_filename, _outputName, _mappingDictionary, _dataRescuer, _languageCode):
    htmlConverter = HtmlConverter(_mappingDictionary,
                                  _mappingDictionary.getTokenThreshold(_tokenThreshold),
                                  _mappingDictionary.getListSimilarityThreshold(_listSimilarityThreshold),
                                  _mappingDictionary.getPathThreshold(_pathThreshold),
                                  _dataRescuer, _languageCode)
    htmlConverter.convertFilename(_filename)
    htmlConverter.saveFilename(_outputName)
    logger.info("Saved xml file = %s", _outputName)


if __name__ == "__main__":
    logging.basicConfig(stream=sys.stdout, level=logging.INFO,
                        format='%(asctime)s [%(name)s] %(levelname)s %(message)s',
                        datefmt='%a, %d %b %Y %H:%M:%S')

    if len(sys.argv) < 2:
        logger.error("Language must be provided as a parameter ...")
        exit()

    languageCode = None
    if len(sys.argv) == 3:
        languageCode = sys.argv[2]
        logger.info("Using Language code = " + languageCode)

    countryCode = sys.argv[1]
    encodingFile = "res/{}/encoding.json".format(countryCode)
    outFolder = "res/{}/converted".format(countryCode)
    inFolder = "res/{}/raw/*".format(countryCode)

    if not os.path.exists(outFolder):
        logger.info("Output folder (%s) does not exist, creating folder ...", outFolder)
        os.makedirs(outFolder)
    else:
        logger.info("Output folder (%s) exists, re-using it ...", outFolder)

    logger.info("Using input folder = %s", inFolder)

    serializer = FileSerializer(encodingFile)
    mappingDictionary = EncodingDictionary()
    mappingDictionary.loadEncoding(serializer)

    dataRescuer = getRescueClass(countryCode)

    corpora_list = glob(inFolder)

    pool = Pool(processes=4)
    for filename in corpora_list:
        outputName = outFolder + "/" + os.path.basename(filename) + ".xml"
        pool.apply_async(convertHtmlToXml, args=(filename, outputName, mappingDictionary, dataRescuer, languageCode))

    pool.close()
    pool.join()
