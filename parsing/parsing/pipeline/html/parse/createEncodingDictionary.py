import codecs
import json
import logging
import os

import sys
from collections import defaultdict

from DigiwhistWeb.parsing.pipeline.html.dictionary.xpathtextdictionary import XPathTextDictionary
from DigiwhistWeb.parsing.pipeline.html.serialisers.fileSerializer import FileSerializer

logger = logging.getLogger("EncodingDictionaryCreation")


def loadDictionary(dictionaryName):
    mongo_serializer = FileSerializer(dictionaryName)
    dictionary = XPathTextDictionary(True, 0.7)
    dictionary.load(mongo_serializer, None)
    mongo_serializer.close()
    return dictionary


def saveEncodingDictionary(dictionaryName, dictionary):
    representation = defaultdict()
    representation["keySet"] = []
    representation["ignoreSet"] = []
    representation["encoding"] = dictionary.textEncoding()

    representations = [representation]

    with codecs.open(dictionaryName, "w+", "utf-8") as ifile:
        ifile.write(json.dumps(representations, sort_keys=False, indent=2))


if __name__ == "__main__":
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG,
                        format='%(asctime)s [%(name)s] %(levelname)s %(message)s',
                        datefmt='%a, %d %b %Y %H:%M:%S')

    if len(sys.argv) < 2:
        logger.error("Language must be provided as a parameter ...")
        exit()

    lang = sys.argv[1]
    translatedFile = "res/{}/data.translated.json".format(lang)
    dataFile = "res/{}/data.json".format(lang)
    encodingFile = "res/{}/encoding.json".format(lang)

    if os.path.isfile(encodingFile):
        logger.error("Encoding file exists = %s. This script does not override the file. "
                     "Please delete the file and try again ...", encodingFile)
        sys.exit(1)

    logger.info("Using language = %s, translatedFilename=%s dataFilename=%s", lang, translatedFile, dataFile)

    if os.path.isfile(translatedFile):
        _corporaDict = loadDictionary(translatedFile)
        logger.info("Loaded translated file as input ...")
    else:
        _corporaDict = loadDictionary(dataFile)
        logger.info("Loaded data file as input ...")

    saveEncodingDictionary(encodingFile, _corporaDict)
