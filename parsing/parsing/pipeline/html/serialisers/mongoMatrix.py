import math
from pymongo import MongoClient


class mongoMatrix:
    def __init__(self, host, port, database, collection, count=-1):
        self.client = MongoClient(host, port)
        db = self.client[database]
        self.collection = db[collection]
        self._count = count
        self._init_matrix(self._count)

    def _init_matrix(self, count):
        if count > 0:
            self._matrix = [[0 for x in range(count)] for x in range(count)]
        else:
            self._matrix = None

    def count(self):
        return self._count

    def set(self, i, j, v):
        if self._matrix is not None and i < self._count and j < self._count:
            self._matrix[i][j] = v

    def get(self, i, j, default_value):
        if self._matrix is not None and i < self._count and j < self._count:
            return self._matrix[i][j]
        else:
            return default_value

    def matrix(self):
        return self._matrix

    def save(self):
        self.collection.drop()
        for i in range(self._count):
            for j in range(self._count):
                tmp_doc = {"i": i, "j": j, "value": self._matrix[i][j]}
                self.collection.insert_one(tmp_doc)

    def load(self):
        self._count = max(0, int(math.sqrt(self.collection.count())))
        self._init_matrix(self._count)
        for i in range(self._count):
            for j in range(self._count):
                tmp_doc = self.collection.find_one({"i": i, "j": j})
                if tmp_doc is not None:
                    self._matrix[i][j] = tmp_doc["value"]

    def to_string(self):
        if self._matrix is not None:
            return "\n".join([" ".join(["{0:.5f}".format(col) for col in row]) for row in self._matrix])
        else:
            return ""

    def close(self):
        self.client.close()
