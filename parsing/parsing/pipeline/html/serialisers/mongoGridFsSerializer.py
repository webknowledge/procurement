import json

import gridfs
from pymongo import MongoClient


class MongoGridFsSerializer:
    def __init__(self, host, port, database, collection):
        self.client = MongoClient(host, port)
        self.db = self.client[database]
        self.collection_name = collection
        self.collection = gridfs.GridFS(self.db, collection=collection)

    def serialize(self, data):
        return self.collection.put(json.dumps(data))

    def deserialize(self, object_id):
        return self.collection.find_one({"_id": object_id})

    def deserialize_object(self, lookup):
        return self.collection.find_one(lookup)

    def count(self):
        return self.db[self.collection_name].count()

    def cleanup(self):
        self.db.drop_collection(self.collection_name)
        return True

    def close(self):
        if self.client is not None:
            self.client.close()
