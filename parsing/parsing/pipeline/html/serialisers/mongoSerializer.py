from pymongo import MongoClient


class MongoSerializer:
    def __init__(self, host, port, database, collection):
        self.client = MongoClient(host, port)
        db = self.client[database]
        self.collection = db[collection]

    def serialize(self, data):
        return self.collection.insert_one(data).inserted_id

    def deserialize(self, object_id):
        return self.collection.find_one({"_id": object_id})

    def deserialize_object(self, lookup):
        return self.collection.find_one(lookup)

    def count(self):
        return self.collection.count()

    def cleanup(self):
        self.collection.drop()
        return True

    def close(self):
        if self.client is not None:
            self.client.close()
