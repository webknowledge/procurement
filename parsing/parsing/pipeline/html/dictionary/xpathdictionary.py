from collections import defaultdict

from parsing.pipeline.html.dictionary.dictionary import BaseDictionary


class XPathDictionary(BaseDictionary):
    def __init__(self, create_encoding):
        self._frequencies = defaultdict(int)
        self._encoding = defaultdict(str)
        self._create_encoding = create_encoding
        self._n_frequencies = defaultdict(int)
        # temporary collection, un-serialized
        self._local_n_frequencies = set()

    def end_document(self):
        for key in self._local_n_frequencies:
            count = self._n_frequencies[key]
            if count is None:
                count = 0
            count += 1
            self._n_frequencies[key] = count
        self._local_n_frequencies.clear()

    def add(self, xpath, text):
        encoding = self._determine_encoding(xpath)
        if encoding is not None:
            freq = self._frequencies.get(encoding)
            if freq is None:
                freq = 0
            freq += 1
            self._frequencies[encoding] = freq
            self._local_n_frequencies.add(encoding)

    def frequencies(self):
        return self._frequencies

    def encoding(self):
        return self._encoding

    def n_frequencies(self, encoding):
        return self._n_frequencies.get(encoding, 0)

    def save(self, serializer):
        serialized = defaultdict()
        serialized["frequencies"] = self._frequencies
        serialized["encoding"] = self._encoding
        serialized["n-frequencies"] = self._n_frequencies
        return serializer.serialize(serialized)

    def load(self, serializer, object_id):
        serialized = serializer.deserialize(object_id)
        self._frequencies = serialized["frequencies"]
        self._encoding = serialized["encoding"]
        self._n_frequencies = serialized["n-frequencies"]

    def _determine_encoding(self, xpath):
        xpath_tuple = "/".join(xpath)
        encoding = self._encoding.get(xpath_tuple)
        if encoding is None and self._create_encoding:
            encoding = len(self._encoding)
            self._encoding[xpath_tuple] = encoding
        return str(encoding)
