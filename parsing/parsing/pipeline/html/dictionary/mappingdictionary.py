# -*- coding: utf-8 -*-

import json
from collections import defaultdict

from parsing.pipeline.html.metrics.similarities import len_similarity_sets, similarity_sets
from parsing.pipeline.html.parse.simpleParsing import regexpTokenize

fieldSeparator = "."
fieldLabel = "LABEL"
max_tokens = 25
len_similarity_threshold = 4


class MappingDictionary:
    def __init__(self):
        self._encoding = defaultdict(set)

    def loadMapping(self, externalDictionary):
        self._loadMapping(externalDictionary, None)

    def _loadMapping(self, externalDictionary, rootPath):
        for (key, value) in externalDictionary.items():
            if value is not None:
                localKey = self._createRootPath(rootPath, key)
                if type(value) is dict:
                    self._loadMapping(value, localKey)
                elif type(value) in [list, tuple, str]:
                    self._addMapping(localKey, value)
                else:
                    print("invalid type for key = " + localKey)

    def _addMapping(self, key, value):
        if value is not None:
            if type(value) in [list, tuple]:
                for v in value:
                    self._addMapping(key, v)
            elif type(value) in [str]:
                self._addSingleTextValue(key, value)
            else:
                print("invalid type for key = " + key)

    def _addSingleTextValue(self, key, textValue):
        textList = tuple(regexpTokenize((textValue.lower())))
        self._encoding[key].add(textList)

    def addTextEncodingSimilarity(self, text, tokenSimilarityThreshold, listSimilarityThreshold):
        textList = tuple(regexpTokenize(text.lower()))
        if len(textList) > max_tokens:
            return None

        for (key, values) in self._encoding.items():
            for value in values:
                if len(value) <= max_tokens:
                    if textList == value:
                        return key
                    elif len_similarity_sets(textList, value) <= len_similarity_threshold:
                        if similarity_sets(textList, value, tokenSimilarityThreshold) > listSimilarityThreshold:
                            values.add(textList)
                            return key
        return None

    def getTextEncodingSimilarity(self, text, tokenSimilarityThreshold):
        textList = tuple(regexpTokenize(text.lower()))
        if len(textList) > max_tokens:
            return None, 0.0

        maxKey = None
        maxSimilarity = 0

        for (key, values) in self._encoding.items():
            for value in values:
                if len(value) <= max_tokens:
                    if textList == value:
                        # perfect match, should not continue
                        return key, 1.0
                    elif len_similarity_sets(textList, value) <= len_similarity_threshold:
                        currentSimilarity = similarity_sets(textList, value, tokenSimilarityThreshold)
                        if currentSimilarity > maxSimilarity:
                            maxSimilarity = currentSimilarity
                            maxKey = key

        return maxKey, maxSimilarity

    @staticmethod
    def _createRootPath(rootPath, key):
        if rootPath is None:
            return key
        elif key is fieldLabel:
            return rootPath
        else:
            return fieldSeparator.join((rootPath, key))

    @staticmethod
    def _convertSetDictionary(setDictionary):
        listDictionary = defaultdict(list)
        for (key, values) in setDictionary.items():
            for value in values:
                listDictionary[key].append(list(value))
        return listDictionary

    def toString(self):
        return json.dumps(self._convertSetDictionary(self._encoding), sort_keys=True, indent=2)
