# -*- coding: utf-8 -*-

import json
from collections import defaultdict
from parsing.pipeline.html.metrics.similarities import len_similarity_sets, similarity_sets
from parsing.pipeline.html.parse.simpleParsing import regexpTokenize

fieldSeparator = "."
fieldLabel = "LABEL"
max_tokens = 25
len_similarity_threshold = 4


class EncodingDictionary:
    def __init__(self):
        self._encoding = defaultdict(set)
        self._keySet = set()
        self._ignoreSet = set()

        self._pathThreshold = None
        self._tokenThreshold = None
        self._listSimilarityThreshold = None

    def loadEncoding(self, serializer):
        # load representation
        representation = serializer.deserialize_object(None)
        self._keySet = set([str(element) for element in representation["keySet"]])
        self._ignoreSet = set([str(element) for element in representation["ignoreSet"]])
        self._encoding = representation["encoding"]

        self._pathThreshold = representation.get("pathThreshold", None)
        if self._pathThreshold is not None:
            self._pathThreshold = int(self._pathThreshold)

        self._tokenThreshold = representation.get("tokenThreshold", None)
        if self._tokenThreshold is not None:
            self._tokenThreshold = float(self._tokenThreshold)

        self._listSimilarityThreshold = representation.get("listSimilarityThreshold", None)
        if self._listSimilarityThreshold is not None:
            self._listSimilarityThreshold = float(self._listSimilarityThreshold)
        # filter unused keys
        _allKeys = set(self._encoding.keys())
        _allKeys.difference_update(self._keySet)
        _allKeys.difference_update(self._ignoreSet)
        for key in _allKeys:
            del self._encoding[key]

    def isKey(self, encoding):
        return encoding in self._keySet

    def isIgnore(self, encoding):
        return encoding in self._ignoreSet

    def getPathThreshold(self, defaultValue):
        if self._pathThreshold is not None:
            return self._pathThreshold
        else:
            return defaultValue

    def getTokenThreshold(self, defaultValue):
        if self._tokenThreshold is not None:
            return self._tokenThreshold
        else:
            return defaultValue

    def getListSimilarityThreshold(self, defaultValue):
        if self._listSimilarityThreshold is not None:
            return self._listSimilarityThreshold
        else:
            return defaultValue

    def getTextEncodingSimilarity(self, text, tokenSimilarityThreshold):
        textList = tuple(regexpTokenize(text.lower()))
        if len(textList) > max_tokens:
            return None, 0.0

        maxKey = None
        maxSimilarity = 0

        for (key, values) in self._encoding.items():
            for value in values:
                if len(value) <= max_tokens:
                    if textList == value:
                        # perfect match, should not continue
                        return key, 1.0
                    elif len_similarity_sets(textList, value) <= len_similarity_threshold:
                        currentSimilarity = similarity_sets(textList, value, tokenSimilarityThreshold)
                        if currentSimilarity > maxSimilarity:
                            maxSimilarity = currentSimilarity
                            maxKey = key

        return maxKey, maxSimilarity

    @staticmethod
    def _convertSetDictionary(setDictionary):
        listDictionary = defaultdict(list)
        for (key, values) in setDictionary.items():
            for value in values:
                listDictionary[key].append(list(value))
        return listDictionary

    def toString(self):
        return json.dumps(self._convertSetDictionary(self._encoding), sort_keys=True, indent=2)
