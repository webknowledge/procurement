from collections import defaultdict

from parsing.pipeline.html.dictionary.dictionary import BaseDictionary


class FrequencyDocument(BaseDictionary):
    def __init__(self, document_name, document_index, encoding_dictionary):
        self._frequencies = defaultdict(int)
        self._tf_idf = defaultdict(int)

        self._document_name = document_name
        self._document_index = document_index
        self._encoding = encoding_dictionary
        self._size = 0

    def add(self, xpath, text):
        encoding = self._determine_encoding(xpath)
        if encoding is not None:
            freq = self._frequencies.get(encoding)
            if freq is None:
                freq = 0
            freq += 1
            self._frequencies[encoding] = freq
        self._size += 1

    def size(self):
        return self._size

    def frequencies(self):
        return self._frequencies

    def tf_idf(self):
        return self._tf_idf

    def name(self):
        return self._document_name

    def index(self):
        return self._document_index

    def save(self, serializer):
        serialized = defaultdict()
        serialized["name"] = self._document_name
        serialized["index"] = self._document_index
        serialized["size"] = self._size
        serialized["frequencies"] = self._frequencies
        serialized["tf-idf"] = self._tf_idf
        return serializer.serialize(serialized)

    def load(self, serializer, object_id):
        serialized = serializer.deserialize(object_id)
        self._load(serialized)

    def load_object(self, serializer, lookup):
        serialized = serializer.deserialize_object(lookup)
        self._load(serialized)

    def _load(self, serialized):
        self._document_name = serialized["name"]
        self._document_index = serialized["index"]
        self._size = serialized["size"]
        self._frequencies = serialized["frequencies"]
        self._tf_idf = serialized["tf-idf"]

    def _determine_encoding(self, xpath):
        xpath_tuple = "/".join(xpath)
        encoding = self._encoding.get(xpath_tuple)
        return str(encoding)
