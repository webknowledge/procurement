
import os

import logging
from lxml import etree
from parsing.parser import NAMESPACE_REMOVAL
import codecs
# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")


xslt_doc = etree.XML(NAMESPACE_REMOVAL)
namespace_cleaner = etree.XSLT(xslt_doc)

class DataProcessor(object):
    '''
    Slovak XML files are not really containing data description.
    These files are describing the layout of the xml in case of building a HTML page.
    We try to convert these files in order to be a real XML self-descripted file.

    Not all the XMLs follow the same structure. A first bunch contains an HTML description
    using XML. These files are converted using the code below. A second bunch contains raw HTML
    code inside the XML file.
    '''

    def process_item(self,item):

        #get the input file in this case, only one
        inputFile,archivalId,url = item['folder'][0]

        with codecs.open(inputFile,'r',encoding="utf-8") as reader:
            logger.debug("Load original file from %s"%inputFile)
            s =reader.read()
            xmlObject = etree.fromstring(s,parser=etree.XMLParser(encoding="utf-8"))
            #xmlObject = etree.fromstring(s,parser=etree.XMLParser())
            xmlObject = namespace_cleaner(xmlObject)

        xmlObject = xmlObject.getroot()

        outputFile = inputFile.split('.')[0]+"_converted.xml"
        item['folder'][0]=(outputFile,archivalId,url)


        #check if the XML file is simply a container of HTML code
        if xmlObject.tag=="oznamenie":
            with open(outputFile,'w') as writer:
                writer.write('<xml>Ignore this all format file</xml>')
        else:
            convertedObject = self.convertChild(xmlObject)
            #print etree.tostring(convertedObject,pretty_print=True)
            self.writeOutputFile(convertedObject,outputFile)
            #now iterate all the elements of the file and create a separated file

            logger.debug("Remove original file %s"%inputFile)
            os.remove(inputFile)

        return item



    def convertChild(self,original):

        if len(original.attrib)==0 and original.text==None and len(original.getchildren())==0:
            #empty node
            return None

        newChild=None
        if original.tag == 'ZovoForm':
            newChild = etree.Element(original.tag)
            #remove rpp entries to avoid stupidly repetitive elements
        elif original.tag == 'DropDownListValue' or original.tag == 'SelectListValue':
            newChild = etree.Element(original.tag)
        elif 'Title' in original.attrib:
            #the tag of the element is the title
            tagName = self.cleanString(original.attrib['Title'])
            newChild=etree.Element(tagName)
        elif 'FormComponentId' in original.attrib:
            tagName = self.cleanString(original.attrib['FormComponentId'])
            newChild=etree.Element(tagName)
        else:
            #simply copy the element
            newChild=etree.Element(original.tag)

        for k,v in original.attrib.iteritems():
            if 'rpp' in v:
                #if repetitive entries, modify them
                candidate = v
                newTag = self.cleanString(v.split('-')[1])
                newChild.tag=newTag
            newChild.attrib[k]=v

        newChild.text=original.text

        for child in original:
            newChildren=self.convertChild(child)
            if newChildren!=None:
                newChild.append(newChildren)

        #print "Process: ",original.tag,"---->",newChild.tag,newChild.attrib,newChild

        return newChild

    def cleanString(self,oldString):
        '''
        Transforms a string into an XML compatible string.
        :param oldString:
        :return:
        '''
        newString = oldString.replace(' ','_')
        newString = newString.replace('.','_')
        newString = newString.replace('-','_')
        newString = newString.replace('(','_')
        newString = newString.replace(')','_')
        newString = newString.replace(',','')
        newString = newString.replace(':','')
        newString = newString.replace(u'\xa7','')
        newString = newString.replace('/','')
        newString = newString.replace('?','')
        newString = newString.replace('%','')
        newString = newString.replace(';','')

        return newString.encode("ascii","ignore")


    def writeOutputFile(self,convertedObject,outputFile):
        '''
        Write the new converted xml file to a new location
        :param convertedObject:
        :param outputFile:
        :return:
        '''

        with codecs.open(outputFile,'w') as writer:
            writer.write(etree.tostring(convertedObject).decode("utf-8"))