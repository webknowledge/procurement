import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '&55^ia0*1^0)o2t^721_98#ui1axk=8&i_&&(em26#%vcoap54'


ROOT_URLCONF='urls.py'


'''
This function initializes a large bunch of databases
'''

userDB="jmt78"
passwordDB="dwhist_web_passwd"
hostDB='localhost'

def initializeDBs(availableDBs):
    dbs={
    'default':{
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
         'NAME': 'default',
         'USER': userDB,
         'PASSWORD':passwordDB,
         'HOST': hostDB
        }
    }

    for db in availableDBs:
        dbs[db]={
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
         'NAME': db,
         'USER': userDB,
         'PASSWORD':passwordDB,
         'HOST': hostDB
    }
    return dbs


INSTALLED_APPS = (
    'parsing',
    'prototype'
)



# settings.py
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    
    'formatters': {
        'verbose': {
            'format': '[%(levelname)s %(asctime)s%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s'
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
       'file':{
            'class':'logging.handlers.RotatingFileHandler',
            'formatter':'verbose',
            'filename':'/tmp/dwhist.log',
            'maxBytes':104857600
        }

    },
    'loggers': {
        'DigiwhistWebLogger': {
            'handlers': ['console','file'],
            'level': 'INFO',
        },
    }
}


#Mongo archive configuration
ARCHIVE_DATABASE={"archive":"archive",
                  "host":"localhost",
                  "port":27017
                  }



AVAILABLE_DATABASES=set(["ted",
                         "pl",
                         "be",
                         "lv",
                         "es",
                         "uk",
                         "dk",
                         "nl",
                         "fi",
                         "cz",
                         "fr",
                         "pt",
                         "ch",
                         "rs",
                         "no",
                         "oldTed",
                         "ro",
                         "ee",
                         "ie",
                         "it",
                         "lt",
                         "hu",
                         "se",
                         "bg",
                         "cy",
                         "hr",
                         "sk"
                         ])



DATABASES=initializeDBs(AVAILABLE_DATABASES)

#Folder containing the dictionaries
DICTIONARY_FOLDER="dictionaries"

#Folder where the mapping files are stored
MAPPING_FOLDER="mappings"

#where to store intermediate created files
INTERMEDIATE_FOLDER="/tmp/intermediate"


#Pipeline definitions
PREPROCESS_PIPELINES = {
            "ted":[
                    'parsing.pipeline.getFile',
                    'parsing.pipeline.tedUncompress'
                    ],
            "oldTed":[
                    'parsing.pipeline.getFile',
                    'parsing.pipeline.oldTedUncompress',
                    'parsing.pipeline.oldTedConverter'
                    ],
            "pl":[
                    'parsing.pipeline.getFile',
                    'parsing.pipeline.plUncompress'
                ],
            "be":[
                    'parsing.pipeline.getFile'
                  ],
            "uk":[
                    'parsing.pipeline.getFile',
                    'parsing.pipeline.ukRemoveEmptyLabels',
                    'parsing.pipeline.ukSplitInput'
                  ],
            "es":[
                   'parsing.pipeline.getFile'
                   ],
            "ro":[
                   'parsing.pipeline.getFile',
                   'parsing.pipeline.roConversion'
                   ],
            "cz":[
                'parsing.pipeline.getFile',
            ],
            "nl":[
                'parsing.pipeline.getFile',
                'parsing.pipeline.dictionaryConversion'
            ],
            "dk":[
                'parsing.pipeline.getFile',
                'parsing.pipeline.dictionaryConversion'
            ],
            "sk":[
                    'parsing.pipeline.getFile',
                    'parsing.pipeline.skConversion',
                  ],
            "fr":[
                    'parsing.pipeline.getFile',
                    'parsing.pipeline.dictionaryConversion'
                  ],
            "pt":[
                    'parsing.pipeline.getFile',
                    'parsing.pipeline.dictionaryConversion'
                  ],
            "no":[
                    'parsing.pipeline.getFile',
                    'parsing.pipeline.dictionaryConversion'
                  ],
            "bg":[
                    'parsing.pipeline.getFile',
                    'parsing.pipeline.dictionaryConversion'
                  ],
            "fi":[
                    'parsing.pipeline.getFile',
                    'parsing.pipeline.dictionaryConversion'
                  ],
            "it":[
                    'parsing.pipeline.getFile',
                    'parsing.pipeline.itSplitInput'
                  ],
            "cy":[
                    'parsing.pipeline.getFile',
                    'parsing.pipeline.dictionaryConversion'
                  ],
            "ie":[
                    'parsing.pipeline.getFile',
                    'parsing.pipeline.dictionaryConversion'
                  ],
            "lt":[
                    'parsing.pipeline.getFile',
                    'parsing.pipeline.dictionaryConversion'
                  ],
            "rs":[
                    'parsing.pipeline.getFile',
                    'parsing.pipeline.dictionaryConversion'
                  ],
            "ee":[
                    'parsing.pipeline.getFile',
                    'parsing.pipeline.dictionaryConversion'
                  ],
            "ch":[
                    'parsing.pipeline.getFile',
                    'parsing.pipeline.languageDetect',
                    'parsing.pipeline.dictionaryConversion'
                  ],
            "hr":[
                    'parsing.pipeline.getFile',
                    'parsing.pipeline.dictionaryConversion'
                  ],
            "hu":[
                    'parsing.pipeline.getFile',
                  ]
            }

POSTPROCESS_PIPELINES = {
             "ted":[
                    'parsing.pipeline.removeFolder'
                    ],
            "oldTed":[
                    'parsing.pipeline.removeFolder'
                    ],
            "pl":[
                    'parsing.pipeline.removeFolder'
                    ],
            "be":[
                    'parsing.pipeline.removeFolder'
                    ],
            "uk":[
                  'parsing.pipeline.removeFolder'
                  ],
            "es":[
                  'parsing.pipeline.removeFolder'
                  ],
            "ro":[
                  'parsing.pipeline.removeFolder'
                  ],
            "cz":[
                  'parsing.pipeline.removeFolder'
                  ],
            "nl":[
                  'parsing.pipeline.removeFolder'
                  ],
            "dk":[
                  'parsing.pipeline.removeFolder'
                  ],
            "sk":[
                  'parsing.pipeline.removeFolder'
                  ],
            "fr":[
                  'parsing.pipeline.removeFolder'
                  ],
            "pt":[
                  'parsing.pipeline.removeFolder'
                  ],
            "no":[
                  'parsing.pipeline.removeFolder'
                  ],
            "bg":[
                  'parsing.pipeline.removeFolder'
                  ],
            "pt":[
                  'parsing.pipeline.removeFolder'
                  ],
            "fi":[
                  'parsing.pipeline.removeFolder'
                  ],
            "it":[
                  'parsing.pipeline.removeFolder'
                  ],
            "cy":[
                  'parsing.pipeline.removeFolder'
                  ],
            "ie":[
                  'parsing.pipeline.removeFolder'
                  ],
            "lt":[
                  'parsing.pipeline.removeFolder'
                  ],
            "rs":[
                  'parsing.pipeline.removeFolder'
                  ],
            "ch":[
                  'parsing.pipeline.removeFolder'
                  ],
            "ee":[
                  'parsing.pipeline.removeFolder'
                  ],
            "ch":[
                  'parsing.pipeline.removeFolder'
                  ],
            "hr":[
                  'parsing.pipeline.removeFolder'
                  ],
            "hu":[
                  'parsing.pipeline.removeFolder'
                  ]
             }





