# Introduction
This version of the parsing solution has been slightly adapted for the public procurement use case. 

# Get the code
```bash
git clone https://gitlab.com/digiwhist/parsing.git
```

# Setup 
The current solution has been designed using python 3.4. Previous versions may not work properly. Before configuring the python environment be sure to install the following dependencies in your Ubuntu environment.
```bash
sudo apt-get install python-virtualenv python3.4-dev libxml2-dev libpq-dev libxslt-dev
```

Create the virtual environment and install the dependencies.
```bash
virtualenv env -p python3.4
source env/bin/activate
pip install -r dependencies
```

# Running
## Configuration of the database
In the given example we assume a Postgresql database will be the target database and it is already configured.

In the `settings.py` file the host, user and password to be used with the database can be modified.
```python
userDB="userDb"
passwordDB="dbPassword"
hostDB='localhost'
```
Modify the variables accordingly to permit the system access the database. Assuming that the indicated user can create databases, scripts `utils/createDBs.sh` and `utils/cleanDBs.sh` can create all the databases and clean them respectively. For each country a different database will be created.

## Configuration of the archive
A Mongo database containing all the crawled elements extracted after the crawling will be used to extract the input files. Configure the values to connect to this database accordingly by modifying the code below included into the `settings.py` file.
```python
#Mongo archive configuration
ARCHIVE_DATABASE={"archive":"archive",
                  "host":"localhost",
                  "port":27017
                  }
```

## Parsing 
The main method to parse the incoming files and create the corresponding database is the `launchParserArchive` command. This command permits to indicate the number of crawled items to be processed and the size of the processing batch.

The following parses the crawled the first 100 files retrieved from the archive containing the Spanish files using batches of 10 elements to process. 
```
python manage.py launchParserArchive es 100 10
```

This one processes all the available files.
```
python manage.py launchParserArchive es -1 10
```

The size of the processing batch should be selected depending on the crawled data and the number of available processors in the current processing machine.



