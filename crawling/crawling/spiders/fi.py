# -*- coding: utf-8 -*-


import scrapy
from scrapy import Request
from scrapy import Selector
from digiwhist import DWhistSpider



import logging
import time

# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

class FISpider(DWhistSpider):
    '''
    The Finnish spider starts at 
    http://www.hankintailmoitukset.fi/fi/
    '''
    
    name = "fi"
   
    allowed_domains = ["http://www.hankintailmoitukset.fi","www.hankintailmoitukset.fi"]
    #put some delay to make it work
    download_delay=1.2
    
    def __init__(self,urls):
        DWhistSpider.__init__(self,urls,self.name)
    
    def parse(self,response):
        '''
        Get all the links from this page
        '''
        currentUrl = response.url
        logger.info("Current url %s"%currentUrl)
        query="/html/body/div/div[2]/div[3]/div[2]/table/tbody/tr/td[4]/a"
        
        links = Selector(text=response.body).xpath(query).extract()
        if len(links)==0:
            logger.info("We did not find any link. Stop here.")
            return
        
        for i in links:
            urlToExplore = i.split('"')[1]
            if not self.isUrlArchived(urlToExplore):
                #print urlToExplore
                yield Request(urlToExplore,callback=self.simpleArchiveItem)
        
        #jump to the next page
        tokens = currentUrl.split('=')
        nextUrl = tokens[0]
        pageNumber = int(tokens[1])+1
        nextUrl = nextUrl+"="+str(pageNumber)
        #logger.info("Next page %s"%nextUrl)
        yield Request(nextUrl,callback=self.parse)
        
        
        
    

        
        