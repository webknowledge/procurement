

import scrapy
from scrapy import Request


from digiwhist import DWhistSpider
import time
from urlparse import urlparse

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException


import logging


from django.conf import settings



# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

class CHSpider(DWhistSpider):
      
    name = "ch"
      
    allowed_domains = ['www.simap.ch','https://www.simap.ch','simap.ch']
   
                       
    #put some delay to make it work
    custom_settings={'DOWNLOAD_DELAY':1.15}

    
    def __init__(self,urls):
        DWhistSpider.__init__(self,urls,self.name)
        
        self.driver = webdriver.PhantomJS(settings.WEBDRIVER_PATH,service_args=['--ssl-protocol=any',
                                                                                  '--ignore-ssl-errors=true',  
                                                                                  '--web-security=false'])
        
      
    
    def parse(self,response):
        
        '''
        Take the list of links available in this page.
        '''
        
        logger.info("Process page "+response.url)
        
        self.driver.get(response.url)
        compose_cookies={}
        for i in self.driver.get_cookies():
            compose_cookies[i['name']]=i["value"]

        totalButtonQuery = '/html/body/div/div/div[3]/div[2]/div[2]/form/div/fieldset[3]/div[2]/div/div[3]/div[1]/input'
        button = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,totalButtonQuery)))
        button.click()
        #search
        searchButtonQuery = '/html/body/div/div/div[3]/div[2]/div[2]/form/div/div[3]/input'
        button = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,searchButtonQuery)))
        button.click()
        
        #Repeat until we don't find the button
        stop=False
        pages=0
        urlChain = 'https://www.simap.ch/shabforms/servlet/web/DocumentSearch?NOTICE_NR=%s'        

        while not stop:
            logger.info("Process search entry: %d"%pages)

            rows = self.driver.find_elements_by_xpath("/html/body/div/div/div[3]/div[2]/div[2]/form/table/tbody/tr")
            #Take all the rows and exclude the header
            
            for r in rows[1:]:
                #get the notice number
                try:
                    col2 = r.find_element_by_class_name("tdcol2")
                    col4 = r.find_element_by_class_name("tdcol4")
                    noticeNumber = col2.text
                    urlToStore = urlChain%noticeNumber                
                                
                    urlToExplore = col4.find_element_by_xpath("./a")
                    urlToExplore = urlToExplore.get_attribute("href")
                    
                    if not self.isUrlArchived(urlToStore):
                        yield Request(urlToExplore,callback=self.processPage,dont_filter=True,cookies=compose_cookies)
                    else:
                        logger.info("%s already stored. Stop here!"%urlToStore)
                        stop=True
                        break
                except:
                    #This was a lot row, no data inside
                    pass   
            
            if stop:
                break    
                
            nextButtonQuery = "//a[contains(@href,'FORWARD')]"
                
            try:
                nextButton = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,nextButtonQuery)))
                self.driver.implicitly_wait(1)
                nextButton.click()
                pages = pages + 1
            except Exception as e:
                logger.info("We couldn't find the next page button. Simply stop here.")
                logger.info("Finally processed: %d pages"%pages)
                stop = True

        self.driver.close()
        
        
    def processPage(self,response):
        urlChain = 'https://www.simap.ch/shabforms/servlet/web/DocumentSearch?NOTICE_NR=%s'        
        noticeNoQuery='//div[re:test(@class,"result_head")]/text()'
        header = response.xpath(noticeNoQuery).extract()
        try:
            noticeNumber = header[2].split(".")[1]
            noticeNumber = noticeNumber.strip()
        
            urlToStore = urlChain%noticeNumber
        
            self.simpleArchiveItem(response, urlToStore)
        except:
            logger.warning("Couldn't extract the contract number from %s"%response.url)
