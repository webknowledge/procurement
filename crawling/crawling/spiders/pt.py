

import scrapy
from scrapy import Request


from digiwhist import DWhistSpider
import time
from urlparse import urlparse

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException


import logging


from django.conf import settings



# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

class PTSpider(DWhistSpider):
    '''
    The pt spider launches two parallel searches one for "contratos" and the other
    one for "anuncios". The search is sequential and stops when a contract has already
    been archived.
    '''
    
    name = "pt"
      
    allowed_domains = ['www.base.gov.pt']
   
                       
    #put some delay to make it work
    custom_settings={'DOWNLOAD_DELAY':1.15}

    
    def __init__(self,urls):
        DWhistSpider.__init__(self,urls,self.name)
    
    def parse(self,response):
        
        '''
        Take the list of links available in this page.
        '''
        
        #logger.info("Process page "+response.url)
        linksQuery = "/html/body/div[3]/div/div/ss3/div[2]/div[4]/table/tbody/tr/td[6]/span/a/@href"
        
        links = response.xpath(linksQuery).extract()
                
        for urlToExplore in links:
            if not self.isUrlArchived(urlToExplore):
                yield Request(urlToExplore,callback=self.simpleArchiveItem)
            else:
                logger.info("We found "+urlToExplore+" already crawled. Time to stop")
                #return
        
        #go to the next page
        queryNextPage="/html/body/div[3]/div/div/ss3/div[2]/div[6]/p/a[2]/@href"
        
        try:
            urlNextPage = response.xpath(queryNextPage).extract()[0]
            urlNextPage = 'http://www.base.gov.pt'+urlNextPage
            time.sleep(3)
            yield Request(urlNextPage,callback=self.parse)
        except Exception as e:
            logger.info("We didn't find the next URL entry. Stop here!")
            logger.info(response.url)
            return
        
        
        
        
            
    