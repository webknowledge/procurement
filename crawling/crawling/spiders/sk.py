# -*- coding: utf-8 -*-

import scrapy
from scrapy import Request
from crawling.spiders.digiwhist import DWhistSpider

import logging

# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

class SKSpider(DWhistSpider):
    '''
    The Slovakian web crawling starts at:
    http://tender.sme.sk/en/contracts-list?order=contract_date.year:desc&page=1
    this contains the contracts from the latest to the oldest one.
    '''
    
    name = "sk"
   
    allowed_domains = ["tender.sme.sk","uvo.gov.sk"]
    #put some delay to make it work
    download_delay=1
    
    def __init__(self,urls):
        DWhistSpider.__init__(self,urls,self.name)

    
    def parse(self,response):
        '''
        The page contains a list of contracts. We launch a request for each of the
        contracts we find and retrieve the xml files.
        We repeat until we don't find a next page button.
        :param response:
        :return:
        '''
        xmlURLTemplate='http://www2.uvo.gov.sk/sk/evestnik/-/vestnik/save/%s.xml'
        # get all the contract ids
        links = response.xpath('/html/body/div[7]/div[2]/table/tbody/tr/td[6]/a/@href').extract()
        alreadyCrawled = 0
        for l in links:
            #format of the links
            #/en/contract/323610_3_1?order=contract_date.year:desc&page=1
            aux=l.split('?')[0]
            aux=aux.split('/')[-1]
            aux=aux.split('_')[0]
            #the final destinatio url containing the XML files is:
            #http://www2.uvo.gov.sk/sk/evestnik/-/vestnik/save/323610.xml
            urlToExplore=xmlURLTemplate%aux
            if not self.isUrlArchived(urlToExplore):
                yield Request(urlToExplore,self.simpleArchiveItem)
            # else:
            #     alreadyCrawled+=1
            # if alreadyCrawled==10:
            #     logger.info("We already crawled %d of these items. We stop here."%alreadyCrawled)
            #     return

        #look for the next page
        nextLink=response.xpath('/html/body/div[7]/div[2]/table/tfoot/tr/td[2]/div/ul/li[3]/span/a/@href').extract()

        if len(nextLink)!=0:
            logger.info("Go to %s"%nextLink[0])
            yield Request(nextLink[0],self.parse)
        else:
            logger.info("No more next pages found. We stop here.")



