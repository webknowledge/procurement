# -*- coding: utf-8 -*-


import scrapy
from scrapy import Request
from crawling.spiders.digiwhist import DWhistSpider


from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


from django.conf import settings


import logging
import time

# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

class HUSpider(DWhistSpider):
    '''
    The Hungarian web crawling starts at:
    http://www.kozbeszerzes.hu/adatbazis/keres/hirdetmeny/
    '''
    
    name = "hu"
   
    allowed_domains = ["kozbeszerzes.hu"]
    #put some delay to make it work
    download_delay=1
    
    def __init__(self,urls):
        DWhistSpider.__init__(self,urls,self.name)
        self.driver = webdriver.PhantomJS(settings.WEBDRIVER_PATH,service_args=['--ssl-protocol=any','--ignore-ssl-errors=true',  '--web-security=false'])
        #self.driver = webdriver.Firefox()
    
    def parse(self,response):
        '''
        Using selenium click search button and start yielding requests
        '''
        logger.info("Get initial page "+response.url.decode("utf-8"))
        self.driver.get(response.url)

        logger.info("Click the search button")
        searchButtonQuery = '/html/body/div[6]/form/div[2]/div[8]/button[1]'
        button = self.driver.find_element_by_xpath(searchButtonQuery)
        button.click()


        #get the links from the current page
        linksQuery = "/html/body/div[6]/div[2]/div/div[1]/table/tbody/tr/td[1]/a"
        pageQuery = "/html/body/div[6]/div[2]/div/div[1]/table/tbody/tr[1]"

        currentPage = 1
        explored=0

        while True:
            WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,pageQuery)))
            #links = self.driver.find_elements_by_xpath(linksQuery)
            links = WebDriverWait(self.driver,10).until(EC.presence_of_all_elements_located((By.XPATH,linksQuery)))

            logger.info("Process page: %d"%currentPage)
            for l in links:
                urlToExplore = l.get_attribute("href")
                if not self.isUrlArchived(urlToExplore):
                    explored+=1
                    yield Request(urlToExplore,self.simpleArchiveItem)
                #else:
                    #logger.info("[%s] already crawled. Stop here."%urlToExplore)
                    #return
            logger.info("Explored %d elements"%explored)
            #jump to the next page
            try:
                inactiveNextButton = self.driver.find_element_by_class_name('next')
            except Exception:
                logger.info("No next button found. We stop here")
                return
            try:
                currentPage += 1
                self.driver.execute_script('setPage(%d);return false;'%currentPage)
                time.sleep(3.20)
            except Exception as e:
                logger.info('Impossible to get next page')
                print e
                return

        self.driver.close()