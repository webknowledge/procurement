

import scrapy
from scrapy import Request,FormRequest


from digiwhist import DWhistSpider

from urlparse import urlparse

import time


import logging




# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

class RSSpider(DWhistSpider):
      
    name = "rs"
      
    allowed_domains = ['portal.ujn.gov.rs']
   
                       
    #put some delay to make it work
    custom_settings={'DOWNLOAD_DELAY':1.15,
                     'USER_AGENT':"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1"
                     }

    contractUrl="http://portal.ujn.gov.rs/Dokumenti/JavnaNabavka.aspx?idd=%d"
    
    def __init__(self,urls):
        DWhistSpider.__init__(self,urls,self.name)
        
        
    
    def parse(self,response):
        
        '''
        Take the list with latest 100 elements. Take the code for the 
        first element. And go decreasing the id until we find something
        we have already stored, or until we reach id 0.
        '''
                           
        queryFirstEntry = '/html/body/form/div[2]/table[2]/tbody/tr/td/div[2]/div[3]/div[2]/div/table/tbody/tr[2]/td[5]/input/@name'
        queryFirstEntry = '//input[re:test(@class,"buttonRowDetails")]/@name'
           
           
        paramName = response.xpath(queryFirstEntry).extract()[0]
               
        return FormRequest.from_response(
                                         response,
                                         formdata={paramName:''},
                                         callback=self.generateIds
                                         )
    
    def generateIds(self,response):
        '''
        Take the page identifier from this page from a url like
        http://portal.ujn.gov.rs/Dokumenti/JavnaNabavka.aspx?idd=928819
        '''
        
        currentId = int(response.url.split("=")[1])
        
        logger.info("We take %d as initial contract id "%currentId)
        
        counter=0
        for numberContract in range(currentId,-1,-1):
            urlToExplore = self.contractUrl%numberContract
                        
            if self.isUrlArchived(urlToExplore):
                logger.info("%s was already archived. Stop here!"%urlToExplore)
                return
            counter+=1
            if counter==500:
                time.sleep(2)
                counter=0
            
            yield Request(self.contractUrl%numberContract,callback=self.storePage)
    
    def storePage(self,response):
        '''
        Store the pages using the referee
        '''
        if response.url!='http://portal.ujn.gov.rs/DokumentNePostoji.aspx':
            #the url exists
            logger.info("Store: ---[%d]---> %s"%(response.status,response.url))
            self.simpleArchiveItem(response, response.url)
        
            
        
        
        
                    
        
