
import scrapy
from scrapy import Request
from digiwhist import DWhistSpider
import time


import logging

# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

class NLSpider(DWhistSpider):
    '''
    Tha main page contains the urls for the main static page containing relevant information.
    Inside these pages we can access the publication link that contains the publication itself.
    This can be generated statically without simulation.
    '''
    
    name = "nl"
   
    allowed_domains = ['www.tenderned.nl']
                       
    #put some delay to make it work
    download_delay=1.85
    
    def __init__(self,urls):
        DWhistSpider.__init__(self,urls,self.name)
    
    def parse(self,response):
                
        logger.info("Current page %s"%response.url)
        query='/html/body/div[1]/div[3]/div[1]/form/span/ol[1]/li/h3/a/@href'
        links = response.xpath(query).extract()
        logger.info("Page with %d links"%len(links))
        for i in links:
            urlPage = 'http://www.tenderned.nl'+i
            yield Request(urlPage,callback=self.explorePage)
            
        nextButtonQuery='//a[re:test(@title,"Volgende pagina")]//@href'
        nextButton = response.xpath(nextButtonQuery).extract()
        if len(nextButton)==0:
            logger.info("There was no next button. Exit.")
            return
        
        nextUrl = 'http://www.tenderned.nl'+nextButton[0]
        time.sleep(1.5)
        yield Request(nextUrl,callback=self.parse)

    def explorePage(self,response):
        '''
        For a given publication page, extract the publication link and archive it
        '''
        query = '/html/body/div/div[3]/div/ul/li[2]/a/@href'
        urlToArchive = response.xpath(query).extract()[0]
        urlToArchive = 'http://www.tenderned.nl'+urlToArchive
        
        if not self.isUrlArchived(urlToArchive):
            yield Request(urlToArchive,callback=self.simpleArchiveItem)
        
        
        