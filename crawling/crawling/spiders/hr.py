# -*- coding: utf-8 -*-


from digiwhist import DWhistSpider

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import datetime as dt
from dateutil.relativedelta import relativedelta

from django.conf import settings

import logging
from scrapy import Request
import time
import os

# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")


class HRSpider(DWhistSpider):
    """
    The Croatian spider starts at

    """

    name = "hr"

    allowed_domains = ["eojn.nn.hr"]
    # put some delay to make it work
    download_delay = 1.2

    temp_folder = settings.INTERMEDIATE_FOLDER + "/hr/"

    def __init__(self, urls):
        logger.info("Started the HR crawler ...")
        DWhistSpider.__init__(self, urls, self.name)
        self.display = Display(visible=0, size=(800, 600))
        self.display.start()
        profile = webdriver.FirefoxProfile()
        profile.set_preference("browser.cache.disk.parent_directory", settings.INTERMEDIATE_FOLDER + "/cache")
        self.driver = webdriver.Firefox(profile)

        if not os.path.isdir(self.temp_folder):
            logger.debug("Non existing. Create folder. " + self.temp_folder)
            os.makedirs(self.temp_folder)

    def parse(self, response):
        """
        Using selenium click search button and start yielding requests
        :param response: -
        """

        logger.info("Initialize virtual display")

        logger.info("Get initial page " + response.url.decode("utf-8"))
        self.driver.get(response.url)

        logger.info("Click the search menu")
        possibleMenus=[
            "TrazilicaCtl1_uiJednostavnaTrazilicaKoncesije",#ca
            "TrazilicaCtl1_uiJednostavnaTrazilicaBagatelne"#'purchase
            "TrazilicaCtl1_uiJednostavnaTrazilica",#cft
        ]
        for element in possibleMenus:
            menuLink = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.ID, element)))
            menuLink.click()

            self.driver.switch_to_frame(self.driver.find_element_by_id("iFrameAutoHeight"))

            currentDate = dt.date.today()
            finalDate = dt.date(2009, 1, 1)

            consecutiveEmptyDays = 0
            while currentDate > finalDate:
                links = self.processLinks(currentDate)
                if len(links) == 0:
                    logger.info("No more links to get on %s" % currentDate.strftime('%d/%m/%Y'))
                    consecutiveEmptyDays += 1
                    if consecutiveEmptyDays == 20:
                        logger.info("%d consecutive days without results. We stop here." % consecutiveEmptyDays)
                        break
                else:
                    consecutiveEmptyDays = 0
                    logger.info("Processing links: " + str(links))

                    for link in links:
                        logger.info("Yielding link: " + link)
                        yield Request(link, callback=self.checkAndArchive)
                currentDate = currentDate - relativedelta(days=1)

        self.driver.close()
        self.display.stop()

    def processLinks(self, currentDate):
        logger.info("Inserted date %s into form" % currentDate.strftime('%d/%m/%Y'))

        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.ID, "uiFilter_Calendar_DatumObjave_textBox")))
        # dateInput.send_keys(currentDate.strftime('%d.%m.%Y'))
        self.driver.execute_script(
            "document.getElementById('uiFilter_Calendar_DatumObjave_textBox').setAttribute('value','%s')" % currentDate.strftime(
                '%d.%m.%Y'))
        self.driver.execute_script("CalendarPopup_FindCalendar('uiFilter_Calendar_DatumObjave').TextChanged();")
        logger.info("Click search button")
        self.driver.execute_script("document.getElementById('uiFilterSearch').click();")
        time.sleep(1.5)

        recordsQuery = '/html/body/form/div[3]/div/table/tbody/tr[2]/td/table/tbody/tr[4]/td/span[1]/table/tbody/tr[2]/td/span/span'
        recordsElement = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, recordsQuery)))
        numRecords = int(recordsElement.text[:-1])
        logger.info("Number of records to find: %d" % numRecords)
        # we have 10 records per page so we have to compute:
        numPages = numRecords / 10
        decimalPart = (numRecords / 10.0) - numPages
        if decimalPart != 0:
            numPages += 1
        logger.info("We have to explore %d pages" % numPages)

        linksQuery = '/html/body/form/div[3]/div/table/tbody/tr[2]/td/table/tbody/tr[4]/td/span[1]/table/tbody/tr[1]/td/table/tbody/tr/td[1]/a'

        nextPageQuery = "javascript:__doPostBack('uiView$gridResults$ctl14$ctl%s','')"

        toReturn = []

        for _id in range(0, numPages):
            links = WebDriverWait(self.driver, 10).until(EC.presence_of_all_elements_located((By.XPATH, linksQuery)))

            for i in range(1, len(links)):
                urlToExplore = links[i].get_attribute("href")
                if "javascript" in urlToExplore:
                    # this is a javascript call
                    break
                if not self.isUrlArchived(urlToExplore):
                    toReturn.append(urlToExplore)

            nextPageId = _id + 1
            if nextPageId < 10:
                queryCall = nextPageQuery % ("0" + str(nextPageId))
            else:
                queryCall = nextPageQuery % str(nextPageId)
            logger.debug("Jump to %s" % queryCall)
            self.driver.execute_script(queryCall)

            time.sleep(2)

        return toReturn
