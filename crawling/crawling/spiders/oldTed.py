
# -*- coding: utf-8 -*-
from scrapy import Request
import scrapy

from digiwhist import DWhistSpider


class OldTedSpider(DWhistSpider):
    '''
    OldTed data is accessible through FTP with authentication. 

    '''
    
    name = "oldTed"
    allowed_domains = ["ted.europa.eu","127.0.0.1"]
    ftp_user='guest'
    ftp_password="guest"
    
    handle_httpstatus_list = [404]
    #put some delay to make it work
    custom_settings={'DOWNLOAD_DELAY':10,
                     'USER_AGENT':"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1",
                     'CONCURRENT_REQUESTS':1
                     }

    
    
    def __init__(self,urls):
        DWhistSpider.__init__(self,urls,self.name)
    
    def start_requests(self):
        
        for i in self.getOldTedURLS():
            if not self.isUrlArchived(i):
                yield Request(
                          url=i,
                          meta={"ftp_user":self.ftp_user,"ftp_password":self.ftp_password},
                          callback=self.simpleArchiveItem
                          )
        

            
            
    def getOldTedURLS(self):
        toReturn = []

            
        
        #example
        #'ftp://ted.europa.eu/daily-packages/2010/2010-03/042/META-XML/EN_20100302_2010042_META_ORG.zip'
        #                                              /year/year month/sequential number/.../year,month,day,year,sequentialnumber
        theString = 'ftp://ted.europa.eu/daily-packages/2010/2010-%s/%s/META-XML/EN_2010%s%s_2010%s_META_ORG.zip'
        for month in range(3,13):
            for day in range(1,31):
                sequentialId=month+day+40
                monthStr = "%02d"%month
                dayStr = "%02d"%day
                sequentialStr = "%03d"%sequentialId
                
                toReturn.append(theString%(monthStr,sequentialStr,monthStr,dayStr,sequentialStr)) 
                
        return toReturn