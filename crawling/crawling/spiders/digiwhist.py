

import scrapy
from crawling.models import ArchiveItem
import datetime
from time import gmtime, strftime

from mongoengine import DoesNotExist
from crawling.items import CrawlingItem
from crawling.models import ArchiveItem


import logging
# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")


class DWhistSpider(scrapy.Spider):
    '''
    This is the main digiwhist spider to inherit by the different
    spiders to be used in the crawling solution. 
    '''
    
    name=None

    def __init__(self,urls,name):
        '''
        We have an archiver object 
        '''
        self.name=name
        #check URL by url if we really have to download them
        self.start_urls = []
        for candidate in urls:
            self.start_urls.append(candidate)

        logger.info("Crawler launched with initially "+str(len(self.start_urls))+" urls")
        
    def getArchiveItem(self,url):
        '''
        Check whether an item is contained into the archival DB
        '''
        result = None
        try:
            result = ArchiveItem.objects.get(url=url)
        except DoesNotExist:
            pass
        return result
    
    
    
    def archiveItem(self,response):
        #we have to store this item
        item = ArchiveItem()
        item.source = self.name
        item.date=strftime("%Y-%m-%d %H:%M:%S", gmtime())
        item.url = response.url
        item.file.put(response.body)
        item.contentType=response.headers['Content-Type'][0]
        item.save()
        logger.info("Save into archival item from: "+response.url)
        return item
    
    
    def simpleArchiveItem(self,response,url=None,hasBody=True):
        #we have to store this item
        item = ArchiveItem()
        item.source = self.name
        item.date=strftime("%Y-%m-%d %H:%M:%S", gmtime())
        if url==None:
            item.url = response.url
        else:
            item.url=url
        if hasBody:
            item.file.put(response.body)
            if 'Content-Type' in response.headers: 
                if type(response.headers['Content-Type'])==list:
                    item.contentType=response.headers['Content-Type'][0]    
                else:
                    item.contentType=response.headers['Content-Type']

        else:
            item.file.put(response)

        item.save()

    def checkAndArchive(self,response,url=None,hasBody=True):
        if not self.isUrlArchived(url):
            self.simpleArchiveItem(response,url,hasBody)

    def archiveItemFromFile(self,filePath,url,contentType):
        '''
        Archive one item previously stored in a local file
        :param filePath:
        :param url
        :param contentType:
        :return:
        '''
        item = ArchiveItem()
        item.source = self.name
        item.date=strftime("%Y-%m-%d %H:%M:%S", gmtime())
        item.url = url
        with open(filePath,'r') as reader:
            content = reader.read()
        item.file.put(content)
        item.contentType=contentType
        item.save()



    def isUrlArchived(self,url):
        numOccur = ArchiveItem.objects.filter(url=url).count() 
        return numOccur!=0
    
