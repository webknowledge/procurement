# -*- coding: utf-8 -*-



from scrapy import Request
from digiwhist import DWhistSpider

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


from django.conf import settings

import logging

import time

# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

class DKSpider(DWhistSpider):
    '''
    The Dannish spider starts at 
    https://www.udbud.dk/
    '''
    
    name = "dk"
   
    allowed_domains = ["https://www.udbud.dk","www.udbud.dk"]
    #put some delay to make it work
    download_delay=1.2
    
    def __init__(self,urls):
        DWhistSpider.__init__(self,urls,self.name)
        self.driver = webdriver.PhantomJS(settings.WEBDRIVER_PATH,service_args=['--ssl-protocol=any','--ignore-ssl-errors=true',  '--web-security=false'])
        #self.driver = webdriver.Firefox()
    
    def parse(self,response):
        '''
        Using selenium click search button and start yielding requests
        '''
        logger.info("Get initial page "+response.url.decode("utf-8"))
        self.driver.get(response.url)
        #click the search menu without any restriction
        logger.info("Click search button...")
        query='/html/body/form/div[3]/div[2]/div[2]/div[1]/div[3]/div/div[4]/input'

        button = WebDriverWait(self.driver,15).until(EC.presence_of_element_located((By.XPATH,query)))
        button.click()
        
        
        #Now we are in the search results page. Collect the results.              
               
        stop=False
        currentPage=1
        foundDocuments=0
        while not stop:
            logger.info("Process all the links from the page: %d"%currentPage)
            query="/html/body/form/div[3]/div[2]/div[3]/div/div[1]/div/table/tbody/tr/td[2]/a"

            
            listLinks = self.driver.find_elements_by_xpath(query)
            for i in listLinks:                
                urlToExplore = i.get_attribute('href')
                if not self.isUrlArchived(urlToExplore):
                    foundDocuments=foundDocuments+1
                    yield Request(urlToExplore,callback=self.simpleArchiveItem)
                            
            #Find the next page button
            
            nextButtonQuery="/html/body/form/div[3]/div[2]/div[3]/div/div[1]/div/div[3]/div/span[4]"
            try:
                button = self.driver.find_element_by_xpath(nextButtonQuery)
                time.sleep(2)
                button.click()
                logger.info("Finally we found %d documents in the page"%foundDocuments)
                foundDocuments=0
                currentPage=currentPage+1
            except Exception as e:
                stop = True
                logger.info("Force the exit")
            
        self.driver.close()


        
        