
import scrapy
from scrapy import Request


from digiwhist import DWhistSpider
import time
from urlparse import urlparse

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException


import logging


from django.conf import settings



# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

class FRSpider(DWhistSpider):
    '''
    The announcements are linked one after the other. First,
    we access the first announcement and navigate following the 
    "suivant" link.
    '''
    
    name = "fr"
      
    allowed_domains = ['http://www.boamp.fr','www.boamp.fr']
   
                       
    #put some delay to make it work
    custom_settings={'DOWNLOAD_DELAY':2.15}

    
    def __init__(self,urls):
        DWhistSpider.__init__(self,urls,self.name)
        self.driver = webdriver.PhantomJS(settings.WEBDRIVER_PATH,service_args=['--ssl-protocol=any',
                                                                                  '--ignore-ssl-errors=true',
                                                                                  '--web-security=false'])

        self.clickingXpaths=[
            #ongoing contracts button
            '/html/body/main/div/div[1]/form/div/div/div[10]/fieldset/div[1]/label/input',
            #archive button
            '/html/body/main/div/div[1]/form/div/div/div[10]/fieldset/div[2]/label/input'
            ]
        
        self.cursor=0
        
        
        
        #self.driver = webdriver.Firefox()
    
    def parse(self,response):
        '''
        In the avancee rechercher screen we launch a crawling
        using the "les avis en cours" and another one using
        "les avis archives"
        '''
        self.driver.get(response.url)
        #logger.info("Current page %s"%response.url)

        #select avis de marche and resultats de marche items
        #avis de marche
        clickable = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.ID,"type01")))
        clickable.click()
        #resultat de marche
        clickable = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.ID,"type06")))
        clickable.click()

        import time
        time.sleep(10)


        button = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,self.clickingXpaths[self.cursor])))
        button.click()
        #click the search button
        searchButtonQuery = "/html/body/main/div/div[1]/form/div/div/div[11]/div[1]/button"
        searchButton = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,searchButtonQuery)))
        searchButton.click()
        
        #get the first entry from the list
                            
        firstEntryQuery = '/html/body/main/div/div[1]/div[1]/form/ul/li[1]/article/header/p[4]/a'
        urlToAnnouncement = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,firstEntryQuery)))
        urlToAnnouncement = urlToAnnouncement.get_attribute('href')
                    
        compose_cookies={}
        for i in self.driver.get_cookies():
            compose_cookies[i['name']]=i["value"]
     
        
        yield Request(urlToAnnouncement,callback=self.parsePage,cookies=compose_cookies,
                      meta={'cookies':compose_cookies})
                
            
    def parsePage(self,response):
        logger.info("Process "+response.url)
        
        queryNextButton = '//html/body/main/div/div[1]/article/div[1]/ul[last()]/li/a/@href'
        
        #we have to remove the latest /number from the url
        archiveUrl = response.url[0:response.url.rfind('/')]
        
        if not self.isUrlArchived(archiveUrl):
            self.simpleArchiveItem(response,url=archiveUrl)
        else:
            logger.info("We have already crawled this item. We stop here.")
            #return

        try:
            next=response.xpath(queryNextButton).extract()
            urlToVisit = "http://www.boamp.fr"+next[-1]
            cookies=response.meta['cookies']
            yield Request(urlToVisit,callback=self.parsePage,
                          cookies=cookies,meta={'cookies':cookies}
                          )
                              
        except Exception as e:
            print e
            logger.info("Next entry not found. Stop")
            if self.cursor==0:
                self.cursor=1
                logger.info("Move the cursor to the archive list")
                yield Request("http://www.boamp.fr/recherche/avancee",callback=self.parse)
            else:
                logger.info("We stop here")
            return
        
            
        
        
        