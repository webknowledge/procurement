# -*- coding: utf-8 -*-

import scrapy
from scrapy import Request
from crawling.spiders.digiwhist import DWhistSpider


from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from django.conf import settings

import logging

import time

# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

class LTSpider(DWhistSpider):
    '''
    The Lituanian web crawling starts at:
    http://cvpp.lt/
    '''
    
    name = "lt"
   
    allowed_domains = ["cvpp.lt","pirkimai.eviesiejipirkimai.lt"]
    #put some delay to make it work
    download_delay=1.2
    
    def __init__(self,urls):
        DWhistSpider.__init__(self,urls,self.name)
        self.driver = webdriver.PhantomJS(settings.WEBDRIVER_PATH,service_args=['--ssl-protocol=any','--ignore-ssl-errors=true',  '--web-security=false'])
        #self.driver = webdriver.Firefox()
    
    def parse(self,response):
        '''
        Using selenium click search button and start yielding requests
        '''
        logger.info("Get initial page "+response.url.decode("utf-8"))
        self.driver.get(response.url)

        #we have to select every category from the drop down menu. There are 9 categories we skip the first one
        menuQuery='/html/body/div[2]/div[4]/table/tbody/tr/td/div/div/div[1]/form/div[3]/select/option[%d]'

        for i in range(2,10):
            logger.info("Go with type entry [%d]"%i)
            button = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,menuQuery%i)))
            button.click()

            entriesQuery='/html/body/div[2]/div[4]/table/tbody/tr/td/div/div/div/table/tbody/tr/td/div[2]/h3/a'
            pageCounter=2
            while True:
                #elements = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,entriesQuery)))
                elements = self.driver.find_elements_by_xpath(entriesQuery)

                for link in elements:
                    urlToExplore = link.get_attribute("href")
                    if self.isUrlArchived(urlToExplore):
                        logger.info("The url [%s] was already archived.")
                    else:
                        yield Request(urlToExplore,self.simpleArchiveItem)

                try:
                    time.sleep(2)
                    #nextButton = self.driver.find_element_by_partial_link_text(str(pageCounter))
                    nextButton = self.driver.find_element_by_link_text(str(pageCounter))
                    nextButton.click()
                except Exception as e:
                    logger.info("No more elements to crawl in category %d"%i)
                    print e
                    break

                pageCounter+=1

        self.driver.close()




