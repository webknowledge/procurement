
from digiwhist import DWhistSpider
import logging
import json

# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

#list of national agencies extracted from here
#We process the list of URLs extracted from


link_files=["crawling/links/it_links_2015.json","crawling/links/it_links_2016.json"]

class ITSpider(DWhistSpider):
    name = "it"
   
    allowed_domains = ['*']
                       
    #put some delay to make it work
    download_delay=0.3
    
    def __init__(self,urls):

        urls = []
        for link_file in link_files:
            with open(link_file) as reader:
                entries = json.load(reader)
            for e in entries:
                if e['esitoUltimoTentativoAccessoUrl']!='fallito':
                    aux = e['url']
                    if aux.startswith(' '):
                        aux=aux.replace(' ','')
                    if not aux.startswith('http://') and not aux.startswith('https://'):
                        aux='http://'+aux
                    urls.append(aux)


        DWhistSpider.__init__(self,urls,self.name)


    
    def parse(self,response):
        self.simpleArchiveItem(response)