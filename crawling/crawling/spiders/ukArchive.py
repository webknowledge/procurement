
import scrapy
from scrapy import Request
from digiwhist import DWhistSpider

from django.conf import settings

from lxml import etree

import logging

# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

class UKArchiveSpider(DWhistSpider):
    '''
    The procurement web page for UK was updated on February 2015.
    Up to that moment all the information is available through the 
    archive. URLS are static following the the pattern below: 
    https://data.gov.uk/data/contracts-finder-archive/static/files/notices_2015_01.xml
    '''
    
    name = "uk"
   
    allowed_domains = ["https://data.gov.uk/data/contracts-finder-archive"]
    #put some delay to make it work
    download_delay=0.500
    
    def __init__(self,urls):
        DWhistSpider.__init__(self,urls,self.name)
       
    
    def parse(self,response):
        '''
        The list of urls
        '''
        self.checkAndArchive(response)