# -*- coding: utf-8 -*-


from scrapy import Request
from crawling.spiders.digiwhist import DWhistSpider

import logging

# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

class SESpider(DWhistSpider):
    '''
    The Sweden web page does not provide continuous data. We can only crawl a
    small fragment of the data per scan
    The Swedish web crawling starts at:
    https://www.avropa.se/Upphandlingar/
    '''
    
    name = "se"
   
    allowed_domains = ["avropa.se"]
    #put some delay to make it work
    download_delay=1.2
    
    def __init__(self,urls):
        DWhistSpider.__init__(self,urls,self.name)
    
    def parse(self,response):

        logger.info("Get the available links")
        linksQuery='/html/body/div/div[2]/div[2]/div[1]/table/tbody/tr/td[1]/a/@href'
        links = response.xpath(linksQuery).extract()
        for i in links:
            urlToExplore = 'https://www.avropa.se'+i
            if not self.isUrlArchived(urlToExplore):
                yield Request(urlToExplore,self.simpleArchiveItem)
            else:
                logger.info("We have already crawled %s. We stop here."%urlToExplore)
                return

