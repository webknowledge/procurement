# -*- coding: utf-8 -*-


import scrapy
from scrapy import Request
from crawling.spiders.digiwhist import DWhistSpider


from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from urlparse import urlparse

from django.conf import settings


import logging

import time

# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

class IESpider(DWhistSpider):
    '''
    The Irish web crawling starts at:
    https://irl.eu-supply.com/ctm/supplier/publictenders
    '''
    
    name = "ie"
   
    allowed_domains = ["eu-supply.com"]
    #put some delay to make it work
    download_delay=1.2
    
    def __init__(self,urls):
        DWhistSpider.__init__(self,urls,self.name)
        self.driver = webdriver.PhantomJS(settings.WEBDRIVER_PATH,service_args=['--ssl-protocol=any','--ignore-ssl-errors=true',  '--web-security=false'])
        #self.driver = webdriver.Firefox()
    
    def parse(self,response):
        '''
        Using selenium click search button and start yielding requests
        '''
        logger.info("Get initial page "+response.url.decode("utf-8"))
        self.driver.get(response.url)

        #click to select even expired tenders
        logger.info("Click show also expired tenders...")
 
        query = '/html/body/div[1]/div[3]/div/div/form/div[1]/div[3]/div[4]/div/label/input[1]'
        button = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,query)))
        button.click()
 
        logger.info('Click the search button')
        buttonQuery = '/html/body/div[1]/div[3]/div/div/form/div[1]/div[4]/ul/li[1]/a'
        button = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,buttonQuery)))
        button.click()
         
        requests = self.collectSearchResults()
        counter=0
        while len(requests)!=0:
            for r in requests:
                counter+=1
                yield r
            logger.info("Found %d links"%counter)
            requests = self.collectSearchResults()
  
  
        logger.info("Click show also expired tenders...")
        query = '/html/body/div[1]/div[3]/div/div/form/div[1]/div[3]/div[4]/div/label/input[1]'
        button = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,query)))
        button.click()    

        logger.info('Go for the contract awards')
        buttonQuery = '/html/body/div[1]/div[3]/div/div/form/div[1]/div[3]/div[1]/div[1]/select/option[2]'
        button = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,buttonQuery)))
        button.click()
        
        logger.info('Click the search button')
        buttonQuery = '/html/body/div[1]/div[3]/div/div/form/div[1]/div[4]/ul/li[1]/a'
        button = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,buttonQuery)))
        button.click()
        
        self.driver.implicitly_wait(3)
        requests = self.collectSearchResults()
        counter=0
        while len(requests)!=0:
            for r in requests:
                counter+=1
                yield r
            logger.info("Found %d links"%counter)
            requests = self.collectSearchResults()
            


    def collectSearchResults(self):
        #Now we are in the search results page. Collect the results.
        #URL format to replace using the PID=7663 string
        
        urlFormat='https://irl.eu-supply.com/app/rfq/publicpurchase.asp?%s&amp;HL=0&amp;PS=1&amp;PP='
        nextButtonQuery="/html/body/div[1]/div[3]/div/div/form/div[2]/div[2]/div/ul[1]/li[14]/a"

        requests = []
        
        
        query="/html/body/div[1]/div[3]/div/div/form/div[2]/div[2]/table/tbody/tr/td[3]/a"
        #extract the internal identifier from the URL

        WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,'/html/body/div[1]/div[3]/div/div/form/div[2]/div[2]/table')))
        #logger.info("We got %s entries in the page"%len(listLinks))
        time.sleep(1)
        listLinks = self.driver.find_elements_by_xpath(query)
        for i in listLinks:
            urlToExplore = i.get_attribute('href')
            urlElements = urlparse(urlToExplore)
            #get the PID
            pid = urlElements.query.split('&')[0]
            urlToExplore = urlFormat%pid

            if not self.isUrlArchived(urlToExplore):
                requests.append(Request(urlToExplore,callback=self.simpleArchiveItem))
            else:
                logger.info("We already crawled %s. We stop here"%urlToExplore)
                return requests

        #Find the next page button
                                    
        try:
            #self.driver.implicitly_wait(2.5)
            time.sleep(4)
            button = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,nextButtonQuery)))
            button.click()
        except Exception as e:
            print e
            logger.info("No next page button. Force the exit")
        return requests
