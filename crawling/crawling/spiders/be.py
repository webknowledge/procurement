# -*- coding: utf-8 -*-
import scrapy
from scrapy import Request
from digiwhist import DWhistSpider
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from django.conf import settings

from lxml import etree

import logging

# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

class BESpider(DWhistSpider):
    '''
    Spider to crawl the data source from Belgium.
    It uses the BE webform to navigate through existing
    pages. Only the entries in French are downloaded.
    '''
    
    name = "be"
    allowed_domains = ["enot.publicprocurement.be","127.0.0.1"]
    #put some delay to make it work
    download_delay=0.500
    
    def __init__(self,urls):
        DWhistSpider.__init__(self,urls,self.name)
        self.driver = webdriver.PhantomJS(settings.WEBDRIVER_PATH,service_args=['--ssl-protocol=any'])
        #self.driver = webdriver.Firefox()
                
    def parse(self,response):
        '''
        In the main page we basically check the buttons
        to retrieve all the information
        '''
        logger.info("Get initial page")
        self.driver.get(response.url)
        WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.TAG_NAME,'body')))
        logger.info("The current page is "+self.driver.current_url)
        #click button search publication in both
        button=self.driver.find_element_by_xpath('/html/body/div[2]/div[3]/div[2]/form/fieldset/div[1]/label[3]')
        
        button.click()
        #click button publication in status all
        button = self.driver.find_element_by_xpath('/html/body/div[2]/div[3]/div[2]/form/fieldset/div[2]/label[3]')
        button.click()
        #click button In all languages
        button = self.driver.find_element_by_xpath('/html/body/div[2]/div[3]/div[2]/form/fieldset/div[3]/label[2]')
        button.click()
        #click search
        button = self.driver.find_element_by_xpath('/html/body/div[2]/div[3]/div[2]/form/fieldset/p[13]/button')
        button.click()
        
        objects = self.parseSearchPage()
        self.driver.close()
        self.driver.quit()
        
        for o in objects:
            yield Request(o,callback=self.processObjectPage)
                        
        logger.info("Finished generation of requests")
    
        
    def parseSearchPage(self):
        '''
        Navigates through the search result pages collecting the resulting links.
        '''
        logger.info("Now we are in page: "+self.driver.current_url)
        #links = self.driver.find_elements_by_xpath('/html/body/div[2]/div[3]/div[2]/form/fieldset[2]/table/tbody/tr[*]/td[1]/a')
        
        #select a larger number of results per page
        self.driver.find_element_by_xpath("/html/body/div[2]/div[3]/div[2]/form/fieldset[2]/select/option[5]").click()
        
        #print links
        exploredPages = 0
        nextButton=None
        latestUrl=None
        hrefs = []
        while True:
            
            WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.TAG_NAME,'body')))
                
            links = self.driver.find_elements_by_xpath('/html/body/div[2]/div[3]/div[2]/form/fieldset[2]/table/tbody/tr[*]/td[1]/a')
            logger.info("Current page ["+str(exploredPages)+"] has "+str(len(links))+" links. Already processed: "+str(len(hrefs)))
            time.sleep(1)
            
                   
            #add to the list of hrefs
                       
            for linkObject in links:
                hrefs.append(linkObject.get_attribute('href'))

            exploredPages = exploredPages+1


            if len(links)<125:
                break

            if exploredPages==1:
                logger.info("Limit of pages to explore reached")
                break

            try:
                #nextButton = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,"/html/body/div[2]/div[3]/div[2]/form/fieldset[2]/span[2]/a[10]")))
                #nextButton = WebDriverWait(self.driver,10).until(EC.presence_of_elements_located((By.XPATH,'/html/body/div[2]/div[3]/div[2]/form/fieldset[2]/span[2]/a')))[-2]
                nextButton = self.driver.find_elements_by_xpath('/html/body/div[2]/div[3]/div[2]/form/fieldset[2]/span[2]/a')[-2]           
                nextButton.click()
            except:
                logger.info("No more next entries. Exit.")
                nextButton=None

            
            if nextButton==None:
                logger.info("Stop because there is no next button")
                break
            
            logger.info("Move to the next page of results")

        logger.info("Explored pages: "+str(exploredPages))
        return hrefs
      
                
        
    def processObjectPage(self,response):
        '''
        Processes the page containing results for a single object
        '''
                
        logger.debug("Process object page "+response.url)

        #click the xml button
        parser = etree.HTML(response.body)
        
        table = parser.xpath('//table[@id="noticeVer"]')[0] 
                
        for row in table.xpath('//tr')[1:]:
            content = row.xpath('//td[7]/script/text()')[0]
            
            tokens = content.split()
            
            entries=[]
            #get the url from the javascript code
            for t in tokens:
                if 'getNoticeXml.do' in t:
                    entries.append(t.split("'")[1])
            chosenEntry = None
            if len(entries)>1:
                #we have entries for nl and french, just select french
                for ent in entries:
                    if "languageCharset=fr" in ent:
                        #remove the latest / symbol
                        chosenEntry = ent[:-1]
                        break
            else:
                chosenEntry=entries[0][:-1]
            
            #form the url to retrieve the XML object        
            delimiter = response.url.index('preViewNotice.do')
            targetUrl = response.url[0:delimiter]+chosenEntry

            logger.debug("Target object is in: "+targetUrl)
            
            if not self.isUrlArchived(targetUrl):
                yield Request(targetUrl,callback=self.simpleArchiveItem)
            else:
                logger.info("Element [%s] already crawled. Stop here!"%targetUrl)
                raise scrapy.exceptions.CloseSpider("Element [%s] already crawled. Stop here!"%targetUrl)
            
           
        
