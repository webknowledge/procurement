

import scrapy
from scrapy import Request,FormRequest
from digiwhist import DWhistSpider
import time
from urlparse import urlparse

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException

from django.conf import settings

import logging
import datetime
import re
# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

class EESpider(DWhistSpider):
    name = "ee"
   
    allowed_domains = ['riigihanked.riik.ee']
                       
    #put some delay to make it work
    custom_settings={'DOWNLOAD_DELAY':1.15,
                     'USER_AGENT':"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1",
                     'CONCURRENT_REQUESTS':2
                     }
    
    def __init__(self,urls):
        DWhistSpider.__init__(self,urls,self.name)
        self.driver = webdriver.PhantomJS(settings.WEBDRIVER_PATH,service_args=['--ssl-protocol=any','--ignore-ssl-errors=true',  '--web-security=false'])
        #self.driver = webdriver.Firefox()
        self.alreadyCrawled = 0
    
    def parse(self,response):
        
        '''
        Initial web page imposes some filtering. Remove it all and start crawling.
        '''
        logger.info("Current page %s"%response.url)
        self.driver.get(response.url)
        #Click drop down menu
        #get the most recently published contract notice
        noticeId = self.getFirstNoticeId()
        if noticeId==None:
            logger.info("The first noticeId we found was None. We stop here")
            #raise scrapy.exceptions.CloseSpider("The first noticeId we found was None. We stop here")
            return
        
        baseUrl = "https://riigihanked.riik.ee/register/teade/%d"
        initialId=int(noticeId)
        for i in range(initialId,(initialId-10),-1):
            urlToExplore = baseUrl%i
            if not self.isUrlArchived(urlToExplore):
                yield Request(urlToExplore,callback=self.processPage)
            else:
                logger.info("Initial url %s was already archived"%urlToExplore)
                        
        self.driver.close()
        
    
    def processPage(self,response):
        
        try:
            currentNoticeId = int(response.url.split('/')[-1])
        except:
            #this is an error page ignore
            return
        
        self.simpleArchiveItem(response)
        
        
        baseUrl = "https://riigihanked.riik.ee/register/teade/%d"
        logger.debug("CurrentNoticeId: %d"%currentNoticeId)
        for i in range((currentNoticeId-1),(currentNoticeId-10),-1):
            urlToExplore = baseUrl%i
            if not self.isUrlArchived(urlToExplore):
                yield Request(urlToExplore,callback=self.processPage)
            else:
                self.alreadyCrawled+=1
        

    
    
    def getFirstNoticeId(self):
        #get all the links
        #we have to click and visit all the contract pages to get the Notice i
        contractLinksQuery = '/html/body/div[3]/div[2]/div/form/div[2]/span/table/tbody/tr/td[13]/input'                     
        #contractLinks = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,contractLinksQuery)))
        
        contractButtons =  self.driver.find_elements_by_xpath(contractLinksQuery)
        for i in range(1,len(contractButtons)):
            contractButtons =  self.driver.find_elements_by_xpath(contractLinksQuery)
            contractButtons[i].click()
            
            #click to visit the reference page
            WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,"/html/body")))
            
                
            #Now get the notice ids. The ids could not be available.
            failed=False
            try:
                #try in Estonian
                noticesButton = self.driver.find_element_by_link_text('Hanke teated')
            except:
                failed=True
                
            if failed:
                try:
                    #try in English
                    noticesButton = self.driver.find_element_by_link_text('Notices')
                    failed = False
                except:
                    #No notices here
                    pass
            
            if not failed:
                #click on the notices menu
                #
                try:
                    noticesButton.click()
                    noticesId = self.driver.find_elements_by_class_name('idColumnValue')
                    for id in noticesId:
                        if id.text != None:
                            return id.text
                except:     
                    pass
            
            #go back and try with the next one
            #press back button
#             #check if there is a nextButton
            self.driver.implicitly_wait(0.5)
            backButton = self.driver.find_element_by_id('PageLink')
            backButton.click()
            
        return None
