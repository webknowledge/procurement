

import scrapy
from scrapy import Request
from digiwhist import DWhistSpider

import json

from django.conf import settings

import logging


# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

class ROSpider(DWhistSpider):
    name = "ro"
   
    allowed_domains = ['data.gov.ro']
                       
    #put some delay to make it work
    custom_settings={'DOWNLOAD_DELAY':1.15,
                     'USER_AGENT':"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1",
                     'CONCURRENT_REQUESTS':1,
                     'DOWNLOAD_WARNSIZE':34359738368
                     }
    
    def __init__(self,urls):
        DWhistSpider.__init__(self,urls,self.name)
    
    def parse(self,response):
        #http://data.gov.ro/api/1/rest/dataset/
        datasets=["achizitii-publice-2007-2015-contracte",
                 "achizitii-publice-2010-2015-anunturi-de-participare",
                 "achizitii-publice-2010-2015-invitatii-de-participare"
                 ]
        
        for d in datasets:
            yield Request("http://data.gov.ro/api/1/rest/dataset/%s"%d,callback=self.processDataset)
        
    def processDataset(self,response):
        #create a json from the response
        response_json =  json.loads(response.body)
        resources = response_json["resources"]
        resourcesSize = response_json["num_resources"]

        if len(resources) == resourcesSize:
            for resource in resources:
                resourceUrl = resource["url"]
                if resourceUrl is not None:
                    if not self.isUrlArchived(resourceUrl):
                        self.logger.info("Downloading " + resourceUrl)
                        yield Request(resourceUrl,callback=self.simpleArchiveItem)
                        
        else:
            logging.error("Error: data set size does not match the number of resources")
        
        