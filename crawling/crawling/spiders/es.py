

import scrapy
from scrapy import Request
from digiwhist import DWhistSpider

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException

from django.conf import settings

from lxml import etree

import logging

# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

class ESSpider(DWhistSpider):
    '''
    The Spain crawler uses the static URLs that can be found at:
    https://contrataciondelestado.es/wps/portal/licRecientes
    '''
    
    name = "es"
   
    allowed_domains = ["https://contrataciondelestado.es/","https://www.contrataciondelestado.es/","contrataciondelestado.es",
                       "http://contrataciondelestado.es/"]
    #put some delay to make it work
    download_delay=0.500
    
    def __init__(self,urls):
        DWhistSpider.__init__(self,urls,self.name)
        self.driver = webdriver.PhantomJS(settings.WEBDRIVER_PATH,service_args=['--ssl-protocol=any','--ignore-ssl-errors=true',  '--web-security=false'])
        #self.driver = webdriver.Firefox()
    
    def parse(self,response):
        '''
        Using selenium click search button and start yielding requests
        '''
        logger.info("Get initial page "+response.url)
        self.driver.get(response.url)
        #click the search menu
        logger.info("Click licitaciones button...")
        button = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,'/html/body/div/div/div[1]/div[2]/div[2]/a')))
        button.click()
        
        #click the search button
        logger.info("Click search button...")
        button = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,'/html/body/div/div/div[1]/div[4]/div[2]/a')))
        button.click()
        
        #click busqueda guiada button
        
        logger.info("Click busqueda guiada button...")
        button = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,'/html/body/div/div/div[2]/div[2]/div/div/div/div/div/form/div/div[1]/a/p[1]/img')))
        button.click()
        
        
        #click (ver todos los resultados) -> all results
        logger.info("Click to see all the results...")
        
        button = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,'/html/body/div/div/div[2]/div[2]/div/div/div/div/div/form/div[2]/a')))
        button.click()
        
        
        #collect all the urls and yield the corresponding request
        #click in every result entry
        #this changes the main page
        #collect the links
        #go back clicking in the search button
        
        stop=False
        currentPage=1
        collectedDocuments=0
        while not stop:
            logger.info("Process all the links from the page")
            listLinks = self.driver.find_elements_by_xpath('/html/body/div/div/div[2]/div[2]/div/div/div/div/div/div/form/div[3]/table/tbody/tr[*]/td[1]/div[1]/a')
            #listLinks = self.driver.find_elements_by_class_name('rowClass1')
            logger.info("In this search page we have "+str(len(listLinks))+" entries to click")
            numItemsToVisit = len(listLinks)
            for i in range(0,numItemsToVisit):
                #Collect the link from the current page
                logger.debug("Visit element "+str(i)+" out of "+str(numItemsToVisit))
                try:
                    if i==0:
                        #first link has a different layout position
                        linkStr = '/html/body/div/div/div[2]/div[2]/div/div/div/div/div/div/form/div[3]/table/tbody/tr[%d]/td[1]/div[1]/a/span'
                    else:
                        linkStr = '/html/body/div/div/div[2]/div[2]/div/div/div/div/div/div/form/div[4]/table/tbody/tr[%d]/td[1]/div[1]/a/span'
                    logger.debug("Click "+linkStr)
                    
                    publicationLink = self.driver.find_element_by_xpath(linkStr%(i+1))
                    #click the link
                    publicationLink.click()
                except Exception as e:
                    print e
                #download
                documentLinks = self.driver.find_elements_by_class_name('documentosPub')        
                logger.debug("There are "+str(len(documentLinks))+" documents to download")
                                        
                for document in documentLinks:
                    cell=None
                    try:
                        cell = document.find_elements_by_class_name("celdaTam2")
                    except Exception as e:
                        logger.error(e)
                        logger.debug("There was no XML link")
                        
                    if cell!=None and len(cell)>=2:
                        logger.debug("There was an XML link")
                        theLink = cell[1]
                        urlToDownload = theLink.get_attribute('href') 
                        
                        if not self.isUrlArchived(urlToDownload):
                            logger.debug("Download: "+theLink.get_attribute('href'))
                            collectedDocuments=collectedDocuments+1
                            
                            yield Request(urlToDownload,callback=self.simpleArchiveItem)
                        else:
                            logger.info("We already crawled %s."%urlToDownload)
                            #return
                
                #go back to search menu
                logger.debug("Go to the search page")
                backButton = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,'/html/body/div/div/div[1]/div[4]/div[2]/a'))) 
                backButton.click()

            #click next button
            logger.debug("Click the next button")
            xpathNextButton1 = '/html/body/div/div/div[2]/div[2]/div/div/div/div/div/div/form/div[3]/table/tfoot/tr/td/div/input[2]'
            xpathNextButton2 = '/html/body/div/div/div[2]/div[2]/div/div/div/div/div/div/form/div[3]/table/tfoot/tr/td/div/input[3]'
         
#             if currentPage==1:
#                 #for some reason the xpath changes in the first page
#                 xpathNextButton1 = '/html/body/div/div/div[2]/div[2]/div/div/div/div/div/div/form/div[3]/table/tfoot/tr/td/div/input[2]'
#                 xpathNextButton2 = '/html/body/div/div/div[2]/div[2]/div/div/div/div/div/div/form/div[3]/table/tfoot/tr/td/div/input[3]'
            nextButton1 = None
            nextButton2 = None
            try:
                nextButton1 = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,xpathNextButton1))) 
                nextButton2 = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,xpathNextButton2)))
            except Exception as e:
                #the button could be allocated using div4
                logger.warn("First attempt to find next button failed")
              
            if nextButton1 == None:
                #sometimes the next button is included inside a different level
                xpathNextButton1 = '/html/body/div/div/div[2]/div[2]/div/div/div/div/div/div/form/div[4]/table/tfoot/tr/td/div/input[2]'
                xpathNextButton2 = '/html/body/div/div/div[2]/div[2]/div/div/div/div/div/div/form/div[4]/table/tfoot/tr/td/div/input[3]'
                try:
                    nextButton1 = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,xpathNextButton1))) 
                    nextButton2 = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,xpathNextButton2)))
                except NoSuchElementException:
                    logger.error("Second attempt to find next button failed. Next button was not found.")
                    nextButton1=None
              
            if nextButton1!=None:
                if nextButton1.get_attribute('value')=='Next':
                    nextButton = nextButton1
                elif nextButton2.get_attribute('value')=='Next':
                    nextButton = nextButton2
                    
                currentPage=currentPage+1
                logger.info("Move to page: "+str(currentPage))
                nextButton.click()
            else:
                logger.info("We didn't find the next button. We assume we have finished")
                stop=True
                
        self.driver.close()
          
                                                                                       
        logger.info("Crawling summary:")
        logger.info("Collected documents "+str(collectedDocuments))
        logger.info("Processed pages: "+str(currentPage))

        
        
        