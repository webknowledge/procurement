# -*- coding: utf-8 -*-
import scrapy
from scrapy import Request
from digiwhist import DWhistSpider

import datetime

class TedSpider(DWhistSpider):
    '''
    The TED web provides with all the monthly releases packaged in .tar.gz packages that
    can be directly extracted by just forming the corresponding URL.

    http://ted.europa.eu/xml-packages/monthly-packages/2011/2011-01.tar.gz

    The link above corresponds to January 2011. When uncompressed there is one tar.gz file
    per each day there was available annotations.

    '''
    
    name = "ted"
    allowed_domains = ["ted.europa.eu","127.0.0.1"]
    
    custom_settings={'DOWNLOAD_DELAY':1.15,
                     'DOWNLOAD_WARNSIZE':34359738368,
                     'CONCURRENT_REQUESTS':1
                     }
    
    def __init__(self,urls):
        toExplore = self.getTedURLS()
        DWhistSpider.__init__(self,toExplore,self.name)
        #Check if we following urls have already been download and skip them when needed
        
                
    def parse(self,response):
        return self.checkAndArchive(response)
    
    
    def getTedURLS(self):
        
        urls=[]
        
        baseString0="http://ted.europa.eu/xml-packages/monthly-packages/%d/%d-0%d.tar.gz"
        baseString1="http://ted.europa.eu/xml-packages/monthly-packages/%d/%d-%d.tar.gz"

    
        today = datetime.date.today()
        
        for year in range(2011,(today.year+1)):
            for month in range(1,13):
                if month<10:
                    urlToExplore = baseString0%(year,year,month)
                else:
                    urlToExplore = baseString1%(year,year,month)
                    
                if not self.isUrlArchived(urlToExplore):
                    urls.append(urlToExplore)
        return urls