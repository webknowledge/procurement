# -*- coding: utf-8 -*-


from scrapy import Request
from crawling.spiders.digiwhist import DWhistSpider


from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select

from django.conf import settings

import logging
import time
import datetime as dt
from dateutil.relativedelta import relativedelta

# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

class CYSpider(DWhistSpider):
    '''
    The Cyprus web crawling starts at:
    https://www.eprocurement.gov.cy/ceproc/prepareAdvancedSearch.do
    '''
    
    name = "cy"
   
    allowed_domains = ["eprocurement.gov.cy"]
    #put some delay to make it work
    download_delay=1.2
    
    def __init__(self,urls):
        DWhistSpider.__init__(self,urls,self.name)
        self.driver = webdriver.PhantomJS(settings.WEBDRIVER_PATH,
                                            service_args=['--ssl-protocol=any',
                                                          '--ignore-ssl-errors=true',
                                                          '--web-security=false']
                                            )
        #self.driver = webdriver.Firefox()
    
    def parse(self,response):
        '''
        Using selenium click search button and start yielding requests.
        We run search queries in intervals of months to avoid the limitation of 1000 results.
        '''
        logger.info("Get initial page "+response.url.decode("utf-8"))
        self.driver.get(response.url)

        finalDate = dt.date(2009,1,1)
        currentDate = dt.date.today()
        while currentDate > finalDate:
            links = self.processLinks(currentDate)
            if len(links)==0:
                logger.info('No more available links. We stop now.')
                return
            logger.info("Found %d links"%len(links))
            for l in links:
                yield Request(l,self.simpleArchiveItem)

            currentDate = currentDate - relativedelta(months=1)

        self.driver.close()

    def processLinks(self,currentDate):
        '''
        Process all the links one month ago from the current date
        :param currentDate:
        :return:
        '''

        logger.info("Select no filters")
        optionQuery='/html/body/div[1]/div[5]/div[2]/form[1]/div[1]/fieldset/p[4]/select'
        optionButton = Select(WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,optionQuery))))
        optionButton.select_by_index(0)

        #insert the dates
        fromDate = currentDate - relativedelta(months=1)
        logger.info("Select entries from %s to %s"%(fromDate.strftime('%d/%m/%Y'),currentDate.strftime('%d/%m/%Y')))

        inputStartingDate = self.driver.find_element_by_xpath('/html/body/div[1]/div[5]/div[2]/form[1]/div[1]/fieldset/p[7]/input[1]')
        #inputStartingDate.send_keys(fromDate.strftime('%d/%m/%Y'))
        self.driver.execute_script("arguments[0].setAttribute('value','%s')"%fromDate.strftime('%d/%m/%Y'),inputStartingDate)
        inputEndDate = self.driver.find_element_by_xpath('/html/body/div[1]/div[5]/div[2]/form[1]/div[1]/fieldset/p[7]/input[2]')
        #inputEndDate.send_keys(currentDate.strftime('%d/%m/%Y'))
        self.driver.execute_script("arguments[0].setAttribute('value','%s')"%currentDate.strftime('%d/%m/%Y'),inputEndDate)

        logger.info("Click the search button")
        searchButtonQuery = '/html/body/div[1]/div[5]/div[2]/form/div[2]/p/input[1]'
        button = self.driver.find_element_by_xpath(searchButtonQuery)
        button.click()

        #get the links from the current page
        linksQuery = "/html/body/div[1]/div[5]/div[2]/div[1]/div/table/tbody/tr/td[2]/a"
        pageQuery = "/html/body/div[6]/div[2]/div/div[1]/table/tbody/tr[1]"
        nextButtonQuery="/html/body/div[1]/div[5]/div[2]/div[2]/p[2]/button[3]"

        currentPage = 1
        explored=0

        toReturn=[]

        while True:
            links = WebDriverWait(self.driver,10).until(EC.presence_of_all_elements_located((By.XPATH,linksQuery)))
            for l in links:
                urlToExplore = l.get_attribute("href")
                if not self.isUrlArchived(urlToExplore):
                    explored+=1
                    toReturn.append(urlToExplore)
                else:
                    logger.info("[%s] already crawled. Stop here."%urlToExplore)
                    return toReturn
            #jump to the next page
            try:
                nextButton = self.driver.find_element_by_xpath(nextButtonQuery)
                if nextButton.get_attribute('disabled')!=None:
                    return toReturn
                time.sleep(1)
                nextButton.click()
                currentPage+=1
            except Exception as e:
                logger.info("No next button found. We stop here")
                return toReturn

        return toReturn