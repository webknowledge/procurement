

from scrapy import Request
from crawling.spiders.digiwhist import DWhistSpider
import time
from urlparse import urlparse

import logging


# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

class NOSpider(DWhistSpider):
    '''
    Tha main page contains the urls for the main static page containing relevant information.
    Inside these pages we can access the publication link that contains the publication itself.
    This can be generated statically without simulation.
    '''
    
    name = "no"
   
    allowed_domains = ['http://www.doffin.no','www.doffin.no']
                       
    #put some delay to make it work
    download_delay=1.85
    
    def __init__(self,urls):
        DWhistSpider.__init__(self,urls,self.name)
    
    def parse(self,response):
                
        logger.info("Current page %s"%response.url)
        #/html/body/div[1]/form/div[4]/div/div/div/div[1]/div/div/article[3]/div[1]/div[1]/a[2]
        query='//div[re:test(@class,"notice-search-item-header")]'
        links = response.xpath(query).extract()
        logger.info("Page with %d links"%len(links))
        
        for i in links:
            for token in i.split("<"):

                if "Notice/Details/" in token:
                    aux = token.replace('a href=\"','')
                    urlPage = "https://www.doffin.no"+aux.split('">')[0]
                    #print urlPage
                    if not self.isUrlArchived(urlPage):
                        yield Request(urlPage,callback=self.simpleArchiveItem)
        
        #get the current page number from the url
        o = urlparse(response.url)
        pageNumber = int(o.query.split('&')[0].replace('pageNumber=',''))
        nextButtonQuery='//a[re:test(@data-current-page,"%d")]/@href'%(pageNumber+1)
        
        nextButton = response.xpath(nextButtonQuery).extract()
        
        if len(nextButton)==0:
            logger.info("There was no next button. Exit.")
            return
        
        nextUrl = 'https://www.doffin.no'+nextButton[0]
        time.sleep(3)
        
        yield Request(nextUrl,callback=self.parse)
        
        