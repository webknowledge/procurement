# -*- coding: utf-8 -*-


from scrapy import Request
from digiwhist import DWhistSpider

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


from django.conf import settings


import logging


# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

class BGSpider(DWhistSpider):
    '''
    The Bulgarian spider starts at 
    http://rop3-app1.aop.bg:7778/portal/page?_pageid=93,1&_dad=portal&_schema=PORTAL
    '''
    
    name = "bg"
   
    allowed_domains = ["http://rop3-app1.aop.bg","http://www.aop.bg/","www.aop.bg"]
    
    custom_settings={'DOWNLOAD_DELAY':1.15,
                     'USER_AGENT':"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1"
                     }
    
    
    
    def __init__(self,urls):
        DWhistSpider.__init__(self,urls,self.name)
        self.dcap = dict(DesiredCapabilities.PHANTOMJS)
        self.dcap["phantomjs.page.settings.userAgent"]=("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1")
        self.driver = webdriver.PhantomJS(settings.WEBDRIVER_PATH,
                                           service_args=['--ssl-protocol=any',
                                                         '--ignore-ssl-errors=true', 
                                                         '--web-security=false'],
                                           desired_capabilities=self.dcap
                                           )
    
    def parse(self,response):
        '''
        Using selenium click search button and start yielding requests
        '''
        logger.info("Get initial page "+response.url.decode("utf-8"))
        self.driver.get(response.url)

        
        #click the search menu without any restriction
        logger.info("Click search button...")
        query='/html/body/table/tbody/tr/td/table[2]/tbody/tr/td[1]/table[1]/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/form/input[5]'
        button = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,query)))
        button.click()

        phpSessionId = self.driver.get_cookie("PHPSESSID")


        logger.info("Click on the first entry to force PHPSessionId")

        
        #Now we are in the search results page. Collect the results.              
               
        stop=False
        currentPage=1
        foundDocuments=0
        while not stop:

            query="/html/body/table/tbody/tr/td/table[2]/tbody/tr/td[2]/table[2]/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td[2]/table/tbody/tr/td[2]/a"

            listLinks = WebDriverWait(self.driver,10).until(EC.presence_of_all_elements_located((By.XPATH,query)))

            for i in listLinks:
                urlToExplore = i.get_attribute('href')
                if not self.isUrlArchived(urlToExplore):
                    foundDocuments+=1
                    yield Request(urlToExplore,callback=self.simpleArchiveItem,cookies=phpSessionId)
                else:
                    logger.info("We already crawled [%s]. We stop here!"%urlToExplore)
                    return

                            
            #Find the next page button
            if currentPage==1:
                query='/html/body/table/tbody/tr/td/table[2]/tbody/tr/td[2]/table[2]/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/form[2]/a[1]'
            elif currentPage<11:
                query='/html/body/table/tbody/tr/td/table[2]/tbody/tr/td[2]/table[2]/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/form[2]/a[3]'
            elif currentPage<101:
                query='/html/body/table/tbody/tr/td/table[2]/tbody/tr/td[2]/table[2]/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/form[2]/a[4]'
            else:
                query='/html/body/table/tbody/tr/td/table[2]/tbody/tr/td[2]/table[2]/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/form[2]/a[5]'
            
            try:
                button = self.driver.find_element_by_xpath(query)
                button.click()
                logger.info("Finally we found %d documents in the page"%foundDocuments)
                currentPage=currentPage+1
            except Exception as e:
                stop = True

            logger.info("Processed page %d total documents: %d"%(currentPage,foundDocuments))
                
        self.driver.close()
