


from scrapy import Request
from crawling.spiders.digiwhist import DWhistSpider

import logging


# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

class CZSpider(DWhistSpider):
    name = "cz"
   
    allowed_domains = ['www.vestnikverejnychzakazek.cz']
                       
    #put some delay to make it work
    download_delay=1.85
    
    def __init__(self,urls):
        DWhistSpider.__init__(self,urls,self.name)
    
    def parse(self,response):
        
        '''
        Initial web page without any query filtering
        '''
        logger.info("Current page %s"%response.url)
        
        #check the available links
        linksQuery = '//a[re:test(@class,"contract-link")]/@href'
        contractLinks = response.xpath(linksQuery).extract()
        
        for url in contractLinks:
            urlToExplore = 'http://www.vestnikverejnychzakazek.cz'+url
            #change to get text version
            urlToExplore = urlToExplore.replace('Display','DisplayContent') 
            if not self.isUrlArchived(urlToExplore):
                yield Request(urlToExplore,callback=self.simpleArchiveItem)
        
         
                
        #do we have a next button?
        nextButtonQuery = '/html/body/div[1]/div[3]/div[2]/div/div[2]/a[3]/@href'
        
        try:
            nextUrl = response.xpath(nextButtonQuery).extract()[0]
            nextUrl='http://www.vestnikverejnychzakazek.cz'+nextUrl
            yield Request(nextUrl,callback=self.parse)
        except Exception as e:
            logger.info("Next page link not found! We stop crawling here")
            
