

import scrapy
from scrapy import Request
from digiwhist import DWhistSpider

import logging


# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

download_delay=1.8

class LVSpider(DWhistSpider):
    '''
    The crawler for latvia starts in
    http://www.iub.gov.lv/lv/iubsearch/
    There is a list of contracts to be downloaded. The elements from the second
    column (Publikacijas veids) contain the link of the data to download
    '''

    name="lv"
    allowed_domains = ["www.iub.gov.lv","pvs.iub.gov.lv"]

    
    def __init__(self,urls):
        DWhistSpider.__init__(self,urls,self.name)
    
    
    def parse(self,response):
        #get all the elements form the page with class c3
        logger.info("Process page "+response.url)
        query = '//div[re:test(@class,"name")]//@href'
        links = response.xpath(query).extract()
        
        for i in links:
            if 'pvs.iub.gov.lv' in i:
                if not self.isUrlArchived(i):
                    #print i
                    yield Request(i,callback=self.simpleArchiveItem) 
        
        #go to the next page
        queryNextButton = '//a[re:test(@class,"next")]//@href'
        nextButton = response.xpath(queryNextButton).extract()
        if len(nextButton) == 0:
            logger.info("We didn't found the next button. We stop crawling.")
        
        nextButton = "http://www.iub.gov.lv"+nextButton[0]
        yield Request(nextButton,callback=self.parse)    

        
        