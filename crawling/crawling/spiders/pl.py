# -*- coding: utf-8 -*-
import scrapy
from scrapy import Request
from digiwhist import DWhistSpider
import time
import logging

logger = logging.getLogger("DigiwhistWebLogger")

class PLSpider(DWhistSpider):
    '''
    This spider follows the same design of TED
    '''
    
    name = "pl"
    allowed_domains = ["ftp.uzp.gov.pl","127.0.0.1"]
    #put some delay to make it work
    download_delay=5
    custom_settings={'DOWNLOAD_DELAY':5,
                     'CONCURRENT_REQUESTS':1,
                     'CONCURRENT_REQUESTS_PER_IP':1
                     }
    
    def __init__(self,urls):
        DWhistSpider.__init__(self,urls,self.name)
    
    
    def start_requests(self):
        for aux in self.start_urls:
            if not self.isUrlArchived(aux):
                logger.info("Download: "+aux)
                yield Request(aux,
                      meta={'ftp_user': 'anonymous', 'ftp_password': ''},
                      callback=self.simpleArchiveItem)
                
    
    
    
    
    
    
    
