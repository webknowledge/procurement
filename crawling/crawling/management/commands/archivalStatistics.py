
from django.core.management.base import BaseCommand,CommandError

from crawling.models import ArchiveItem


import logging
# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

class Command(BaseCommand):
    help ='Shows statistics about the archival'
    
        
    def handle(self, *args, **options):
        self.launchParser()
        
    def launchParser(self):
        sources = ArchiveItem.objects.order_by('source').values_list('source').distinct('source')
        
        logger.info("There are %d available sources"%len(sources))
        total = 0
        for source in sources:
            numEntries = ArchiveItem.objects.filter(source=source).count()
            logger.info("[%s]:\t%d"%(source,numEntries))
            total+=numEntries
        logger.info("Total:\t%d"%total)
    