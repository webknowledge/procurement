
from django.core.management.base import BaseCommand


from scrapy.settings import Settings

from twisted.internet import reactor
from django.conf import settings
from scrapy.crawler import CrawlerRunner
from scrapy.utils.log import configure_logging

import datetime

import os

import logging
from crawling.spiders.oldTed import OldTedSpider
from crawling.spiders.ted import TedSpider
from crawling.spiders.pl import PLSpider
from crawling.spiders.be import BESpider
from crawling.spiders.es import ESSpider
from crawling.spiders.ukArchive import UKArchiveSpider
from crawling.spiders.bg import BGSpider
from crawling.spiders.dk import DKSpider
from crawling.spiders.fi import FISpider
from crawling.spiders.lv import LVSpider
from crawling.spiders.nl import NLSpider
from crawling.spiders.no import NOSpider
from crawling.spiders.fr import FRSpider
from crawling.spiders.cz import CZSpider
from crawling.spiders.it import ITSpider
from crawling.spiders.pt import PTSpider
from crawling.spiders.ch import CHSpider
from crawling.spiders.rs import RSSpider
from crawling.spiders.ee import EESpider
from crawling.spiders.ro import ROSpider
from crawling.spiders.ie import IESpider
from crawling.spiders.lt import LTSpider
from crawling.spiders.hu import HUSpider
from crawling.spiders.se import SESpider
from crawling.spiders.cy import CYSpider
from crawling.spiders.hr import HRSpider
from crawling.spiders.sk import SKSpider

# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

configure_logging({'LOG_LEVEL':'INFO'})
#configure_logging({'LOG_LEVEL':'DEBUG'})


def startCrawlers(urls,spider):
    os.environ['SCRAPY_SETTINGS_MODULE']=settings.SETTINGS_MODULE
    scrapySettings = Settings()
    settings_module_path=os.environ['SCRAPY_SETTINGS_MODULE']
    scrapySettings.setmodule(settings_module_path,priority='project')
    scrapySettings.set('ITEM_PIPELINES',{})
    #scrapySettings.set('COOKIES_DEBUG',True)
    
    runner = CrawlerRunner()
    d=runner.crawl(spider,urls)

    #d=runner.join()
    d.addBoth(lambda _:reactor.stop())
    reactor.run()

 
def getPLUrls():
    logger.info("Crawl data from Poland")
    toReturn = []
    #year/%year%month%day
    baseUrl="ftp://ftp.uzp.gov.pl/bzp/xml/%s/%s%s%s.exe"
    today = datetime.date.today()
    startYear = 2011
    
    for year in range(startYear,(today.year+1)):
        for month in range(1,13):
            for day in range(1,32):
                stringMonth = ""
                stringDay = ""
                if month<10:
                    stringMonth="0"+str(month)
                else:
                    stringMonth=str(month)
                if day<10:
                    stringDay="0"+str(day)
                else:
                    stringDay=str(day)
                toReturn.append(baseUrl%(str(year),str(year),stringMonth,stringDay))
    return toReturn
    
def getBEUrls():
    logger.info("Crawl data from Belgium")
    return ["https://enot.publicprocurement.be/enot-war/preSearchNotice.do"]

def getESUrls():
    logger.info("Crawl data from Spain")
    return ["https://contrataciondelestado.es/wps/portal/"]

def getUKArchiveUrls():
    #fill the list of urls
    start_urls=[]
    url = "https://data.gov.uk/data/contracts-finder-archive/static/files/notices_%d_%s.xml"
    for year in range(2011,2015):
        for month in range(1,13):
            if month<10:
                strMonth = "0"+str(month)
            else:
                strMonth = str(month)
            start_urls.append(url%(year,strMonth))
    #extra for January
    start_urls.append(url%(2015,"01"))
    return start_urls


    
class Command(BaseCommand):
    
    help = 'Start the crawling process'
    
    def add_arguments(self,parser):
        parser.add_argument('source',type=str)
    
    def handle(self, *args, **options):
        if options['source']=='all':
            logger.info("Launch all crawlers simultaneously")
            for db in settings.DATABASES.keys():
                if db!="default":
                    self.launchCrawler(db)  
        else:
            self.launchCrawler(options['source'])
             
    def launchCrawler(self,source):
        urls=None
        spider=None
        if source=="ted":
            urls=[]
            spider=TedSpider
        elif source=="oldTed":
            urls=['ftp://ted.europa.eu/daily-packages/2010/2010-01/utf8/en/en_20100102_001_utf8_org.zip']
            spider=OldTedSpider
        elif source=="pl":
            urls=getPLUrls()
            spider=PLSpider
        elif source=="be":
            urls=getBEUrls()
            spider=BESpider
        elif source=="es":
            urls=getESUrls()
            spider=ESSpider
        elif source=="uk":
            urls=['https://www.contractsfinder.service.gov.uk/Search/Results?LocationType=AllLocations']
            spider=UKSpider
        elif source=="ukArchive":
            urls = getUKArchiveUrls()
            spider=UKArchiveSpider
        elif source=="bg":
            urls=['http://rop3-app1.aop.bg']
            spider=BGSpider
        elif source=="dk":
            urls=["https://www.udbud.dk/"]
            spider=DKSpider
        elif source=="fi":
            urls=["http://www.hankintailmoitukset.fi/fi/?page=1"]
            spider=FISpider
        elif source=="lv":
            urls=["http://www.iub.gov.lv/lv/iubsearch/"]
            spider=LVSpider
        elif source=="nl":
            urls=["https://www.tenderned.nl/tenderned-web/aankondiging/overzicht/aankondigingenplatform"]
            spider=NLSpider
        elif source=="no":
            urls=["https://www.doffin.no/Notice?pageNumber=1&pageSize=10"]
            spider=NOSpider
        elif source=='fr':
            urls=["http://www.boamp.fr/recherche/avancee"]
            spider=FRSpider
        elif source=='cz':
            urls=['https://www.vestnikverejnychzakazek.cz/hu/Searching/FullTextSearch']
            spider=CZSpider
        elif source=='it':
            urls=['http://portaletrasparenza.avcp.it/microstrategy/html/index.htm']
            spider=ITSpider
        elif source=="pt":
            urls=["http://www.base.gov.pt/Base/pt/ResultadosPesquisa?type=contratos&query=texto%3D%26tipo%3D0%26tipocontrato%3D0%26cpv%3D%26aqinfo%3D%26adjudicante%3D%26adjudicataria%3D%26desdeprecocontrato_false%3D%26desdeprecocontrato%3D%26ateprecocontrato_false%3D%26ateprecocontrato%3D%26desdedatacontrato%3D%26atedatacontrato%3D%26desdedatapublicacao%3D%26atedatapublicacao%3D%26desdeprazoexecucao%3D%26ateprazoexecucao%3D%26desdedatafecho%3D%26atedatafecho%3D%26desdeprecoefectivo_false%3D%26desdeprecoefectivo%3D%26ateprecoefectivo_false%3D%26ateprecoefectivo%3D%26pais%3D0%26distrito%3D0%26concelho%3D0",
                  "http://www.base.gov.pt/Base/pt/ResultadosPesquisa?type=anuncios&query=texto%3D%26numeroanuncio%3D%26emissora%3D%26desdedatapublicacao%3D%26atedatapublicacao%3D%26desdeprecobase_false%3D%26desdeprecobase%3D%26ateprecobase_false%3DAt%25C3%25A9%253A%26ateprecobase%3D%26tipoacto%3D0%26tipomodelo%3D0%26tipo-contrato%3D0%26tipocontrato%3D0%26cpv%3D%26activo%3D"]
            spider=PTSpider
        elif source=='ch':
            urls=["https://www.simap.ch/shabforms/COMMON/search/searchForm.jsf"]
            spider=CHSpider
        elif source=='rs':
            urls=["http://portal.ujn.gov.rs/Pretraga.aspx?tab=3"]
            spider=RSSpider
        elif source=='ee':
            urls=["https://riigihanked.riik.ee/register"]
            spider=EESpider
        elif source=="ro":
            urls=['http://data.gov.ro/api/1/rest/dataset/']
            spider=ROSpider
        elif source=='ie':
            urls=['https://irl.eu-supply.com/ctm/supplier/publictenders']
            spider=IESpider
        elif source=='lt':
            urls=['http://www.cvpp.lt']
            spider=LTSpider
        elif source=='hu':
            urls=['http://www.kozbeszerzes.hu/adatbazis/keres/hirdetmeny/']
            spider=HUSpider
        elif source=='se':
            urls=['http://www.avropa.se/Upphandlingar/']
            spider=SESpider
        elif source=='cy':
            urls=['https://www.eprocurement.gov.cy/ceproc/prepareAdvancedSearch.do']
            spider=CYSpider
        elif source=='hr':
            urls=['https://eojn.nn.hr/Oglasnik/']
            spider=HRSpider
        elif source=='sk':
            urls=['http://tender.sme.sk/en/contracts-list?order=contract_date.year:desc&page=1']
            spider=SKSpider
        elif source=='it':
            urls=[]
            spider=ITSpider
        if spider!=None:
            startCrawlers(urls,spider)
        else:
            logger.error("The selected spider was not correct")
