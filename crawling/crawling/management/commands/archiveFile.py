

from django.core.management.base import BaseCommand,CommandError

from crawling.models import ArchiveItem
from mongoengine import DoesNotExist
from time import gmtime, strftime


import logging
# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

class Command(BaseCommand):
    help ='Put one file into the archive'
    
    def add_arguments(self,parser):
        parser.add_argument('source',type=str)
        parser.add_argument('inputFile',type=str)
        parser.add_argument('url',type=str)
        
    
    def handle(self, *args, **options):
        self.archiveFile(options['source'],options['inputFile'],options['url'])
        
    def archiveFile(self,source,inputFile,url):
        
        item = ArchiveItem()
        item.source = source
        item.date=strftime("%Y-%m-%d %H:%M:%S", gmtime())
        item.url = url
        with open(inputFile) as reader:
            item.file.put(reader.read())
        item.save()
        logger.info("Save into archival item: "+inputFile+", "+url+", "+source)
        