

from django.core.management.base import BaseCommand,CommandError

from crawling.models import ArchiveItem
from mongoengine import DoesNotExist

import os
import logging
import codecs
import random
import urllib
# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")

class Command(BaseCommand):
    help ='Dump the archival content into the specified folder'
    
    def add_arguments(self,parser):
        parser.add_argument('source',type=str,help="Data source to acquire data from",default="all")
        parser.add_argument('folder',type=str,help="Folder to store the data")
        parser.add_argument('numItems',nargs='?',type=int,help="Number of items to process",
                            default=-1)
        parser.add_argument('randomSample',nargs='?',type=bool,help="Indicate whether we run a random sampling")
        
    
    def handle(self, *args, **options):
        if options['folder']=="all":
            logger.info("Dump all the database")
            self.dumpAll(options['folder'])
        elif not options['randomSample']:
            self.dumpSource(options['folder'],options['source'],options['numItems'])
        elif options['randomSample']:
            self.dumpRandomSample(options['folder'],options['source'],options['numItems'])
    
    def dumpRandomSample(self,folder,source,numItems):
        '''
        Run a random sample from the query of items.
        '''
        #first check how many objects we have for that source
        numAvailableObjects = ArchiveItem.objects.filter(source=source).count()
        logger.info("Found %d available items"%numAvailableObjects)
        if numAvailableObjects==0:
            return
        if numAvailableObjects <= numItems:
            logger.info("There are %d available objects, less than the %d required"%(numAvailableObjects,numItems))
            logger.info("Dumping everything...")
            self.dumpSource(folder, source, numItems)
            return
        
        #generate list of random numbers
        indices = random.Random().sample(range(0,numAvailableObjects),numItems)
                
        for i in indices:
            object=ArchiveItem.objects.filter(source=source)[i]
            self.dumpObject(object,folder)
                

    def dumpAll(self,folder):
        self.createFolder(folder)
        query = ArchiveItem.objects.all()
        self.dumpObjectsFromQuery(folder, query)
                        
                
    def dumpSource(self,folder,source,numItems):
        if numItems>0:
            logger.info("Query a maximum of %d elements from source %s"%(numItems,source))
            query=ArchiveItem.objects.filter(source=source)[:numItems]
        else:
            logger.info("Query all elements from source %s"%(source))
            query=ArchiveItem.objects.filter(source=source)
        
        self.dumpObjectsFromQuery(folder, query)
        

    def dumpObject(self,object,folder):
        self.createFolder(folder)
        logger.info("Read entry "+object.url)
        data = object.file.read()

        outputPath = folder+"/"+urllib.parse.quote(object.url,safe='')

        if len(outputPath)>255:
            logger.info("The filename was too long. We make it shorter.")
            outputPath=outputPath[0:220]

        sourceEncoding = "utf-8"
        contentType = object.contentType
        if contentType != None:
            sourceCharset = contentType.split("charset=")
            if sourceCharset is not None and len(sourceCharset) > 1:
                logger.info("Content Type: " + sourceCharset[1])
                sourceEncoding = sourceCharset[1]
            else:
                logger.info("No content type provided for the object!")
            logger.info("->Dump into: "+outputPath)
            try:
                with codecs.open(outputPath,'w','utf-8') as writer:
                    writer.write(data.decode(sourceEncoding))
            except:
                logger.error("The file couldn't be writed using %s. Use default"%sourceEncoding)
                with open(outputPath,'w') as writer:
                    writer.write(str(data))

        else:
            with open(outputPath,'w') as writer:
                writer.write(str(data))


    def dumpObjectsFromQuery(self,folder,query):
        for f in query:
            self.dumpObject(f,folder)
            


                    
    def createFolder(self,folder):
        logger.info("Check output folder "+folder)
        if not os.path.isdir(folder):
            logger.info("Non existing. Create folder. "+folder)
            os.makedirs(folder)
        else:
            logger.info("The output folder "+folder+" already exists")
    
