
from django.core.management.base import BaseCommand,CommandError

from crawling.models import ArchiveItem
from mongoengine import DoesNotExist
import codecs

import logging
# Get an instance of a logger
logger = logging.getLogger("DigiwhistWebLogger")


class Command(BaseCommand):
    help ='Put one file into the archive'
    
    def add_arguments(self,parser):
        parser.add_argument('url',type=str)
        parser.add_argument('outputFile',type=str)
        
        
    
    def handle(self, *args, **options):
        self.getFromArchive(options['url'],options['outputFile'])
        
    def getFromArchive(self,url,outputFile):
        
        if ArchiveItem.objects.filter(url=url).count()==0:
            logger.info("Element [%s] not found in the archive"%url)
            return
        
        item = ArchiveItem.objects.get(url=url)
        
        with codecs.open(outputFile,'w',encoding="utf-8") as writer:
            writer.write(item.file.read().decode("utf-8"))
            logger.info("File was dumped onto %s"%outputFile)
        
