
from scrapy.item import Field,Item


class CrawlingItem(Item):
    '''
    Item crawled
    '''
    archivalId=Field()
    content=Field()
    url=Field()
    source=Field()
    completed=Field()
    
    def __str__(self):
        return self['source']+","+self['url']

