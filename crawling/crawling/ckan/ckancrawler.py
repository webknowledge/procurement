import json
import logging
import os
import urllib.request


class CkanResource:
    def __init__(self, remoteUrl, localUrl):
        self._remoteUrl = remoteUrl
        self._localUrl = localUrl

    def getLocalUrl(self):
        return self._localUrl

    def getRemoteUrl(self):
        return self._remoteUrl

    def __str__(self):
        return self._remoteUrl + " => " + self._localUrl


class CkanCrawler:
    def __init__(self, archiver, ckanDatasetUrl, datasetList):
        self._archiver = archiver
        self._datasetList = datasetList
        self._ckanDatasetUrl = ckanDatasetUrl
        self.logger = logging.getLogger("CkanCrawler")

    def processData(self, archiveFolder):
        self.logger.info("Starting CKAN Crawler ...")
        for item in self._datasetList:
            self.processSingleDataset(item, archiveFolder)

    def processSingleDataset(self, datasetName, archiveFolder):
        response = json.loads(urllib.request.urlopen(self._ckanDatasetUrl + "/" + datasetName).read().decode("utf8"))
        resources = response["resources"]
        resourcesSize = response["num_resources"]

        if len(resources) == resourcesSize:
            for resource in resources:
                resourceUrl = resource["url"]
                if resourceUrl is not None:
                    if self._archiver.isUrlArchived(resourceUrl):
                        self.logger.info("Resource url is archived already: " + resourceUrl)
                    else:
                        self.logger.info("Downloading " + resourceUrl)
                        ckanResource = self._downloadResource(archiveFolder, resourceUrl)
                        self.logger.info("Done downloading " + resourceUrl)
                        self.logger.info("Archiving url: " + resourceUrl)
                        self._archiver.fileArchiveItem(ckanResource)
        else:
            self.logger.error("Error: data set size does not match the number of resources")

    @staticmethod
    def _downloadResource(archiveFolder, resourceUrl):
        filename = archiveFolder + "/" + os.path.basename(resourceUrl)
        if not os.path.isfile(filename):
            urllib.request.urlretrieve(resourceUrl, filename)
        return CkanResource(resourceUrl, filename)
