import logging

import sys

from DigiwhistWeb.crawling.ckan.archiver import MongoArchiver
from DigiwhistWeb.crawling.ckan.ckancrawler import CkanCrawler

if __name__ == "__main__":
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG,
                        format='%(asctime)s [%(name)s] %(levelname)s %(message)s',
                        datefmt='%a, %d %b %Y %H:%M:%S')

    ARCHIVE_DATABASE = {"archive": "archive",
                        "host": "128.232.98.147",
                        # "host":"128.232.98.147",
                        "port": 27017}

    archiver = MongoArchiver("RO")
    archiver.connect(ARCHIVE_DATABASE)


    ckanCrawler = CkanCrawler(archiver,
                              "http://data.gov.ro/api/1/rest/dataset/",
                              ["achizitii-publice-2007-2015-contracte",
                               "achizitii-publice-2010-2015-anunturi-de-participare",
                               "achizitii-publice-2010-2015-invitatii-de-participare"])
    ckanCrawler.processData("/home/oms27/Projects/ro-conversion")
    archiver.disconnect()
