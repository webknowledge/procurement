import codecs
import logging
from time import strftime, gmtime
from mongoengine import DoesNotExist, StringField, DateTimeField, FileField, Document, connect
from mongoengine.connection import disconnect

logger = logging.getLogger("MongoArchiver")


class ArchiveItem(Document):
    source = StringField(max_length=10, null=False)
    date = DateTimeField(null=False)
    url = StringField(null=False)
    file = FileField(null=False)


class MongoArchiver:
    def __init__(self, name):
        self.name = name

    @staticmethod
    def connect(archiveSettings):
        connect(archiveSettings['archive'],
                host=archiveSettings['host'],
                port=archiveSettings['port'])

    @staticmethod
    def disconnect():
        disconnect()

    @staticmethod
    def getArchiveItem(url):
        result = None
        try:
            result = ArchiveItem.objects.get(url=url)
        except DoesNotExist:
            pass
        return result

    def archiveItem(self, response):
        # we have to store this item
        item = ArchiveItem()
        item.source = self.name
        item.date = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        item.url = response.url
        item.file.put(response.body)
        item.save()
        logger.info("Save into archival item from: " + response.url)
        return item

    def fileArchiveItem(self, ckanResource):
        # we have to store this item
        item = ArchiveItem()
        item.source = self.name
        item.date = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        item.url = ckanResource.getRemoteUrl()
        content, encoding = self.fileReadFilename(ckanResource.getLocalUrl())
        item.file.put(content, encoding=encoding)
        item.save()
        logger.debug("Save into archival item from: " + ckanResource.getRemoteUrl())

    @staticmethod
    def fileReadFilename(filename):
        content = None
        encoding = None
        try:
            with codecs.open(filename, mode="r+", encoding="utf8") as fin:
                content = fin.read()
                encoding = "utf8"
        except UnicodeDecodeError:
            with codecs.open(filename, mode="r+", encoding="latin1") as fin:
                content = fin.read()
                encoding = "latin1"
        return content, encoding

    @staticmethod
    def isUrlArchived(url):
        numOccur = ArchiveItem.objects.filter(url=url).count()
        return numOccur != 0
