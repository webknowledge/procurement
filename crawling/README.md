# Introduction

Set of crawlers implemented in the context of the Digiwhist project. The current approach contains 27 crawlers for 27 different countries.

# Additional configuration
The settings.py file contains all the configuration parameters to deal with the system. The crawlers are built on top of [Scrapy](http://pyscrapy.org) and many of the parameters modify their behavior. For more information visit the original Scrapy documentation.

## Mongo database

We assume that the final destination of the crawler files is a Mongo database. If the Mongo database is not accessible through the local machine, please configure accordingly.

```
ARCHIVE_DATABASE={
 "archive":"archive",
 "host":"localhost",
 "port":27017
}
```
`archive` indicates the name of the db inside Mongo, `host` is the host identifier and `port` is the Mongo listening port.

## PhantomJS

Some spiders run the [PhantomJS](http://phantomjs.org) headless Javascript driver. This driver has to be installed before running the crawlers. In Ubuntu you can installed using:

```
sudo apt-get install phantomjs
```


# Setup
1. Get the code
```
git clone https://gitlab.com/digiwhist/crawling.git
```
2. Create the python environment using python 2.7. This limitation is imposed by the utilization of Scrapy package ([more info here](http://doc.scrapy.org/en/latest/faq.html#does-scrapy-work-with-python-3)).
```
virtualenv env -p python2.7
source env/bin/activate
```
3. Install the dependencies in the local environment using the provided dependencies file
```
pip install -r dependencies
```

# Commands

To check the list of available commands:
```
python manage.py help

Type 'manage.py help <subcommand>' for help on a specific subcommand.

Available subcommands:

[crawling]
    archivalStatistics
    dumpArchival
    launchCrawler
    resetArchival
...
```
* `archivalStatistics` queries the Mongo DB and returns how many entries are available for each country
* `dumpArchival` dumps the crawled entries in the archive to a specified folder
* `launchCrawler` launches the crawler for one country
* `resetArchival` removes all the crawled entries for one country

All the commands offer their own help by using `python manage.py archivalStatistics --help`.

# Crawling

There are 27 available crawlers identified by their [two-letters ISO code](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) in the case of the countries or a descriptive name otherwise. The list of available crawlers is be, bg, ch, cy, cz, dk, ee, es, fi, fr, hr, hu, ie, it, lt, lv, nl, no, oldTed, pl, pt, ro, rs, se, sk, ted, uk and their crawlers can be found in the `crawling/spiders` folder.

If we want to crawl Spain we run:
```
python manage.py launchCrawler es
```
Each crawler is configured with different parameters to avoid being banned. Modifying these parameters may increase the performance, but may result in an access denial.




