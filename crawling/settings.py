import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '&55^ia0*1^0)o2t^721_98#ui1axk=8&i_&&(em26#%vcoap54'


INSTALLED_APPS = (
     'crawling',
)



# settings.py
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s '
                      '%(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
       'file':{
            'class':'logging.handlers.RotatingFileHandler',
            'formatter':'verbose',
            'filename':'/tmp/dwhist.log',
            'maxBytes':20971520
        }

    },
    'loggers': {
        'DigiwhistWebLogger': {
            'handlers': ['console','file'],
            'level': 'INFO',
        },
    }
}


#Mongo archive configuration
ARCHIVE_DATABASE={"archive":"archive",
                  "host":"localhost",
                  "port":27017
                  }

 
ROOT_URLCONF='crawling.urls'

#----------------------------------------------------------------------
#Scrapy settings
#----------------------------------------------------------------------


BOT_NAME = 'DigiwhistCrawler'

SPIDER_MODULES = ['crawling.spiders']
#NEWSPIDER_MODULE = 'crawling.spiders'


#Path to find the phantomjs web driver
WEBDRIVER_PATH="/usr/lib/phantomjs/phantomjs"


INTERMEDIATE_FOLDER="/tmp/intermediate"

#user agent to use in requests
USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1"
#General configuration parameters for the crawling.
CONCURRENT_ITEMS=1
CONCURRENT_REQUESTS=2
DOWNLOAD_TIMEOUT=3600

